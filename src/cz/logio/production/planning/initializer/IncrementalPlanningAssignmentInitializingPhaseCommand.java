package cz.logio.production.planning.initializer;

import java.util.Map;

import org.optaplanner.core.impl.phase.custom.CustomPhaseCommand;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.app.ProductionPlanningApp;
import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.MachineSetup;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.utils.ChangeoverMatrix;
import cz.logio.production.planning.utils.Mapper;

public class IncrementalPlanningAssignmentInitializingPhaseCommand implements
		CustomPhaseCommand {
	
	ChangeoverMatrix changeoverMatrix;
	Map<Machine,PlanningAssignment> lastAssigned;

	public void changeWorkingSolution(ScoreDirector scoreDirector) {
		System.out.println("INCREMENTAL INITIALIZING");
		ProductionPlanning productionPlanning = (ProductionPlanning) scoreDirector.getWorkingSolution();
		ProductionPlanning previousProductionPlanning = ProductionPlanningApp.getPreviousProductionPlanning();
		
		//planning assignments from the previous solving
		Map<String,PlanningAssignment> previousPlanningAssignments = Mapper.createPlanningAssignmentMap(previousProductionPlanning);
		
		for (PlanningAssignment planningAssignment : productionPlanning.getPlanningAssignmentList()) {
			if (planningAssignment.isFixed()) {
				setTimeStart(scoreDirector,planningAssignment,planningAssignment.getTimeStart());
				setMachineSetup(scoreDirector,planningAssignment,planningAssignment.getMachineSetup());
				
				int numberOfTimeSteps = planningAssignment.getNumberOfTimeSteps();
		
				continue;
			}
			
			PlanningAssignment previous = previousPlanningAssignments.get(planningAssignment.getId());
			
			if (previous != null) {
				MachineSetup machineSetup = getCorrespondingMachineSetup(planningAssignment,previous.getMachineSetup());
				setMachineSetup(scoreDirector,planningAssignment,machineSetup == null ? 
						planningAssignment.getAllowedMachineSetupList().get(0) : machineSetup);
				setTimeStart(scoreDirector,planningAssignment,previous.getTimeStart());
				setAdditionalTime(scoreDirector,planningAssignment,previous.getAdditionalTime());
			}
			else {
				setMachineSetup(scoreDirector,planningAssignment,planningAssignment.getAllowedMachineSetupList().get(0));
				setAdditionalTime(scoreDirector,planningAssignment,0);
				
				if (!planningAssignment.isAssigned()) {
					setTimeStart(scoreDirector,planningAssignment,0);	
				}
				else {
					setTimeStart(scoreDirector,planningAssignment,productionPlanning.getTimePointsAvailable());
		}
	}
		}
	}
	
	private MachineSetup getCorrespondingMachineSetup(PlanningAssignment assignment, MachineSetup machineSetup) {
		for (MachineSetup ms : assignment.getAllowedMachineSetupList()) {
			if (areEqual(machineSetup,ms)) return ms;
		}
		return null;
	}
	
	private boolean areEqual(MachineSetup m1, MachineSetup m2) {
		if (m1.getMachineList().size() != m2.getMachineList().size()) return false;
		if (m1.getMaterialList().size() != m2.getMaterialList().size()) return false;
		
		for (Material m : m1.getMaterialList()) {
			if (!m2.getMaterialList().contains(m)) return false;
		}
		
		for (Machine m : m1.getMachineList()) {
			if (!m2.getMachineList().contains(m)) return false;
		}
		
		return true;
	}
	
	private void setTimeStart(ScoreDirector scoreDirector, PlanningAssignment assignment, int time) {
		scoreDirector.beforeVariableChanged(assignment, "timeStart");
		assignment.setTimeStart(time);
		scoreDirector.afterVariableChanged(assignment, "timeStart");
	}
	
	private void setMachineSetup(ScoreDirector scoreDirector, PlanningAssignment assignment, MachineSetup machineSetup) {
		scoreDirector.beforeVariableChanged(assignment, "machineSetup");
		assignment.setMachineSetup(machineSetup);
		scoreDirector.afterVariableChanged(assignment, "machineSetup");
	}
	
	private void setAdditionalTime(ScoreDirector scoreDirector, PlanningAssignment assignment, int time) {
		scoreDirector.beforeVariableChanged(assignment, "additionalTime");
		assignment.setAdditionalTime(0);
		scoreDirector.afterVariableChanged(assignment, "additionalTime");
	}
	
}
