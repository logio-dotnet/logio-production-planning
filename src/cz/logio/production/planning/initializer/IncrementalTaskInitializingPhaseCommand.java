package cz.logio.production.planning.initializer;

import java.util.List;
import java.util.Map;

import org.optaplanner.core.impl.phase.custom.CustomPhaseCommand;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.app.ProductionPlanningApp;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.Task;
import cz.logio.production.planning.utils.Mapper;

public class IncrementalTaskInitializingPhaseCommand implements CustomPhaseCommand {

	public void changeWorkingSolution(ScoreDirector scoreDirector) {
		ProductionPlanning productionPlanning = (ProductionPlanning) scoreDirector.getWorkingSolution();
		ProductionPlanning previousProductionPlanning = ProductionPlanningApp.getPreviousProductionPlanning();
		
		List<Task> tasks = productionPlanning.getTaskList();
		
		//tasks from the previous solving
		Map<String,Task> previousTasks = Mapper.createTaskMap(previousProductionPlanning);
		Map<String,PlanningAssignment> currentPlanningAssignments = Mapper.createPlanningAssignmentMap(productionPlanning);
		
		for (Task task : tasks) {
			if (task.getAssignment() != null) continue;

			scoreDirector.beforeVariableChanged(task, "assignment");
			
			if (task.isFixed()) {
				task.setAssignment(task.getInitialAssignment());
			}
			else {
				Task previous = previousTasks.get(task.getId());
				boolean assigned = false;
				if (previous != null) {
						PlanningAssignment assignment = currentPlanningAssignments.get(previous.getAssignment().getId());
						if (assignment != null) {
							task.setAssignment(assignment);
							assigned = true;
						}
				}
				
				if (!assigned) {
					task.setAssignment(task.getInitialAssignment());
				}
			}
			
			scoreDirector.afterVariableChanged(task, "assignment");		
		}
	}
	
	
}
