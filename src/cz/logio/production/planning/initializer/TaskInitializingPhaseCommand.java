package cz.logio.production.planning.initializer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collections;

import org.optaplanner.core.impl.phase.custom.CustomPhaseCommand;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.Task;
import cz.logio.production.planning.domain.comparator.TaskDifficultyComparator;

public class TaskInitializingPhaseCommand implements CustomPhaseCommand {

	public void changeWorkingSolution(ScoreDirector scoreDirector) {
		ProductionPlanning productionPlanning = (ProductionPlanning) scoreDirector.getWorkingSolution();
		
		List<Task> tasks = productionPlanning.getTaskList();
		Collections.sort(tasks, new TaskDifficultyComparator());
		
		Map<Material,Task> lastAssignedTasks = new HashMap<Material,Task>();
		int maxTimeToGroupTasks = productionPlanning.getTimeHandler().hoursToTimeSteps(48); 
		
		for (Task task : tasks) {
			if (task.getAssignment() != null) continue;

			scoreDirector.beforeVariableChanged(task, "assignment");
			
			if (task.isFixed()) {
				System.out.println(task + " --> " + task.getInitialAssignment());
				task.setAssignment(task.getInitialAssignment());
			}
			else {
				Task lastAssignedTask = lastAssignedTasks.get(task.getMaterial());
				
				if (lastAssignedTask == null || 
						task.getDueTime() - lastAssignedTask.getDueTime() > maxTimeToGroupTasks) {
					task.setAssignment(task.getInitialAssignment());
					lastAssignedTasks.put(task.getMaterial(), task);
					task.getInitialAssignment().addInitialTask(task);
				}
				else {
					task.setAssignment(lastAssignedTask.getInitialAssignment());
					lastAssignedTask.getInitialAssignment().addInitialTask(task);
				}	
			}
			
			scoreDirector.afterVariableChanged(task, "assignment");		
		}
	}
}
