package cz.logio.production.planning.initializer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collections;

import org.optaplanner.core.impl.phase.custom.CustomPhaseCommand;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.MachineSetup;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.comparator.PlanningAssignmentDifficultyComparator;
import cz.logio.production.planning.utils.ChangeoverMatrix;

public class PlanningAssignmentInitializingPhaseCommand implements
		CustomPhaseCommand {
	
	ChangeoverMatrix changeoverMatrix;
	Map<Machine,PlanningAssignment> lastAssigned;

	public void changeWorkingSolution(ScoreDirector scoreDirector) {
		System.out.println("NON-INCREMENTAL INITIALIZING");
		ProductionPlanning productionPlanning = (ProductionPlanning) scoreDirector.getWorkingSolution();

		changeoverMatrix = productionPlanning.getChangeoverMatrix();
		
		List<PlanningAssignment> planningAssignments = productionPlanning.getPlanningAssignmentList();
		Collections.sort(planningAssignments, new PlanningAssignmentDifficultyComparator());
		
		lastAssigned = new HashMap<Machine,PlanningAssignment>();
		
		for (PlanningAssignment assignment : planningAssignments) {
			if (assignment.isFixed()) {
				//if(!assignment.isAssigned()) System.out.println("Not assigned assignment " + assignment);
				setTimeStart(scoreDirector,assignment,assignment.getTimeStart());
				setMachineSetup(scoreDirector,assignment,assignment.getMachineSetup());
				for (Machine machine : assignment.getMachineSetup().getMachineList()) {
					lastAssigned.put(machine,assignment);
				}
			}
		}
		
		for (PlanningAssignment assignment : planningAssignments) {
			if (assignment.isFixed()) continue;
			
			setMachineSetup(scoreDirector,assignment,assignment.getAllowedMachineSetupList().get(0));
			
			if (!assignment.isAssigned()) {
				setTimeStart(scoreDirector,assignment,0);	
				setAdditionalTime(scoreDirector,assignment,0);
			}
			else {
					PlanningAssignment previous = assignment.getNeighbourhood().getPreviousPlanningAssignment(assignment.getMachineSetup().getLine());
					if (previous != null && (componentProductionChange(assignment) || !assignment.isRightAfter(previous))) {
						ensureMoq(assignment.getMachineSetup().getLine(),scoreDirector);
					}
					setTimeStart(scoreDirector,assignment,getMinimalTimeStart(assignment));
				}
			
			for (Machine machine : assignment.getMachineSetup().getMachineList()) {
				lastAssigned.put(machine,assignment);
			}
		}
	}
	
	private boolean componentProductionChange(PlanningAssignment assignment) {
		Machine line = assignment.getMachineSetup().getLine();	
		PlanningAssignment last = lastAssigned.get(line);
		
		if (last == null) return false;
		
		Material currentComponent = assignment.getMachineSetup().getComponentMaterial();
		Material previousComponent = last.getMachineSetup().getComponentMaterial();
		return currentComponent != previousComponent;	
	}
	
	private void ensureMoq(Machine line, ScoreDirector scoreDirector) {
		PlanningAssignment firstAssignment = lastAssigned.get(line);
		if (firstAssignment == null) return;
		
		Material component = firstAssignment.getMachineSetup().getComponentMaterial();
		int minimalProductionTime = component.getMoq()/component.getLineCapacityInKg();
		int overallProductionTime = 0;
		int numberOfAssignments = 0;
		
		while (true) { //get first assignment with production of the component in chain
			overallProductionTime += firstAssignment.getNumberOfTimeSteps();
			if (overallProductionTime >= minimalProductionTime) return;
			if (!isMakeToOrder(firstAssignment)) numberOfAssignments++;
			PlanningAssignment previousAssignment = firstAssignment.getNeighbourhood().getPreviousPlanningAssignment(line);
			if (previousAssignment == null || previousAssignment.getMachineSetup().getComponentMaterial() != component || 
					!firstAssignment.isRightAfter(previousAssignment)) break;		
			firstAssignment = previousAssignment;
		}
		
		if (numberOfAssignments == 0) return;
		
		int overallTimeToAddLeft = minimalProductionTime-overallProductionTime;
		int timeToAdd = (int)Math.ceil(overallTimeToAddLeft/numberOfAssignments);
		int addedTime = 0;
		
		PlanningAssignment currentAssignment = firstAssignment;
		while (currentAssignment != null) {
			setTimeStart(scoreDirector,currentAssignment,currentAssignment.getTimeStart() + addedTime);
			
			int adding = Math.min(overallTimeToAddLeft,timeToAdd);	
			setAdditionalTime(scoreDirector,currentAssignment,adding);
			overallTimeToAddLeft -= adding;
			addedTime += adding;
			
			currentAssignment = currentAssignment.getNeighbourhood().getNextPlanningAssignment(line);
		}
	}
	
	private boolean isMakeToOrder(PlanningAssignment assignment) {
		for (Material material : assignment.getMachineSetup().getMaterialList()) {
			if (material.isMakeToOrder()) return true;
		}	
		return false;
	}
	
	private int getMinimalTimeStart(PlanningAssignment assignment) {
		int startTime = 0;	
		for (Machine machine : assignment.getMachineSetup().getMachineList()) {
			PlanningAssignment last = lastAssigned.get(machine);
			Material currentMaterial = assignment.getMachineSetup().getMaterialProducedOnMachine(machine);
			
			if (last != null) {
				Material previousMaterial = last.getMachineSetup().getMaterialProducedOnMachine(machine);
				int switchTime = changeoverMatrix.getChangeover(previousMaterial, currentMaterial, machine);				
				startTime = Math.max(startTime, last.getTimeEnd() + switchTime);
			}
		}
		
		PlanningAssignment previousOnLine = lastAssigned.get(assignment.getMachineSetup().getLine());
		if (previousOnLine != null && (startTime - previousOnLine.getTimeEnd()) > 2) {
			int switchTime = assignment.getMachineSetup().getComponentMaterial().getStartupTime();
			startTime = Math.max(startTime, previousOnLine.getTimeEnd() + switchTime);
		}
		
		return startTime;
	}	
	
	private void setTimeStart(ScoreDirector scoreDirector, PlanningAssignment assignment, int time) {
		scoreDirector.beforeVariableChanged(assignment, "timeStart");
		assignment.setTimeStart(time);
		scoreDirector.afterVariableChanged(assignment, "timeStart");
	}
	
	private void setMachineSetup(ScoreDirector scoreDirector, PlanningAssignment assignment, MachineSetup machineSetup) {
		scoreDirector.beforeVariableChanged(assignment, "machineSetup");
		assignment.setMachineSetup(machineSetup);
		scoreDirector.afterVariableChanged(assignment, "machineSetup");
	}
	
	private void setAdditionalTime(ScoreDirector scoreDirector, PlanningAssignment assignment, int time) {
		scoreDirector.beforeVariableChanged(assignment, "additionalTime");
		assignment.setAdditionalTime(0);
		scoreDirector.afterVariableChanged(assignment, "additionalTime");
	}

}
