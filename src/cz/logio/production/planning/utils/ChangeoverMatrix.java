package cz.logio.production.planning.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.logio.production.planning.domain.*;

public class ChangeoverMatrix {
	
	TimeHandler timeHandler;
	private List<Changeover> changeovers;	
	private Map<Material,Set<Machine>> materialToMachines = new HashMap<Material,Set<Machine>>();
	
	private Map<Material,Map<Material,Map<Machine,Changeover>>> changeoverMap = new HashMap<Material,Map<Material,Map<Machine,Changeover>>>();
	
	public ChangeoverMatrix(TimeHandler timeHandler) {
		this.timeHandler = timeHandler; 
	}
	
	public Set<Machine> getMachines(Material m) {
		Set<Machine> machines = materialToMachines.get(m);
		
		if (machines == null) {
			machines = new HashSet<Machine>();
			for (MachineSetup machineSetup : m.getAllowedMachineSetups()) {
				machines.add(machineSetup.getLine());
				machines.add(machineSetup.getMachine(m));
			}
			materialToMachines.put(m, machines);
		}
		
		return machines;
	}
	
	public void prepareChangeoversFromModel(List<Material> materials) {
		changeovers = new ArrayList<Changeover>();
		
		for (Material m1 : materials) {
			//if (!m1.isFinalMaterial()) continue;
			
			Set<Machine> machines1 = getMachines(m1);
			
			Map<Material,Map<Machine,Changeover>> mapFromMaterial = new HashMap<Material,Map<Machine,Changeover>>();
			changeoverMap.put(m1, mapFromMaterial);
		
			for (Material m2 : materials) {
				//if (!m2.isFinalMaterial()) continue;	
				
				Map<Machine,Changeover> mapToMaterial = new HashMap<Machine,Changeover>();
				mapFromMaterial.put(m2,mapToMaterial);
				
				Set<Machine> machines2 = new HashSet<Machine>(getMachines(m1)); //copy (to use retainAll)	
				machines2.retainAll(machines1);
				
				for (Machine machine : machines2) {		
					Changeover changeover = new Changeover(m1,m2,machine);
					changeovers.add(changeover);
					mapToMaterial.put(machine, changeover);
				}
			}
		}
	}
	
	public int getChangeover(Material from, Material to, Machine machine) {
		Map<Material,Map<Machine,Changeover>> mapFromMaterial = changeoverMap.get(from);
		if (mapFromMaterial == null) return 0;
		Map<Machine,Changeover> mapToMaterial = mapFromMaterial.get(to);
		if (mapToMaterial == null) return 0;
		Changeover changeover = mapToMaterial.get(machine);
		if (changeover == null) return 0;
		return changeover.time;
	}
	
	public List<Changeover> getChangeovers() {
		return changeovers;
	}	
	
	public class Changeover {
		private Material materialFrom;
		private Material materialTo;
		public Machine machine;
		
		int lineSanitationTime = 0;	
		int packerSanitationTime = 0;
		int tubeSwitchTime = 0;
		int packerPreparationTime = 0;
		
		String description = "";
		
		private int time;
		
		public Changeover(Material materialFrom, Material materialTo, Machine machine) {
			this.materialFrom = materialFrom;
			this.materialTo = materialTo;	
			this.machine = machine;
			
			if (machine.isLine()) {
				computeLineChangeoverTimes();
				time = overallLineChangeoverTime();
			}
			else {
				computePackerChangeoverTimes();	
				time = overallPackerChangeoverTime();
			}
		}
		
		private void computeLineChangeoverTimes() {
				Material fromComponent = materialFrom.isFinalMaterial() ? materialFrom.getComponentMaterial() : materialFrom;
				Material toComponent = materialTo.isFinalMaterial() ? materialTo.getComponentMaterial() : materialTo;
				
				if (fromComponent == null || toComponent == null) return;
				
				//cistenie linky ak sa meni zhluk, alebo je nespravne poradie v zhluku
				if (fromComponent.getGroup() != toComponent.getGroup() ||
						fromComponent.getOrder() > toComponent.getOrder()) {
					lineSanitationTime = fromComponent.getGroup().getSanitationTime();		
					if (lineSanitationTime > 0) {
						lineSanitationTime += toComponent.getStartupTime();
					}
				}
		}
		
		private void computePackerChangeoverTimes() {
			if (!materialFrom.isFinalMaterial() || !materialTo.isFinalMaterial()) return;
			
			if (materialFrom != materialTo) {
				//FICL: pokud se balí polotovar ze stejného shluku a tubus zùstává stejný, èas na pøípravu balení je 0
				if (materialFrom.getComponentMaterial().getGroup() != materialTo.getComponentMaterial().getGroup()) {
					packerPreparationTime = materialTo.getStartupTime();
				}
			}
			
			if (materialFrom.getTube() != materialTo.getTube()) {
				tubeSwitchTime = materialTo.getTube().getSwitchTime();
			}
			
			if(materialFrom.getGroup() != materialTo.getGroup() ||
					materialFrom.getOrder() > materialTo.getOrder()) {
				packerSanitationTime = materialTo.getGroup().getSanitationTime();
			}
		}
		
		private int overallLineChangeoverTime() {
			return lineSanitationTime;
		}
		
		private int overallPackerChangeoverTime() {
			return Math.max(packerSanitationTime, Math.max(packerPreparationTime,tubeSwitchTime));
		}
		
		public int getChangeoverTimeInMinutes() {
			if (machine.isLine()) {
				return timeHandler.timeStepsToMinutes(overallLineChangeoverTime());
			}
			else {
				return timeHandler.timeStepsToMinutes(overallPackerChangeoverTime());
			}
		}
	
		public String getChangeoverDescription() {			
			String desc = "";
			
			if (machine.isLine()) {
				desc = "Linka: ";
				if (lineSanitationTime > 0) {
					desc += "sanitacia " + timeHandler.timeStepsToMinutes(lineSanitationTime);
				}
			}
			else {
				desc = "Balicka: ";
				if (packerPreparationTime > 0) {
					desc += " priprava balenia " + timeHandler.timeStepsToMinutes(packerPreparationTime);
				}
				if (tubeSwitchTime > 0) {
					desc += " vymena tubusu " + timeHandler.timeStepsToMinutes(tubeSwitchTime);
				}
				if (packerSanitationTime > 0) {
					desc += " sanitacia " + timeHandler.timeStepsToMinutes(packerSanitationTime);
				}
			}
			
			return desc;
		}
		
		public Material getMaterialFrom() {
			return materialFrom;
		}
		
		public Material getMaterialTo() {
			return materialTo;
		}
	}
	
}
