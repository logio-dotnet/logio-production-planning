package cz.logio.production.planning.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;

public class Neighbourhood {
	private Map<Machine,TreeSet<PlanningAssignment>> trees;
	private Set<Machine> filledTrees = new HashSet<Machine>();
	
	private Map<Machine,PlanningAssignment> previousPlanningAssignments = new HashMap<Machine,PlanningAssignment>();
	private Map<Machine,PlanningAssignment> nextPlanningAssignments = new HashMap<Machine,PlanningAssignment>();
	
	public Map<Machine,TreeSet<PlanningAssignment>> getTrees() {
		return trees;
	}

	public void setTrees(Map<Machine,TreeSet<PlanningAssignment>> trees) {
		this.trees = trees;
	}

	public Map<Machine,PlanningAssignment> getPreviousPlanningAssignments() {
		return previousPlanningAssignments;
	}

	public void setPreviousPlanningAssignments(
			Map<Machine,PlanningAssignment> previousPlanningAssignments) {
		this.previousPlanningAssignments = previousPlanningAssignments;
	}
	
	public PlanningAssignment getPreviousPlanningAssignment(Machine machine) {
		return getPreviousPlanningAssignments().get(machine);
	}
	
	public void setPreviousPlanningAssignment(Machine machine, PlanningAssignment planningAssignment) {
		if (planningAssignment == null) {
			this.getPreviousPlanningAssignments().remove(machine);
		}
		this.getPreviousPlanningAssignments().put(machine,planningAssignment);
	}

	public Map<Machine,PlanningAssignment> getNextPlanningAssignments() {
		return nextPlanningAssignments;
	}

	public void setNextPlanningAssignments(
			Map<Machine,PlanningAssignment> nextPlanningAssignments) {
		this.nextPlanningAssignments = nextPlanningAssignments;
	}
	
	public PlanningAssignment getNextPlanningAssignment(Machine machine) {
		return getNextPlanningAssignments().get(machine);
	}
	
	public void setNextPlanningAssignment(Machine machine, PlanningAssignment planningAssignment) {
		if (planningAssignment == null) {
			this.getNextPlanningAssignments().remove(machine);
		}
		this.getNextPlanningAssignments().put(machine,planningAssignment);
	}
	
	public static Map<Machine,TreeSet<PlanningAssignment>> createTreeStructure(Collection<Machine> machines) {
		Map<Machine,TreeSet<PlanningAssignment>> structure = new HashMap<Machine,TreeSet<PlanningAssignment>>();
		
		for (Machine machine : machines) {
			structure.put(machine, new TreeSet<PlanningAssignment>());
		}
		
		return structure;
	}

	public Set<Machine> getFilledTrees() {
		return filledTrees;
	}

	public void setFilledTrees(Set<Machine> filledTrees) {
		this.filledTrees = filledTrees;
	}
}
