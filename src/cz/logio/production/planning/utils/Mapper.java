package cz.logio.production.planning.utils;

import java.util.HashMap;
import java.util.Map;

import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.Task;

public class Mapper {
	
	public static Map<String,PlanningAssignment> createPlanningAssignmentMap(ProductionPlanning productionPlanning) {
		Map<String,PlanningAssignment> map = new HashMap<String,PlanningAssignment>();	
		for (PlanningAssignment assignment : productionPlanning.getPlanningAssignmentList()) {
			map.put(assignment.getId(), assignment);
		}	
		return map;
	}
	
	public static Map<String,Task> createTaskMap(ProductionPlanning productionPlanning) {
		Map<String,Task> map = new HashMap<String,Task>();	
		for (Task task : productionPlanning.getTaskList()) {
			map.put(task.getId(), task);
		}	
		return map;
	}
}
