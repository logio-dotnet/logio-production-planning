package cz.logio.production.planning.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.time.DateUtils;

public class TimeHandler {
	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat HOUR_FORMAT = new SimpleDateFormat("HH");
	
	private static final long ONE_MINUTE_IN_MILLIS=60000;
	
	public static final int shiftHours = 12;	
	private int[] shiftStarts = {7,19}; //must be ordered
	
	private int startingHour; 
	
	public static int timeStepLength;
	
	private int daysAvailable;
	private int daysOfc;
	private int daysProductionSuggested;
	
	public String pwDate;
	public Date pwDateTime;
	
	public TimeHandler(int timeStepLength, String dateOfReference) {
		this.timeStepLength = timeStepLength;
		this.pwDate = dateOfReference;		
		this.pwDateTime = getDateTime(dateOfReference);
	
		//setStartingHour();
	}
	
	private int getStartingHour() {
		return pwDateTime.getHours();
	}
	
	/*private void setStartingHour() {
		Calendar cal = Calendar.getInstance();
		
		String currentHourString = HOUR_FORMAT.format(cal.getTime());
		startingHour = Integer.parseInt(currentHourString)+1;
	}*/
	
	public int getTimeStepsToNextShift() {
		for (int shiftStart : shiftStarts) {
			if (getStartingHour() <= shiftStart) return hoursToTimeSteps(shiftStart-getStartingHour()); 
		}
		
		return hoursToTimeSteps(24 - getStartingHour() + shiftStarts[0]);
	}
	
	public static Date getDate(String date) {
		try {
			return FORMAT.parse(date);
		} catch (ParseException e) {
			System.out.println("Unable to parse date "+date);
			e.printStackTrace();
		}
		return null;
	}
	
	public static Date getDateTime(String dateTime) {
		try {
			return DATETIME_FORMAT.parse(dateTime);
		} catch (ParseException e) {
			System.out.println("Unable to parse date "+dateTime);
			e.printStackTrace();
		}
		return null;
	}
	
	public Date getStartDate() {
		return pwDateTime;
	}
	
	public Date getEndDate() {
		return getDate(getTimePointsAvailable());
	}
	
	public Date getEndDateOfc() {
		return getDate(getTimePointsOfc());
	}
	
	public Date getEndDateProductionSuggested() {
		return getDate(getTimePointsProductionSuggested());
	}
	
	public int getTimeStepsFromStart(Date date) {
		return (int)TimeUnit.MILLISECONDS.toMinutes(
				date.getTime() - getStartDate().getTime())/timeStepLength;
	}
	
	public Date getDate(int timeSteps) {
		long startTime = getStartDate().getTime();
		return new Date(startTime + (timeStepLength * timeSteps * ONE_MINUTE_IN_MILLIS));
	}
	
	public int hoursToTimeSteps(double hours) {
		return (int)Math.ceil((hours*60/timeStepLength));
	}
	
	public double timeStepsToHours(int timeSteps) {
		return timeSteps*timeStepLength/60.0;
	}
	
	public int timeStepsToMinutes(int timeSteps) {
		return timeSteps*timeStepLength;
	}
	
	public String dateToString(Date date) {
		return FORMAT.format(date);
	}
	
	public int getShiftLengthInTimeSteps() {
		return hoursToTimeSteps(shiftHours);
	}
	
	/**
	 * 1..Monday
	 * 7..Sunday
	 * @param timeSteps
	 * @return
	 */
	public int getDayOfWeek(int timeSteps) {
		Date date = getDate(timeSteps);
		
		SimpleDateFormat dayOfWeekDateFormat = new SimpleDateFormat("u");
		
		return Integer.parseInt(dayOfWeekDateFormat.format(date));	
	}


	public int getDaysProductionSuggested() {
		return daysProductionSuggested;
	}

	public void setDaysProductionSuggested(int daysProductionSuggested) {
		this.daysProductionSuggested = daysProductionSuggested;
	}
	
	public int getTimePointsProductionSuggested() {
		return hoursToTimeSteps(24*daysProductionSuggested);
	}

	public int getDaysOfc() {
		return daysOfc;
	}

	public void setDaysOfc(int daysOfc) {
		this.daysOfc = daysOfc;
	}
	
	public int getTimePointsOfc() {
		return hoursToTimeSteps(24*daysOfc);
	}
	
	public int getDaysAvailable() {
		return daysAvailable;
	}

	public void setDaysAvailable(int daysAvailable) {
		this.daysAvailable = daysAvailable;
	}
	
	public int getTimePointsAvailable() {
		return hoursToTimeSteps(24*daysAvailable);
	}	

	public Date getShiftStartingDate(Date date, int shift) {
		return DateUtils.setHours(date, shiftStarts[shift-1]);
	}
}
