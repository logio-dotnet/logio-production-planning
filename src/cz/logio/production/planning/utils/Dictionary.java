package cz.logio.production.planning.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import cz.logio.production.planning.domain.Group;
import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.Tube;

public class Dictionary {
	private  Map<String,Material> materialNameToMaterialMap = new HashMap<String,Material>();
	private Map<String,Group> groupNameToGroupMap = new HashMap<String,Group>();
	private Map<String,Machine> machineNameToMachineMap = new HashMap<String,Machine>();
	private Map<String,Tube> tubeNameToTubeMap = new HashMap<String,Tube>();
	
	/**
	 * Might return null
	 * @param name
	 * @return
	 */
	public Material getMaterial(String name) {
		return materialNameToMaterialMap.get(name);
	}
	
	public void addMaterial(Material material) {
		materialNameToMaterialMap.put(material.getName(), material);
	}
	
	/**
	 * Might return null
	 * @param name
	 * @return
	 */
	public Group getGroup(String name) {
		return groupNameToGroupMap.get(name);
	}
	
	public void addGroup(Group group) {
		groupNameToGroupMap.put(group.getName(), group);
	}
	
	/**
	 * Might return null
	 * @param name
	 * @return
	 */
	public Machine getMachine(String name) {
		return machineNameToMachineMap.get(name);
	}
	
	public void addMachine(Machine machine) {
		machineNameToMachineMap.put(machine.getName(), machine);
	}
	
	/**
	 * Might return null
	 * @param name
	 * @return
	 */
	public Tube getTube(String name) {
		return tubeNameToTubeMap.get(name);
	}
	
	public void addTube(Tube tube) {
		tubeNameToTubeMap.put(tube.getName(), tube);
	}
	
	public Collection<Machine> getMachines() {
		return machineNameToMachineMap.values();
	}
}
