package cz.logio.production.planning.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbConnector {

	private static final String HOST = "tunel.logio.cz";
	private static final String PORT = "30168";
	private static final String user = "root";
	private static final String pass = "lopata";
	private static final String LOCAL_URL = "jdbc:mysql://localhost:3306/";
	
	private static String url = "jdbc:mysql://" + HOST + ":" + PORT + "/";
	private static String driver = "com.mysql.jdbc.Driver";
	
	private static Connection pwDB = null;
	
	public static String PW_DB = null;
	
	public static String PRODUCTION_INFO_TABLE = "bonavita_product_production_info";
	
	public static Connection getConnectionToPwDB() {
		if (pwDB == null) {
			pwDB = getConnection(PW_DB);
		}	
		return pwDB;
	}
	
	public static void closeConnections() {
		try {
		if (pwDB != null) pwDB.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static Connection getConnection(String db) {	
		System.out.println("Connecting to database " + db);
		
		String encoding = "?useUnicode=true&characterEncoding=UTF-8";

		try {
			try { //local connection
				Class.forName(driver).newInstance();	
				return DriverManager.getConnection(LOCAL_URL+db+encoding,user,pass);
			} catch (SQLException e) {
				System.out.println("Local connection failed, trying to connect to the server");	
			}
			
			try {
				Class.forName(driver).newInstance();	
				return DriverManager.getConnection(url+db+encoding,user,pass);
			} catch (SQLException e) {
				System.out.println("Connection to the server failed");		
			}
		} catch (Exception e) {
			System.out.println("DB connection failed");
		}
		
		return null;
		
	}
	
	public static ResultSet executeFetchAll(String query) {
		try {
			Connection conn = DbConnector.getConnectionToPwDB();	
			Statement st = conn.createStatement();	
			return st.executeQuery(query);
		} catch (Exception e) {
			e.printStackTrace();
		  	}
		
		return null;
	}
	
	public static void exec(String query) {
		try {
			Connection conn = DbConnector.getConnectionToPwDB();
			Statement st = conn.createStatement();
			st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		  	}
	}
}
