package cz.logio.production.planning.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.Shift;
import cz.logio.production.planning.domain.Task;

public class SolutionManager {
	
	private ProductionPlanning solution;
	
	protected Map<Machine,PlanningAssignmentList> machineToAssignmentsOrdered = 
			new HashMap<Machine,PlanningAssignmentList>();
	protected Map<PlanningAssignment,List<Task>> assignmentToTasksMap;
	protected List<PlanningAssignment> sortedAssignmentList;
	
	protected Map<Integer,List<Machine>> timeStepForbiddenMachines;

	public SolutionManager(ProductionPlanning solution) {
		this.solution = solution;
		
		sortedAssignmentList = solution.getPlanningAssignmentList();
	    Collections.sort(sortedAssignmentList);
	    
	    prepareMachineToAssignmentsMap(solution);
	    prepareAssignmentToTasksMap(solution);
	    prepareTimeStepForbiddenMachines(solution);
	}
	
	public List<Task> getTasks(PlanningAssignment assignment) {
		return assignmentToTasksMap.get(assignment);
	}
	
	public PlanningAssignment getPreviousPlanningAssignments(Machine machine,PlanningAssignment assignment) {
		return machineToAssignmentsOrdered.get(machine).getPreviousAssignment(assignment);
	}
	
	public PlanningAssignment getNextPlanningAssignments(Machine machine,PlanningAssignment assignment) {
		return machineToAssignmentsOrdered.get(machine).getNextAssignment(assignment);
	}
	
	public List<PlanningAssignment> getSortedPlanningAssignments() {
		return sortedAssignmentList;
	}
	
	public boolean isInForbiddenShift(int timeStep, Machine machine) {
		return timeStepForbiddenMachines.get(timeStep).contains(machine);
	}
	
	public boolean isInForbiddenShift(PlanningAssignment assignment) {
		//check every shift that the assignment lies in
		int period = TimeHandler.shiftHours*TimeHandler.timeStepLength - 1;
		
		for (int time = assignment.getTimeStart(); time < assignment.getTimeEnd(); time += period) {
			for (Machine machine : assignment.getMachineSetup().getMachineList()) {
				if (isInForbiddenShift(time,machine)) return true;
			}
		}	
		return false;
	}
	
	/******************************************************************************/
	
	private void prepareMachineToAssignmentsMap(ProductionPlanning productionPlanning) {	
		for (Machine m : productionPlanning.getMachineList()) {
	    	machineToAssignmentsOrdered.put(m, new PlanningAssignmentList());
	    }
	    
	    for (PlanningAssignment assignment : sortedAssignmentList) {
	    	if (!assignment.isAssigned()) continue;
	    	
	    	for (Machine m : assignment.getMachineSetup().getMachineList()) {	    		
	    		machineToAssignmentsOrdered.get(m).addNextAssignment(assignment);
	    	}
	    }  
	}
	
	private void prepareAssignmentToTasksMap(ProductionPlanning productionPlanning) {
		
		assignmentToTasksMap = new HashMap<PlanningAssignment, List<Task>>();
		
		for (Task task : productionPlanning.getTaskList()) {
			
			List<Task> tasks = assignmentToTasksMap.get(task.getAssignment());
			
			if (tasks == null) {
				tasks = new ArrayList<Task>();
				assignmentToTasksMap.put(task.getAssignment(),tasks);
			}
			
			tasks.add(task);
		}
	}
	
	private void prepareTimeStepForbiddenMachines(ProductionPlanning productionPlanning) {
		
		timeStepForbiddenMachines = new HashMap<Integer,List<Machine>>();
		
		for (Shift shift : productionPlanning.getShiftList()) {
			List<Machine> forbiddenMachines = shift.getForbiddenMachines();
			for (int time = shift.getTimeStart(); time < shift.getTimeEnd(); time++) {
				timeStepForbiddenMachines.put(time, forbiddenMachines);
			}
		}
	}
	
	/******************************************************************************/
	
	private class PlanningAssignmentList {
		Map<Integer,PlanningAssignment> orderToAssignment = new HashMap<Integer, PlanningAssignment>();
		Map<PlanningAssignment,Integer> assignmentToOrder = new HashMap<PlanningAssignment,Integer>();
		private int current = 0;
		
		public void addNextAssignment(PlanningAssignment assignment) {
			orderToAssignment.put(current, assignment);
			assignmentToOrder.put(assignment, current);
			current++;
		}
		
		/**
		 * Might return null
		 * @param assignment
		 * @return
		 */
		 public PlanningAssignment getNextAssignment(PlanningAssignment assignment) {
			Integer order = assignmentToOrder.get(assignment);
			return order != null ? orderToAssignment.get(order + 1) : null;
		}
		
		public PlanningAssignment getPreviousAssignment(PlanningAssignment assignment) {
			Integer order = assignmentToOrder.get(assignment);
			return order != null ? orderToAssignment.get(order - 1) : null;
		}
		
		public PlanningAssignment getLastAssignment() {
			if (current == 0) return null;		
			return orderToAssignment.get(current-1);
		}
		
	}
	
}
