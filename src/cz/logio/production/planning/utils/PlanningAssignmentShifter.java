package cz.logio.production.planning.utils;

import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;

/**
 * Shifts planning assignments to minimize empty space on machine. Does not consider incompatible groups.
 * 
 * @author Naty
 */
public class PlanningAssignmentShifter {
	
	ProductionPlanning productionPlanning;
	SolutionManager solutionManager;
	ScoreDirector scoreDirector;
	
	public PlanningAssignmentShifter(ScoreDirector scoreDirector) {
		this.scoreDirector = scoreDirector;
		productionPlanning = (ProductionPlanning) scoreDirector.getWorkingSolution();
		solutionManager = new SolutionManager(productionPlanning);
	}
	
	public void shiftPlanningAssignments() {		
		for (PlanningAssignment assignment : solutionManager.sortedAssignmentList) {
			int shift = getMaxShiftDistance(assignment);
			if (shift > 0) {
				tryShift(assignment,shift);
			}
		}
	}
	
	private int getMaxShiftDistance(PlanningAssignment planningAssignment) {
		int maxShiftDistance = planningAssignment.getTimeStart();
		
		for (Machine machine : planningAssignment.getMachineSetup().getMachineList()) {
			PlanningAssignment previousAssignment = solutionManager.getPreviousPlanningAssignments(machine, planningAssignment);
			if (previousAssignment == null) continue;
			
			Material materialFrom = previousAssignment.getMachineSetup().getMaterialProducedOnMachine(machine);
			Material materialTo = planningAssignment.getMachineSetup().getMaterialProducedOnMachine(machine);
			int changeover = productionPlanning.getChangeoverMatrix().getChangeover(materialFrom, materialTo, machine);
			
			maxShiftDistance = Math.min(maxShiftDistance,planningAssignment.getTimeStart() - changeover - previousAssignment.getTimeEnd());
			if (maxShiftDistance < 0) break;
		}
		
		return maxShiftDistance;
	}
	
	private void tryShift(PlanningAssignment planningAssignment, int shift) {
		//shift
		int originalTimeStart = planningAssignment.getTimeStart();
		scoreDirector.beforeVariableChanged(planningAssignment, "timeStart");
		planningAssignment.setTimeStart(originalTimeStart - shift);
		scoreDirector.afterVariableChanged(planningAssignment, "timeStart");
		
		//undo if PA now lies in forbidden shift
		if (solutionManager.isInForbiddenShift(planningAssignment)) {
			scoreDirector.beforeVariableChanged(planningAssignment, "timeStart");
			planningAssignment.setTimeStart(originalTimeStart);
			scoreDirector.afterVariableChanged(planningAssignment, "timeStart");
		}
	}
}
