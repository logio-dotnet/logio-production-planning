package cz.logio.production.planning.listeners;

import java.util.Iterator;
import java.util.Map.Entry;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;

public class PreviousAssignmentTimeEndUpdatingVariableListener implements
		VariableListener<PlanningAssignment> {

	public void beforeEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updatePreviousAssignmentTimeEnd(scoreDirector,entity);
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updatePreviousAssignmentTimeEnd(scoreDirector,entity);
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updatePreviousAssignmentTimeEnd(scoreDirector,entity);
	}
	
	private void updatePreviousAssignmentTimeEnd(ScoreDirector scoreDirector, 
			PlanningAssignment assignment) {
		
		for (Entry next : assignment.getNeighbourhood().getNextPlanningAssignments().entrySet()) {
			PlanningAssignment nextAssignment = (PlanningAssignment) next.getValue();
			Machine machine = (Machine) next.getKey();
			
			if (nextAssignment == null) continue;
			
			Integer previousValue = nextAssignment.getPreviousAssignmentTimeEnd().get(machine);
			if (previousValue == null || previousValue != assignment.getTimeEnd()) {
				scoreDirector.beforeVariableChanged(nextAssignment, "previousAssignmentTimeEnd");
				//System.out.println("Updating previous assignment time end for " + nextAssignment + " to " + assignment.getTimeEnd() + " on " + machine);
				nextAssignment.getPreviousAssignmentTimeEnd().put(machine,assignment.getTimeEnd());
				scoreDirector.afterVariableChanged(nextAssignment, "previousAssignmentTimeEnd");
			}
		}
		
		boolean variableChanged = false;
		
		for (Entry previous : assignment.getNeighbourhood().getPreviousPlanningAssignments().entrySet()) {
			PlanningAssignment previousAssignment = (PlanningAssignment) previous.getValue();
			Machine machine = (Machine) previous.getKey();
			
			Integer previousValue = assignment.getPreviousAssignmentTimeEnd().get(machine);
			Integer currentValue = previousAssignment == null ? null : previousAssignment.getTimeEnd();
			if (!areEqual(previousValue,currentValue)) {
				if (!variableChanged) {
					variableChanged = true;
					scoreDirector.beforeVariableChanged(assignment, "previousAssignmentTimeEnd");
				}
				//System.out.println("Updating previous assignment time end for " + assignment + " to " + currentValue + " on " + machine);

				assignment.getPreviousAssignmentTimeEnd().put(machine,currentValue);
			}
		}
		
		Iterator<Entry<Machine, Integer>> it = assignment.getPreviousAssignmentTimeEnd().entrySet().iterator();
		while (it.hasNext()){
			Machine machine = it.next().getKey();

			if (assignment.getNeighbourhood().getPreviousPlanningAssignment(machine) == null) {
				if (!variableChanged) {
					variableChanged = true;
					scoreDirector.beforeVariableChanged(assignment, "previousAssignmentTimeEnd");
				}
				//System.out.println("Updating previous assignment time end for " + assignment + " to null on " + machine);
				it.remove();
			}
		}
		
		if (variableChanged) {
			scoreDirector.afterVariableChanged(assignment, "previousAssignmentTimeEnd");
		}		
	}
	
	private boolean areEqual(Integer i1, Integer i2) {
		return (i1 == null && i2 == null) || (i1 != null && i2 != null 
				&& i1.intValue() == i2.intValue());
	}

}
