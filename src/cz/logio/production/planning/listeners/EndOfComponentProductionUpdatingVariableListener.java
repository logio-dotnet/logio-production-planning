package cz.logio.production.planning.listeners;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;

public class EndOfComponentProductionUpdatingVariableListener implements
		VariableListener<PlanningAssignment> {

	public void beforeEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateEndOfComponentProduction(scoreDirector,entity);
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateEndOfComponentProduction(scoreDirector,entity);
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateEndOfComponentProduction(scoreDirector,entity);
	}
	
	private void updateEndOfComponentProduction(ScoreDirector scoreDirector,
			PlanningAssignment assignment) {
		
		if (assignment.getMachineSetup() == null) {
			setEndOfComponentProduction(scoreDirector,assignment,false);
			return;
		}
		
		Machine line = assignment.getMachineSetup().getLine();
		PlanningAssignment next = assignment.getNeighbourhood().getNextPlanningAssignment(line);
		if (next == null || next.getComponentProductionTimeSoFar() == 0) {
			setEndOfComponentProduction(scoreDirector,assignment,true);
		}
		else {
			setEndOfComponentProduction(scoreDirector,assignment,false);
		}
		
		PlanningAssignment previous = assignment.getNeighbourhood().getPreviousPlanningAssignment(line);
		if (previous != null) {
			if (assignment.getComponentProductionTimeSoFar() > 0) {
				setEndOfComponentProduction(scoreDirector,previous,false);
			}
			else {
				setEndOfComponentProduction(scoreDirector,previous,true);
			}
		}
	}
	
	private void setEndOfComponentProduction(ScoreDirector scoreDirector, 
			PlanningAssignment assignment, boolean value) {
		if (assignment.isEndOfComponentProduction() != value) {
			scoreDirector.beforeVariableChanged(assignment, "endOfComponentProduction");
			assignment.setEndOfComponentProduction(value);
			scoreDirector.afterVariableChanged(assignment, "endOfComponentProduction");
		}
	}

}
