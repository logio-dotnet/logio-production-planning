package cz.logio.production.planning.listeners;

import java.util.TreeSet;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;

public class NeighbourhoodUpdatingVariableListener implements
		VariableListener<PlanningAssignment> {

	public void beforeEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		if (entity.isAssigned()) {
			addToTrees(scoreDirector,entity);
		}
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		if (entity.isAssigned()) {
			removeFromTrees(scoreDirector,entity);
		}
	}

	public void afterVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		if (entity.isAssigned()) {
			addToTrees(scoreDirector,entity);
		}
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		if (entity.isAssigned()) {
			removeFromTrees(scoreDirector,entity);
		}
	}

	public void afterEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
	}
	
	private void addToTrees(ScoreDirector scoreDirector, PlanningAssignment planningAssignment) {
		
		if (planningAssignment.getMachineSetup() != null) {
			
			scoreDirector.beforeVariableChanged(planningAssignment,"neighbourhood");
			for (Machine machine : planningAssignment.getMachineSetup().getMachineList()) {
				TreeSet<PlanningAssignment> tree = planningAssignment.getNeighbourhood().getTrees().get(machine);
				tree.add(planningAssignment);
				planningAssignment.getNeighbourhood().getFilledTrees().add(machine);
							
				PlanningAssignment previous = tree.lower(planningAssignment);
				planningAssignment.getNeighbourhood().setPreviousPlanningAssignment(machine, previous);
				if (previous != null) {
					scoreDirector.beforeVariableChanged(previous,"neighbourhood");
					//if (machine.isLine()) System.out.println(previous + "->" + planningAssignment + "(" + machine.getName() + ")");
					previous.getNeighbourhood().setNextPlanningAssignment(machine, planningAssignment);
					scoreDirector.afterVariableChanged(previous,"neighbourhood");
				}
				
				PlanningAssignment next = tree.higher(planningAssignment);
				
				planningAssignment.getNeighbourhood().setNextPlanningAssignment(machine, next);
				if (next != null) {
					scoreDirector.beforeVariableChanged(next,"neighbourhood");
					//if (machine.isLine()) System.out.println(planningAssignment + "->" + next + "(" + machine.getName() + ")");
					next.getNeighbourhood().setPreviousPlanningAssignment(machine, planningAssignment);
					scoreDirector.afterVariableChanged(next,"neighbourhood");
				}
			}
			
			scoreDirector.afterVariableChanged(planningAssignment,"neighbourhood");
		}
	}
	
	private void removeFromTrees(ScoreDirector scoreDirector, PlanningAssignment planningAssignment) {
		
		if (planningAssignment.getMachineSetup() != null) {		
			scoreDirector.beforeVariableChanged(planningAssignment,"neighbourhood");
			
			for (Machine machine : planningAssignment.getMachineSetup().getMachineList()) {
				TreeSet<PlanningAssignment> tree = planningAssignment.getNeighbourhood().getTrees().get(machine);
					
				PlanningAssignment previous = planningAssignment.getNeighbourhood().getPreviousPlanningAssignment(machine);
				PlanningAssignment next = planningAssignment.getNeighbourhood().getNextPlanningAssignment(machine);
				
				if (previous != null) {
					scoreDirector.beforeVariableChanged(previous,"neighbourhood");
					//if (machine.isLine()) System.out.println(previous + "->" + next + "(" + machine.getName() + ")");
					previous.getNeighbourhood().setNextPlanningAssignment(machine, next);
					scoreDirector.afterVariableChanged(previous,"neighbourhood");
				}

				if (next != null) {
					scoreDirector.beforeVariableChanged(next,"neighbourhood");
					next.getNeighbourhood().setPreviousPlanningAssignment(machine, previous);
					scoreDirector.afterVariableChanged(next,"neighbourhood");
				}
				
				planningAssignment.getNeighbourhood().setPreviousPlanningAssignment(machine, null);
				planningAssignment.getNeighbourhood().setNextPlanningAssignment(machine, null);
				tree.remove(planningAssignment);
				planningAssignment.getNeighbourhood().getFilledTrees().remove(machine);
			}
			
			scoreDirector.afterVariableChanged(planningAssignment,"neighbourhood");
		}
	}

}
