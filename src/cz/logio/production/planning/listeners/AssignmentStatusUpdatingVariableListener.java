package cz.logio.production.planning.listeners;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.PlanningAssignment;

public class AssignmentStatusUpdatingVariableListener implements
		VariableListener<PlanningAssignment> {

	public void beforeEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void beforeVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		
		if ((shouldBeAssigned(entity) && !entity.isAssigned()) ||
				(!shouldBeAssigned(entity) && entity.isAssigned())	) {		
			scoreDirector.beforeVariableChanged(entity, "assigned");
			entity.setAssigned(!entity.isAssigned());
			scoreDirector.afterVariableChanged(entity, "assigned");			
		}
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}
	
	private boolean shouldBeAssigned(PlanningAssignment assignment) {
		return (assignment.getAssignedTasksCount() > 0 && assignment.getAdditionalTime() > 0) || assignment.getAmountSum() > 0;
	}

}
