package cz.logio.production.planning.listeners;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.PlanningAssignment;

public class TimeEndUpdatingVariableListener implements
		VariableListener<PlanningAssignment> {

	public void beforeEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateTimeEnd(scoreDirector,entity);
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateTimeEnd(scoreDirector,entity);
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateTimeEnd(scoreDirector,entity);
	}

	private void updateTimeEnd(ScoreDirector scoreDirector, PlanningAssignment entity) {
		int timeEnd = entity.getTimeStart() + Math.max(0,entity.getNumberOfTimeSteps() + entity.getAdditionalTime()); 
		
		if (timeEnd != entity.getTimeEnd()) {
			scoreDirector.beforeVariableChanged(entity, "timeEnd");	
			entity.setTimeEnd(timeEnd);
			scoreDirector.afterVariableChanged(entity, "timeEnd");
		}
	}
	
	
}
