package cz.logio.production.planning.listeners;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;

public class ComponentProductionTimeUpdatingVariableListener implements
		VariableListener<PlanningAssignment> {

	public void beforeEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		recomputeComponentProductionTime(scoreDirector,entity);
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
	}

	public void afterVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		recomputeComponentProductionTime(scoreDirector,entity);
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
	}

	public void afterEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		recomputeComponentProductionTime(scoreDirector,entity);
	}
	
	private void recomputeComponentProductionTime(ScoreDirector scoreDirector, 
			PlanningAssignment assignment) {
		if (assignment.getMachineSetup() == null) {
			setNewTime(scoreDirector,assignment,0);		
			updateNextAssignments(scoreDirector,assignment,0);
		}
		else {
			Machine line = assignment.getMachineSetup().getLine();
			Material component = assignment.getMachineSetup().getComponentMaterial();
			
			int productionTimeBefore = 0;	
			
			PlanningAssignment previous = assignment.getNeighbourhood().getPreviousPlanningAssignment(line);
	
			if (previous != null && previous.getMachineSetup() != null &&
					previous.getMachineSetup().getComponentMaterial() == component &&
					assignment.isRightAfter(previous)) {
				//System.out.println("Recomputing component production time " + assignment + " (previous " + previous + ")");
				productionTimeBefore = previous.getNumberOfTimeSteps() + previous.getAdditionalTime() + previous.getComponentProductionTimeSoFar();
			}	
			
			setNewTime(scoreDirector,assignment,productionTimeBefore);	
			updateNextAssignments(scoreDirector, assignment, productionTimeBefore + assignment.getNumberOfTimeSteps() + assignment.getAdditionalTime());
		}
	}
	
	/**
	 * Update component production time for the following planning assignments
	 * 
	 * @param assignment
	 * @param time
	 */
	private void updateNextAssignments(ScoreDirector scoreDirector, PlanningAssignment assignment, int time) {	
		
		if (assignment.getMachineSetup() == null) return;
		
		Machine line = assignment.getMachineSetup().getLine();
		Material component = assignment.getMachineSetup().getComponentMaterial();
		
		int productionTime = time;
		PlanningAssignment current = assignment; 
		PlanningAssignment next = assignment.getNeighbourhood().getNextPlanningAssignment(line);
		//System.out.println("Updating next assignment " + next +  " component production time to ");
		while (next != null && next.getMachineSetup() != null && 
				next.getMachineSetup().getComponentMaterial() == component) {
			if (!next.isRightAfter(current)) {
				productionTime = 0;
			}
				
			setNewTime(scoreDirector,next,productionTime);
			
			productionTime += next.getNumberOfTimeSteps() + next.getAdditionalTime();
			current = next;
			next = next.getNeighbourhood().getNextPlanningAssignment(line);
		}
	}
	
	private void setNewTime(ScoreDirector scoreDirector, PlanningAssignment assignment, int time) {
		if (assignment.getComponentProductionTimeSoFar() != time) {
			scoreDirector.beforeVariableChanged(assignment,"componentProductionTimeSoFar");
			assignment.setComponentProductionTimeSoFar(time);
			scoreDirector.afterVariableChanged(assignment,"componentProductionTimeSoFar");
		}
	}

}
