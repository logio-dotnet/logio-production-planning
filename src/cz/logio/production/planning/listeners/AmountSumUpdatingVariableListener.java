package cz.logio.production.planning.listeners;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Task;

public class AmountSumUpdatingVariableListener implements
		VariableListener<Task> {

	public void beforeEntityAdded(ScoreDirector scoreDirector, Task entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector, Task entity) {
		addAmount(scoreDirector,entity);
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector, Task entity) {
		removeAmount(scoreDirector,entity);
	}

	public void afterVariableChanged(ScoreDirector scoreDirector, Task entity) {
		addAmount(scoreDirector,entity);
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector, Task entity) {
		removeAmount(scoreDirector,entity);
	}

	public void afterEntityRemoved(ScoreDirector scoreDirector, Task entity) {}
	
	protected void removeAmount(ScoreDirector scoreDirector, Task task) {
		if (task.getAssignment() != null) {
			scoreDirector.beforeVariableChanged(task.getAssignment(), "amountSum");
			
			task.getAssignment().addAmountSum(-1*task.getAmount());

			scoreDirector.afterVariableChanged(task.getAssignment(), "amountSum");
		}
	}
	
	protected void addAmount(ScoreDirector scoreDirector, Task task) {
		if (task.getAssignment() != null) {
			scoreDirector.beforeVariableChanged(task.getAssignment(), "amountSum");
			
			task.getAssignment().addAmountSum(task.getAmount());
			
			scoreDirector.afterVariableChanged(task.getAssignment(), "amountSum");
		}
	}
}
