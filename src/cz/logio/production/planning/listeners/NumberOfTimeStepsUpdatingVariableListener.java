package cz.logio.production.planning.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.MachineSetup;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;

public class NumberOfTimeStepsUpdatingVariableListener implements
		VariableListener<PlanningAssignment> {

	public void beforeEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		recomputeTimeSteps(scoreDirector,entity);
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		recomputeTimeSteps(scoreDirector,entity);
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		recomputeTimeSteps(scoreDirector,entity);
	}
	
	private void recomputeTimeSteps(ScoreDirector scoreDirector,
			PlanningAssignment assignment) {
		
		if (assignment.getMachineSetup() == null) {
			setNewNumberOfTimeSteps(scoreDirector,assignment,0);
			return;
		}
		
		int time = 0;	

		for (Entry<Material,Integer> entry : assignment.getAmountMap().entrySet()) {
			Material material = entry.getKey();
			int amount = roundToPalettes(material,Math.max(entry.getValue(),material.getMoq()));
			
			int capacity = assignment.getMachineSetup().getCapacity(material);
			int newTime = capacity == 0 ? 0 : (int)Math.ceil((double)amount/capacity);
						
			time = Math.max(time, newTime); 
		}	

		setNewNumberOfTimeSteps(scoreDirector,assignment,time);	
	}
	
	private int roundToPalettes(Material material,double amount) {
		return (int)(material.getPaletteAmount()*(Math.ceil(amount/material.getPaletteAmount())));
	}
	
	
	
	private void setNewNumberOfTimeSteps(ScoreDirector scoreDirector, 
			PlanningAssignment assignment, int timeSteps) {	
		
		if (assignment.getNumberOfTimeSteps() != timeSteps) {
			scoreDirector.beforeVariableChanged(assignment, "numberOfTimeSteps");
			assignment.setNumberOfTimeSteps(timeSteps);
			scoreDirector.afterVariableChanged(assignment, "numberOfTimeSteps");
		}
	}
	
}
