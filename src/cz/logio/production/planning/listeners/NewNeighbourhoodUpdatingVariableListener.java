package cz.logio.production.planning.listeners;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.utils.Neighbourhood;

public class NewNeighbourhoodUpdatingVariableListener implements VariableListener<PlanningAssignment>{

	public void beforeEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateNeighbourhood(scoreDirector,entity);
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterVariableChanged(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateNeighbourhood(scoreDirector,entity);
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {}

	public void afterEntityRemoved(ScoreDirector scoreDirector,
			PlanningAssignment entity) {
		updateNeighbourhood(scoreDirector,entity);
	}
	
	private void updateNeighbourhood(ScoreDirector scoreDirector, PlanningAssignment planningAssignment) {
		
		if (!planningAssignment.isAssigned() || planningAssignment.getMachineSetup() == null) {			
			clearNeighbourhood(scoreDirector,planningAssignment);
			return;
		}
		
		boolean variableChanged = false;
		
		Neighbourhood neighbourhood = planningAssignment.getNeighbourhood();
		
		Iterator<Machine> it = planningAssignment.getNeighbourhood().getFilledTrees().iterator();
		while (it.hasNext()) {
			Machine machine = it.next();
		
			if (!planningAssignment.getMachineSetup().getMachineList().contains(machine)) {
				TreeSet<PlanningAssignment> tree = neighbourhood.getTrees().get(machine);
				tree.remove(planningAssignment);
				
				PlanningAssignment originalPrevious = neighbourhood.getPreviousPlanningAssignment(machine);
				PlanningAssignment originalNext = neighbourhood.getNextPlanningAssignment(machine);
				
				setNext(scoreDirector,originalPrevious,originalNext,machine);
				setPrevious(scoreDirector,originalPrevious,originalNext,machine);
				
				if (neighbourhood.getNextPlanningAssignment(machine) != null) {
					if (!variableChanged) {
						variableChanged = true;
						scoreDirector.beforeVariableChanged(planningAssignment,"neighbourhood");
					}
					neighbourhood.setNextPlanningAssignment(machine, null);
				}
				
				if (neighbourhood.getPreviousPlanningAssignment(machine) != null) {
					if (!variableChanged) {
						variableChanged = true;
						scoreDirector.beforeVariableChanged(planningAssignment,"neighbourhood");
					}
					neighbourhood.setPreviousPlanningAssignment(machine, null);
				}			
			}
		}
		
		for (Machine machine : planningAssignment.getMachineSetup().getMachineList()) {
			TreeSet<PlanningAssignment> tree = neighbourhood.getTrees().get(machine);
			tree.remove(planningAssignment);
			tree.add(planningAssignment);
			planningAssignment.getNeighbourhood().getFilledTrees().add(machine);
			
			PlanningAssignment previous = tree.lower(planningAssignment);
			PlanningAssignment originalPrevious = neighbourhood.getPreviousPlanningAssignment(machine);
			PlanningAssignment originalNext = neighbourhood.getNextPlanningAssignment(machine);
			
			if (!areEqual(previous,originalPrevious)) {
				if (!variableChanged) {
					variableChanged = true;
					scoreDirector.beforeVariableChanged(planningAssignment,"neighbourhood");
				}
				
				setNext(scoreDirector,originalPrevious,originalNext,machine);
				setPrevious(scoreDirector,originalPrevious,originalNext,machine);
				
				PlanningAssignment next = previous == null ? tree.higher(planningAssignment) : 
					previous.getNeighbourhood().getNextPlanningAssignment(machine);
				
				setNext(scoreDirector,previous,planningAssignment,machine);
				setPrevious(scoreDirector,planningAssignment,next,machine);
				
				//bez scoredirectora
				neighbourhood.setNextPlanningAssignment(machine, next);
				neighbourhood.setPreviousPlanningAssignment(machine, previous);
			}
		}
		
		
	}
	
	private void clearNeighbourhood(ScoreDirector scoreDirector,PlanningAssignment planningAssignment) {
		System.out.println("clearing neighbourhood for " + planningAssignment);
		
		if (planningAssignment.getNeighbourhood().getNextPlanningAssignments().isEmpty() &&
			planningAssignment.getNeighbourhood().getPreviousPlanningAssignments().isEmpty()) {
			return;
		}
		
		scoreDirector.beforeVariableChanged(planningAssignment,"neighbourhood");
		
		Neighbourhood neighbourhood = planningAssignment.getNeighbourhood();
			
		for (TreeSet<PlanningAssignment> tree : neighbourhood.getTrees().values()) {
			tree.remove(planningAssignment);
		}
		planningAssignment.getNeighbourhood().getFilledTrees().clear();
		

		for (Entry next : neighbourhood.getNextPlanningAssignments().entrySet()) {
			Machine machine = (Machine) next.getKey();
			PlanningAssignment nextAssignment = (PlanningAssignment) next.getValue();			
			PlanningAssignment previousAssignment = neighbourhood.getPreviousPlanningAssignment(machine);
			
			setPrevious(scoreDirector,previousAssignment,nextAssignment,machine);	
			
			setNext(scoreDirector,previousAssignment,nextAssignment,machine);
			neighbourhood.getPreviousPlanningAssignments().remove(machine);			
		}
		
		for (Entry previous : neighbourhood.getPreviousPlanningAssignments().entrySet()) {
			Machine machine = (Machine) previous.getKey();		
			PlanningAssignment previousAssignment = (PlanningAssignment) previous.getValue();
			
			setNext(scoreDirector,previousAssignment,null,machine);		
		}	
		
		neighbourhood.getNextPlanningAssignments().clear();
		neighbourhood.getPreviousPlanningAssignments().clear();
		
		scoreDirector.afterVariableChanged(planningAssignment,"neighbourhood");
	}
	
	/**
	 * returns true if both planning assignment are null or if they are equal
	 * @param assignment1
	 * @param assignment2
	 * @return
	 */
	private boolean areEqual(PlanningAssignment assignment1, PlanningAssignment assignment2) {
		return (assignment1 == null && assignment2 == null) || assignment1 == assignment2;
	}
	
	private void setNext(ScoreDirector scoreDirector, PlanningAssignment previous, 
			PlanningAssignment next, Machine machine) {		
		if (previous != null) {
			if (areEqual(next,previous.getNeighbourhood().getNextPlanningAssignment(machine))) {
				return;
			}
			
			scoreDirector.beforeVariableChanged(previous,"neighbourhood");
			previous.getNeighbourhood().setNextPlanningAssignment(machine, next);
			scoreDirector.afterVariableChanged(previous,"neighbourhood");
		}
	}
	
	private void setPrevious(ScoreDirector scoreDirector, PlanningAssignment previous, 
			PlanningAssignment next, Machine machine) {		
		if (next != null) {
			if (areEqual(previous,next.getNeighbourhood().getPreviousPlanningAssignment(machine))) {
				return;
			}
			
			scoreDirector.beforeVariableChanged(next,"neighbourhood");
			next.getNeighbourhood().setPreviousPlanningAssignment(machine, previous);
			scoreDirector.afterVariableChanged(next,"neighbourhood");
		}
	}
}
