package cz.logio.production.planning.listeners;

import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Task;

public class AmountUpdatingVariableListener implements
		VariableListener<Task> {

	public void beforeEntityAdded(ScoreDirector scoreDirector, Task entity) {}

	public void afterEntityAdded(ScoreDirector scoreDirector, Task entity) {
		addAmount(scoreDirector,entity);
	}

	public void beforeVariableChanged(ScoreDirector scoreDirector, Task entity) {
		removeAmount(scoreDirector,entity);
	}

	public void afterVariableChanged(ScoreDirector scoreDirector, Task entity) {
		addAmount(scoreDirector,entity);
	}

	public void beforeEntityRemoved(ScoreDirector scoreDirector, Task entity) {
		removeAmount(scoreDirector,entity);
	}

	public void afterEntityRemoved(ScoreDirector scoreDirector, Task entity) {}
	
	protected void removeAmount(ScoreDirector scoreDirector, Task task) {
		if (task.getAssignment() != null) {
			scoreDirector.beforeVariableChanged(task.getAssignment(), "amountMap");		
			task.getAssignment().addAmount(task.getMaterial(),-1*task.getAmount());
			scoreDirector.afterVariableChanged(task.getAssignment(), "amountMap");
		}
	}
	
	protected void addAmount(ScoreDirector scoreDirector, Task task) {
		if (task.getAssignment() != null) {
			scoreDirector.beforeVariableChanged(task.getAssignment(), "amountMap");		
			task.getAssignment().addAmount(task.getMaterial(),task.getAmount());			
			scoreDirector.afterVariableChanged(task.getAssignment(), "amountMap");
		}
	}

}
