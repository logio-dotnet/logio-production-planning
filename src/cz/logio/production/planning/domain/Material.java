package cz.logio.production.planning.domain;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Material")
public class Material {
	private String name;
	
	private Group group;	
	private int order;
	
	private Machine primaryPacker;
	private Machine secondaryPacker;
	private Machine line;
	private Machine secondaryLine;

	private List<MachineSetup> allowedMachineSetups = new ArrayList<MachineSetup>();
	
	private List<Integer> availableTimeRanges;
	
	private Tube tube;	
	
	private int startupTime = 0;
		
	private boolean finalMaterial;
	
	private Material componentMaterial;
	
	private double weight;
	
	private boolean makeToOrder = false;
	
	private int moq = 1;
	
	private int paletteAmount = 1;
	
	private Integer lineCapacityInKg;
	
	/**
	 * Priemerny takt, pouzite iba pri penalizacii za vyrobenie nad max/ pod min
	 */
	private int avgCapacity = -1;
	
	private int makeToMin = 0;
	private int makeToMax = 0;
	private double priority = 0;
	/**
	 * Cim je mensia povodna priorita, tym horsie je ak sa vyraba nad maximum a naopak
	 */
	private double reversedPriority = 0;
	
	public Material(){
	}
	
	public Material(String name){
		this.name = name;
	}
	
	public List<MachineSetup> getAllowedMachineSetups() {	
		return allowedMachineSetups;
	}
	
	public void addAllowedMachineSetup(MachineSetup machineSetup) {
		allowedMachineSetups.add(machineSetup);
	}
	
	public List<MachineSetup> getSimpleMachineSetups() {
		List<MachineSetup> simpleMachineSetups = new ArrayList<MachineSetup>();
		
		for (MachineSetup machineSetup : getAllowedMachineSetups()) {
			if (machineSetup.getMachineList().size() <= 2) {
				simpleMachineSetups.add(machineSetup);
			}
		}
		
		return simpleMachineSetups;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setGroup(Group group) {
		this.group = group;
	}
	
	public Group getGroup() {
		return this.group;
	}
	
	public void setOrder(int order) {
		this.order = order;
	}
	
	public int getOrder(){
		return this.order;
	}
	
	public void setStartupTime(int startupTime) {
		this.startupTime = startupTime;
	}
	
	public int getStartupTime() {
		return this.startupTime;
	}
	
	public boolean hasPrimaryLine(MachineSetup machineSetup) {
		return machineSetup.getLine() == getLine();
	}
	
	public boolean hasPrimaryPacker(MachineSetup machineSetup) {
		return machineSetup.getMachine(this) == getPrimaryPacker();
	}
	
	public boolean isPrimaryMachineSetup(MachineSetup machineSetup) {
		return (hasPrimaryPacker(machineSetup) && hasPrimaryLine(machineSetup));
	}

	/**
	 * Might return null (for component material)
	 * @return
	 */
	public Tube getTube() {
		return tube;
	}

	public void setTube(Tube tube) {
		this.tube = tube;
	}

	public boolean isFinalMaterial() {
		return finalMaterial;
	}

	public void setFinalMaterial(boolean finalMaterial) {
		this.finalMaterial = finalMaterial;
	}

	/**
	 * Might return null (for component material)
	 * @return
	 */
	public Material getComponentMaterial() {
		return componentMaterial;
	}

	public void setComponentMaterial(Material componentMaterial) {
		this.componentMaterial = componentMaterial;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Machine getPrimaryPacker() {
		return primaryPacker;
	}

	public void setPrimaryPacker(Machine primaryPacker) {
		this.primaryPacker = primaryPacker;
	}

	/**
	 * @return might return null
	 */
	public Machine getSecondaryPacker() {
		return secondaryPacker;
	}

	public void setSecondaryPacker(Machine secondaryPacker) {
		this.secondaryPacker = secondaryPacker;
	}

	public Machine getLine() {
		return line;
	}

	public void setLine(Machine line) {
		this.line = line;
	}

	public Machine getSecondaryLine() {
		return secondaryLine;
	}

	public void setSecondaryLine(Machine secondaryLine) {
		this.secondaryLine = secondaryLine;
	}

	public boolean isMakeToOrder() {
		return makeToOrder;
	}

	public void setMakeToOrder(boolean makeToOrder) {
		this.makeToOrder = makeToOrder;
	}

	public int getMoq() {
		return moq;
	}

	public void setMoq(int moq) {
		this.moq = moq;
	}

	public Integer getLineCapacityInKg() {
		return lineCapacityInKg;
	}

	public void setLineCapacityInKg(Integer lineCapacityInKg) {
		if (!isFinalMaterial()) {
			this.lineCapacityInKg = lineCapacityInKg;
		}
	}
	
	public int getMakeToMin() {
		return makeToMin;
	}

	public void setMakeToMin(int makeToMin) {
		this.makeToMin = makeToMin;
	}

	public int getMakeToMax() {
		return makeToMax;
	}

	public void setMakeToMax(int makeToMax) {
		this.makeToMax = makeToMax;
	}

	public String toString() {
		return getName();
	}

	public int getAvgCapacity() {
		if (avgCapacity == -1) {
			int capacitySum = 0;
			for (MachineSetup machineSetup : getAllowedMachineSetups()) {
				capacitySum += machineSetup.getCapacity(this);
			}
			avgCapacity = capacitySum/getAllowedMachineSetups().size();
		}
			
		return avgCapacity;
	}

	public void setAvgCapacity(int avgCapacity) {
		this.avgCapacity = avgCapacity;
	}

	public double getPriority() {
		return priority;
	}

	public void setPriority(double priority) {
		this.priority = priority;
	}

	public int getPaletteAmount() {
		return paletteAmount;
	}

	public void setPaletteAmount(int paletteAmount) {
		this.paletteAmount = paletteAmount;
	}

	public double getReversedPriority() {
		return reversedPriority;
	}

	public void setReversedPriority(double reversedPriority) {
		this.reversedPriority = reversedPriority;
	}
	
	public List<Integer> getAvailableTimeRanges(int lowerBound, int upperBound) {
		if (availableTimeRanges == null) {
			availableTimeRanges = new ArrayList<Integer>();
			
			int initialMaterialCapacity = getMaxCapacity(); //approximate
			int palette = 1;
			int paletteAmount = getPaletteAmount();
			int paletteRoundedTime = (int)Math.ceil((palette*paletteAmount)/(double)initialMaterialCapacity);
			boolean timeChanged = true;
			
			while (paletteRoundedTime <= upperBound) {
				if (paletteRoundedTime >= lowerBound && timeChanged) {
					availableTimeRanges.add(paletteRoundedTime);
				}
				palette++;
				int newPaletteRoundedTime = (int)Math.ceil((palette*paletteAmount)/(double)initialMaterialCapacity);
				if (newPaletteRoundedTime == paletteRoundedTime) {
					timeChanged = false;
				}
				else {
					timeChanged = true;
					paletteRoundedTime = newPaletteRoundedTime;
				}
			}
		}
		return availableTimeRanges;
	}
	
	public int getMaxCapacity() {
		int capacity = 0;
		
		for (MachineSetup ms : getAllowedMachineSetups()) {
			capacity = Math.max(capacity, ms.getCapacity(this));
		}
		
		return capacity;
	}

	public void setAvailableTimeRanges(List<Integer> availableTimeRanges) {
		this.availableTimeRanges = availableTimeRanges;
	}
	
}
