package cz.logio.production.planning.domain;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Group")
public class Group {
	
	private String name;
	
	private int sanitationTime;
	
	private int sanitationWorkers;
	
	private int sanitationPeriod;
	
	private int periodicalSanitationTime;
	
	private List<Group> incompatibleGroups = new ArrayList<Group>();
	
	public Group() {}
	
	public Group(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String groupName) {
		this.name = groupName;
	}
	
	public int getSanitationTime() {
		return sanitationTime;
	}
	
	public void setSanitationTime(int cleanupTime) {
		this.sanitationTime = cleanupTime;
	}
	
	public int getSanitationPeriod() {
		return sanitationPeriod;
	}
	
	public void setSanitationPeriod(int sanitationPeriod) {
		this.sanitationPeriod = sanitationPeriod;
	}

	public int getPeriodicalSanitationTime() {
		return periodicalSanitationTime;
	}

	public void setPeriodicalSanitationTime(int periodicalSanitationTime) {
		this.periodicalSanitationTime = periodicalSanitationTime;
	}

	public int getSanitationWorkers() {
		return sanitationWorkers;
	}

	public void setSanitationWorkers(int sanitationWorkers) {
		this.sanitationWorkers = sanitationWorkers;
	}

	public List<Group> getIncompatibleGroups() {
		return incompatibleGroups;
	}

	public void setIncompatibleGroups(List<Group> incompatibleGroups) {
		this.incompatibleGroups = incompatibleGroups;
	}
	
	public void addIncompatibleGroup(Group group) {
		if (!this.incompatibleGroups.contains(group)) {
			this.incompatibleGroups.add(group);
		}
	}	
}
