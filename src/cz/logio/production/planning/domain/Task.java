package cz.logio.production.planning.domain;

import java.util.ArrayList;
import java.util.List;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.solution.cloner.PlanningCloneable;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cz.logio.production.planning.filter.MovableTaskSelectionFilter;

//@PlanningEntity(difficultyComparatorClass = TaskDifficultyComparator.class)
@PlanningEntity(movableEntitySelectionFilter = MovableTaskSelectionFilter.class)
@XStreamAlias("Task")
public class Task implements PlanningCloneable<Task>{
		
	private Material material;
	private int amount;
	private String id;
	private double price = 1;
	
	private PlanningAssignment initialAssignment;
	
	private boolean fixed;
	
	private boolean ofc = false;
	private String ofcId = null;
	
	private int readyTime;
	private int dueTime;
	private int ofcDueTime;
	
	private List<PlanningAssignment> allowedPlanningAssignments = new ArrayList<PlanningAssignment>();
	
	private PlanningAssignment assignment;
	
	public Task() {}
	
	public Task(String id, int readyTime, int dueTime, Material material, int amount) {
		setId(id);
		setReadyTime(readyTime);
		setDueTime(dueTime);
		setMaterial(material);
		setAmount(amount);
	}
	
	public Task(Task original) {
		setMaterial(original.getMaterial());
		setAmount(original.getAmount());
		setId(original.getId());
		setPrice(original.getPrice());
		setFixed(original.isFixed());
		setOfc(original.isOfc());
		setOfcId(original.getOfcId());
		setReadyTime(original.getReadyTime());
		setDueTime(original.getDueTime());
		setOfcDueTime(original.getOfcDueTime());
		setAllowedPlanningAssignments(original.getAllowedPlanningAssignments());
	}
	
	public void setReadyTime(int readyTime) {
		this.readyTime = readyTime;
	}
	
	public int getReadyTime() {
		return this.readyTime;
	}
	
	public void setDueTime(int dueTime) {
		this.dueTime = dueTime;
	}
	
	public int getDueTime() {
		return this.dueTime;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public int getAmount() {
		return this.amount;
	}
	
	public double getAmountInKg() {
		return getAmount()*getMaterial().getWeight();
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}	

	@PlanningVariable(valueRangeProviderRefs = {"planningAssignmentRange"})
	public PlanningAssignment getAssignment() {
		return assignment;
	}

	public void setAssignment(PlanningAssignment assignment) {	
		this.assignment = assignment;
	}

	public Task planningClone() {
		Task clone = new Task(this);		
		return clone;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@ValueRangeProvider(id = "planningAssignmentRange")
	public List<PlanningAssignment> getAllowedPlanningAssignments() {
		return allowedPlanningAssignments;
	}

	public void setAllowedPlanningAssignments(
			List<PlanningAssignment> allowedPlanningAssignments) {
		this.allowedPlanningAssignments = allowedPlanningAssignments;
	}

	public PlanningAssignment getInitialAssignment() {
		return initialAssignment;
	}

	public void setInitialAssignment(PlanningAssignment initialAssignment) {
		this.initialAssignment = initialAssignment;
	}

	public boolean isOfc() {
		return ofc;
	}

	public void setOfc(boolean ofc) {
		this.ofc = ofc;
	}

	public String getOfcId() {
		return ofcId;
	}

	public void setOfcId(String ofcId) {
		this.ofcId = ofcId;
	}

	public int getOfcDueTime() {
		return ofcDueTime;
	}

	public void setOfcDueTime(int ofcDueTime) {
		this.ofcDueTime = ofcDueTime;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public String toString() {
		return "Task " + id + " amount " + amount;
	}
	
}
