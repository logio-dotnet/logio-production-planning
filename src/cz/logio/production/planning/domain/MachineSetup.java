package cz.logio.production.planning.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MachineSetup")
public class MachineSetup implements Cloneable {
	
	private List<Machine> machineList = new ArrayList<Machine>();
	
	private List<Material> materialList = new ArrayList<Material>();
	
	/**
	 * actual capacity - minimum of the line and packer
	 */
	private Map<Material,Integer> capacityMap = new HashMap<Material,Integer>();
	
	/**
	 * available capacity on the packers
	 */
	private Map<Material,Integer> packerCapacityMap = new HashMap<Material,Integer>();
	
	//private int lineCapacityInKg;
	
	private Map<Material,Machine> machineMap = new HashMap<Material,Machine>();
	
	private Map<Machine,Material> machineToMaterialMap = new HashMap<Machine,Material>();
	
	private Map<Machine,Tube> machineToTubeMap = new HashMap<Machine,Tube>();
	
	private int workers;
	
	private Machine line;
	
	public void clear() {
		machineList = new ArrayList<Machine>();
		materialList = new ArrayList<Material>();
		capacityMap = new HashMap<Material,Integer>();
		machineMap = new HashMap<Material,Machine>();
		machineToMaterialMap = new HashMap<Machine,Material>();
		workers = 0;
	}
	
	public List<Machine> getMachineList() {
		return machineList;
	}
	
	private void addMachine(Machine machine) {
		if (!machineList.contains(machine)) {
			machineList.add(machine);
		}
	}
	
	public List<Material> getMaterialList() {
		return materialList;
	}
	
	public void addProduction(Material material, int capacity, Machine machine) {	
		addMachine(machine);
		
		if (!materialList.contains(material)) {
			materialList.add(material);
			machineMap.put(material, machine);
			packerCapacityMap.put(material, capacity);
			
			if (material.isFinalMaterial()) {
				Integer lineCapacity = capacityMap.get(material.getComponentMaterial());
				if (lineCapacity == null) {
					System.out.println("Missing line capacity!");
				}
				capacityMap.put(material, Math.min(capacity,lineCapacity));
			}
			else {
				capacityMap.put(material, capacity);
			}
		}
				
		Material materialOnMachine = machineToMaterialMap.get(machine);
		
		if (materialOnMachine != null && materialOnMachine != material) {
			System.out.println("Multiple materials on the same machine!");
			return;
		}
		
		machineToMaterialMap.put(machine, material);
		
		Material m = machineToMaterialMap.get(machine);
		
		if (material.getTube() != null) {
			machineToTubeMap.put(machine, material.getTube());
		}
		
	}
	
	public Machine getLine() {
		if (line != null) return line;
		
		for (Machine machine : getMachineList()) {
			if (!machine.isLine()) continue;		
			if (line != null) {
				System.out.println("Multiple lines for machine setup!"); 
			}		
			line = machine;
		}
		
		return line;		
	}
	
	public Map<Material,Integer> getCapacityMap(){
		return capacityMap;
	}
	
	public void setCapacity(Material material, int capacity) {
		capacityMap.put(material, capacity);
	}
	
	public int getCapacity(Material material) {
		Integer capacity = capacityMap.get(material);
		return capacity == null ? 0 : capacity;
	}
	
	public int getCapacitySum() {
		int capacity = 0;
		
		for (Entry<Material,Integer> entry : capacityMap.entrySet()) {
			if (entry.getKey().isFinalMaterial()) {
				capacity += entry.getValue();
			}
		}
		
		return capacity;
	}
	
	public int getPackerCapacity(Material material) {
		Integer capacity = packerCapacityMap.get(material);
		return capacity == null ? 0 : capacity;
	}
	
	public void setWorkers(int workers){
		this.workers = workers;
	}
	
	public int getWorkers(){
		return this.workers;
	}
	
	public Map<Material,Machine> getMachineMap() {
		return machineMap;
	}
	
	public Machine getMachine(Material material) {
		return machineMap.get(material);
	}
	
	
	public Material getMaterialProducedOnMachine(Machine machine) {
		return machineToMaterialMap.get(machine);
	}
	
	public Material getComponentMaterial() {
		return getMaterialProducedOnMachine(line);
	}
	
	public Group getGroupProducedOnMachine(Machine machine) {
		Material material = getMaterialProducedOnMachine(machine);
		return material == null ? null : material.getGroup();
	}
	
	public Tube getTubeForMachine(Machine machine) {
		return machineToTubeMap.get(machine);
	}
	
	public MachineSetup clone() {
		MachineSetup newMachineSetup = new MachineSetup();
		
		newMachineSetup.machineList =  new ArrayList<Machine>(machineList);
		newMachineSetup.materialList = new ArrayList<Material>(materialList);
		newMachineSetup.capacityMap = new HashMap<Material,Integer>(capacityMap);
		newMachineSetup.machineMap = new HashMap<Material,Machine>(machineMap);
		newMachineSetup.machineToMaterialMap = new HashMap<Machine,Material>(machineToMaterialMap);
		newMachineSetup.machineToTubeMap = new HashMap<Machine,Tube>(machineToTubeMap);
		newMachineSetup.packerCapacityMap = new HashMap<Material,Integer>(packerCapacityMap);
		newMachineSetup.workers = workers;
		newMachineSetup.line = line;
		//newMachineSetup.lineCapacityInKg = lineCapacityInKg;
		
		return newMachineSetup;
	}
	
	public boolean isSimple() {
		return getMachineList().size() <= 2;
	}
	
	public boolean isPackerCritical(Material material) {
		int lineCapacity = getCapacity(material.getComponentMaterial());
		int packerCapacity = getCapacity(material);		
		return lineCapacity > packerCapacity;
	}
	
	public String toString() {
		String description = "SETUP[";
		for (Machine machine : machineList) {
			description += machine + " "; 
		}
		description += "]";
		return description;
	}

	/*public int getLineCapacityInKg() {
		return lineCapacityInKg;
	}

	public void setLineCapacityInKg(int lineCapacityInKg) {
		this.lineCapacityInKg = lineCapacityInKg;
	}*/
}
