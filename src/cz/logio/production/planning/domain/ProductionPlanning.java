package cz.logio.production.planning.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.solution.cloner.PlanningCloneable;
import org.optaplanner.core.api.domain.valuerange.CountableValueRange;
import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.bendable.BendableScore;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cz.logio.production.planning.utils.ChangeoverMatrix;
import cz.logio.production.planning.utils.Dictionary;
import cz.logio.production.planning.utils.Neighbourhood;
import cz.logio.production.planning.utils.TimeHandler;

@PlanningSolution
@XStreamAlias("ProductionPlanning")
public class ProductionPlanning implements Solution<BendableScore>, PlanningCloneable<ProductionPlanning>  {
	
	private List<Task> taskList;
	
	private List<MachineSetup> machineSetupList;
	
	private List<Group> groupList;
	
	private List<Machine> machineList;
	
	private List<Material> materialList;
	
	private List<Shift> shiftList;
	
	private List<Tube> tubeList;
	
	private List<PlanningAssignment> planningAssignmentList;
	
	private TimeHandler timeHandler;
	
	private Dictionary dictionary;
	
	private ChangeoverMatrix changeoverMatrix;
	
	private Map<Machine,TreeSet<PlanningAssignment>> trees;

	//@XStreamConverter(value = XStreamScoreConverter.class, types = {BendableScoreDefinition.class})
    private BendableScore score;
	
	public BendableScore getScore() {
		return score;
	}

	public void setScore(BendableScore score) {
		this.score = score;
	}
	
	@PlanningEntityCollectionProperty
	public List<Task> getTaskList() {
		return taskList;
	}
	
	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}
	
	//@ValueRangeProvider(id = "planningAssignmentRange")
	@PlanningEntityCollectionProperty
	public List<PlanningAssignment> getPlanningAssignmentList() {
		return planningAssignmentList;
	}

	public void setPlanningAssignmentList(List<PlanningAssignment> planningAssignmentList) {
		this.planningAssignmentList = planningAssignmentList;
	}
	
	public List<MachineSetup> getMachineSetupList() {
		return machineSetupList;
	}
	
	public void setMachineSetupList(List<MachineSetup> machineSetup) {
		this.machineSetupList = machineSetup;
	}
	
	@ValueRangeProvider(id = "timePointRange")
	public CountableValueRange<Integer> getTimePointList() {
		return ValueRangeFactory.createIntValueRange(0, timeHandler.getTimePointsAvailable());
	}
	
	public List<Group> getGroupList() {
		return groupList;
	}
	
	public void setGroupList(List<Group> groupList) {
		this.groupList = groupList;
	}
	
	public List<Machine> getMachineList() {
		return machineList;
	}
	
	public void setMachineList(List<Machine> machineList) {
		this.machineList = machineList;
	}
	
	public List<Material> getMaterialList() {
		return materialList;
	}
	
	public void setMaterialList(List<Material> materialList) {
		this.materialList = materialList;
	}
	
	public List<Shift> getShiftList() {
		return shiftList;
	}
	
	public void setShiftList(List<Shift> shiftList) {
		this.shiftList = shiftList;
	}
	
	public int getTimePointsAvailable() {
		return timeHandler.getTimePointsAvailable();
	}
	
	public int getTimeStepLength() {
		return this.timeHandler.timeStepLength;
	}

	public void setTimeStepLength(int timeStepLength) {
		this.timeHandler.timeStepLength = timeStepLength;
	}

	public TimeHandler getTimeHandler() {
		return timeHandler;
	}

	public void setTimeHandler(TimeHandler timeHandler) {
		this.timeHandler = timeHandler;
	}

	public Dictionary getDictionary() {
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	public List<Tube> getTubeList() {
		return tubeList;
	}

	public void setTubeList(List<Tube> tubeList) {
		this.tubeList = tubeList;
	}

	public Collection<? extends Object> getProblemFacts() {
		List<Object> facts = new ArrayList<Object>();
        facts.addAll(machineSetupList);
        facts.addAll(groupList);
        facts.addAll(machineList);
        facts.addAll(materialList);
        facts.addAll(shiftList);
        facts.addAll(tubeList);
        facts.add(changeoverMatrix);
        
        return facts;
	}

	public ProductionPlanning planningClone() {
		ProductionPlanning clone = new ProductionPlanning();
		
		clone.timeHandler = timeHandler;
		clone.dictionary = dictionary;
		clone.changeoverMatrix = changeoverMatrix;
		clone.machineSetupList = machineSetupList;
		clone.groupList = groupList;
		clone.machineList = machineList;
		clone.materialList = materialList;
		clone.shiftList = shiftList;
		clone.tubeList = tubeList;
		
		clone.score = score;
		
		clone.trees = Neighbourhood.createTreeStructure(getMachineList());
		//deep clone planning entities
		Map<String,PlanningAssignment> clonnedAssignments = clonePlanningAssignments(clone.trees);
			
		clone.planningAssignmentList = new ArrayList<PlanningAssignment>(clonnedAssignments.values());
		
		clone.taskList = cloneTasks(clonnedAssignments);
		
		return clone;
		
	}
	
	private List<Task> cloneTasks(Map<String,PlanningAssignment> clonnedAssignments) {
		List<Task> clonnedTasks = new ArrayList<Task>();
		Map<String,Task> clonnedTasksMap = new HashMap<String,Task>();
		
		for (Task t : taskList){
			Task clonnedTask = t.planningClone(); 
			clonnedTasksMap.put(clonnedTask.getId(), clonnedTask);
			
			updateAllowedPlanningAssignments(clonnedTask,clonnedAssignments);
			
			PlanningAssignment clonnedInitialAssignment = clonnedAssignments.get(t.getInitialAssignment().getId());
			clonnedTask.setInitialAssignment(clonnedInitialAssignment);

			if (t.getAssignment() != null) {
				PlanningAssignment clonnedAssignment = clonnedAssignments.get(t.getAssignment().getId());
				clonnedTask.setAssignment(clonnedAssignment);
			}
			
			clonnedTasks.add(clonnedTask);
		}
		
		//update initial tasks for clonned planned assignments
		for (PlanningAssignment assignment : planningAssignmentList) {
			PlanningAssignment clonnedPlanningAssignment = clonnedAssignments.get(assignment.getId());
			
			for (Task task : assignment.getInitialTasks()) {
				clonnedPlanningAssignment.addInitialTask(clonnedTasksMap.get(task.getId()));
			}
		}
		
		return clonnedTasks;
	}
	
	/**
	 * Updates allowed planning assignments of given task to the new clones
	 * 
	 * @param task
	 * @param clonnedAssignments
	 */
	private void updateAllowedPlanningAssignments(Task task, Map<String,PlanningAssignment> clonnedAssignments) {	
		List<PlanningAssignment> allowedPlanningAssignments = new ArrayList<PlanningAssignment>();
		
		for (PlanningAssignment original : task.getAllowedPlanningAssignments()) {
			allowedPlanningAssignments.add(clonnedAssignments.get(original.getId()));
		}
		
		task.setAllowedPlanningAssignments(allowedPlanningAssignments);
	}
	
	private Map<String,PlanningAssignment> clonePlanningAssignments(Map<Machine,TreeSet<PlanningAssignment>> trees) {
		Map<String,PlanningAssignment> clonnedAssignments = new HashMap<String,PlanningAssignment>();
		
		for (PlanningAssignment pa : planningAssignmentList) {
			PlanningAssignment clone = pa.planningClone();
			clonnedAssignments.put(pa.getId(),clone);
		}
		
		//create neighbourhood
		for (PlanningAssignment pa : planningAssignmentList) {
			PlanningAssignment clone = clonnedAssignments.get(pa.getId());
			
			clone.getNeighbourhood().setFilledTrees(new HashSet<Machine>(pa.getNeighbourhood().getFilledTrees()));
					
			clone.getNeighbourhood().setTrees(trees);
			for (Machine machine : clone.getNeighbourhood().getFilledTrees()) {
				clone.getNeighbourhood().getTrees().get(machine).add(clone);
			}
			
			if (pa.getMachineSetup() != null) {
				for (Machine machine : pa.getMachineSetup().getMachineList()) {
					
					PlanningAssignment nextAssignment = pa.getNeighbourhood().getNextPlanningAssignment(machine);
					if (nextAssignment != null) {
						PlanningAssignment nextPlanningAssignmentClone = clonnedAssignments.get(nextAssignment.getId());
						clone.getNeighbourhood().setNextPlanningAssignment(machine,nextPlanningAssignmentClone);
					}			
					
					PlanningAssignment prevAssignment = pa.getNeighbourhood().getPreviousPlanningAssignment(machine);
					if (prevAssignment != null) {
						PlanningAssignment prevPlanningAssignmentClone = clonnedAssignments.get(prevAssignment.getId());
						clone.getNeighbourhood().setPreviousPlanningAssignment(machine,prevPlanningAssignmentClone);
					}
				}	
			}
		}		
		
		return clonnedAssignments;
	}

	public ChangeoverMatrix getChangeoverMatrix() {
		return changeoverMatrix;
	}

	public void setChangeoverMatrix(ChangeoverMatrix changeoverMatrix) {
		this.changeoverMatrix = changeoverMatrix;
	}
	
	public void computeChangeoverMatrix() {
		ChangeoverMatrix changeoverMatrix = new ChangeoverMatrix(getTimeHandler());
	    changeoverMatrix.prepareChangeoversFromModel(getMaterialList());
	    setChangeoverMatrix(changeoverMatrix);
	}
	
	public Map<Machine,TreeSet<PlanningAssignment>> getTrees() {
		return trees;
	}

	public void setTrees(Map<Machine,TreeSet<PlanningAssignment>> trees) {
		this.trees = trees;
	}

}
