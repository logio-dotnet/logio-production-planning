package cz.logio.production.planning.domain;

public class Tube {
	private String name;
	private int switchTime;
	
	public Tube() {
	}
	
	public Tube(String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSwitchTime() {
		return switchTime;
	}

	public void setSwitchTime(int switchTime) {
		this.switchTime = switchTime;
	}
	
	public String toString() {
		return getName();
	}
}
