package cz.logio.production.planning.domain.comparator;

import java.util.Comparator;

import org.apache.commons.lang.builder.CompareToBuilder;

import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.Task;

public class PlanningAssignmentDifficultyComparator implements
		Comparator<PlanningAssignment> {

	public int compare(PlanningAssignment pa1, PlanningAssignment pa2) {
		return new CompareToBuilder()
		.append(getEarliestDueTime(pa1),getEarliestDueTime(pa2))
		.toComparison(); 
	}
	
	private int getEarliestDueTime(PlanningAssignment assignment) {
		int earliestDueTime = Integer.MAX_VALUE;	
		for (Task t : assignment.getInitialTasks()) {
			if (t.getDueTime() < earliestDueTime) {
				earliestDueTime = t.getDueTime();
			}
		}	
		return earliestDueTime;
	}

}
