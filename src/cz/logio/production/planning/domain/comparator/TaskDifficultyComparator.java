package cz.logio.production.planning.domain.comparator;

import java.util.Comparator;

import org.apache.commons.lang.builder.CompareToBuilder;

import cz.logio.production.planning.domain.Task;

public class TaskDifficultyComparator implements Comparator<Task> {

	public int compare(Task a, Task b) {
		return new CompareToBuilder()
			.append(a.getDueTime(),b.getDueTime())
			.toComparison(); 			
	}

}
