package cz.logio.production.planning.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.solution.cloner.PlanningCloneable;
import org.optaplanner.core.api.domain.valuerange.CountableValueRange;
import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.domain.variable.CustomShadowVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import cz.logio.production.planning.filter.MovablePlanningAssignmentSelectionFilter;
import cz.logio.production.planning.listeners.*;
import cz.logio.production.planning.utils.Neighbourhood;

@PlanningEntity(movableEntitySelectionFilter = MovablePlanningAssignmentSelectionFilter.class)
public class PlanningAssignment implements Comparable<PlanningAssignment>,PlanningCloneable<PlanningAssignment> {
	private String id;
	private int numericId;
	/** Not clonned */
	private List<Task> initialTasks = new ArrayList<Task>();
	private Material initialMaterial;
	private int price = 1;
	private int additionalTimeRangeLowerBound = 0;
	private int additionalTimeRangeUpperBound = 40;
	
	private boolean fixed;
	
	//shadow variables
	private int assignedTasksCount = 0;
	private int amountSum = 0;
	private Map<Material,Integer> amountMap = new HashMap<Material,Integer>();
	private boolean assigned = false;
	/** Not clonned */
	private Neighbourhood neighbourhood = new Neighbourhood();
	private int componentProductionTimeSoFar;
	private int numberOfTimeSteps;	
	private int timeEnd;
	private boolean endOfComponentProduction;
	private Map<Machine,Integer> previousAssignmentTimeEnd = new HashMap<Machine,Integer>();
	
	//planning variables
	private int timeStart;
	private MachineSetup machineSetup;
	private int additionalTime;
	
	public PlanningAssignment() {}
	
	public PlanningAssignment(String id, int numericId, Material material) {
		this.setId(id);
		this.setNumericId(numericId);
		this.setInitialMaterial(material);
	}
	
	public PlanningAssignment(PlanningAssignment original) {
		setId(original.getId());
		setNumericId(original.getNumericId());
		setInitialMaterial(original.getInitialMaterial());
		setPrice(original.getPrice());
		setAdditionalTimeRangeLowerBound(original.getAdditionalTimeRangeLowerBound());
		setAdditionalTimeRangeUpperBound(original.getAdditionalTimeRangeUpperBound());
		
		setFixed(original.isFixed());
		
		setAssignedTasksCount(original.getAssignedTasksCount());
		setAmountSum(original.getAmountSum());
		setAmountMap(new HashMap<Material,Integer>(original.getAmountMap()));
		setAssigned(original.isAssigned());
		setComponentProductionTimeSoFar(original.getComponentProductionTimeSoFar());
		setNumberOfTimeSteps(original.getNumberOfTimeSteps());
		setTimeEnd(original.getTimeEnd());
		setEndOfComponentProduction(original.isEndOfComponentProduction());
		setPreviousAssignmentTimeEnd(new HashMap<Machine,Integer>(original.getPreviousAssignmentTimeEnd()));
		
		setTimeStart(original.getTimeStart());
		setMachineSetup(original.getMachineSetup());
		setAdditionalTime(original.getAdditionalTime());
	}
	
	public PlanningAssignment planningClone() {
		PlanningAssignment clone = new PlanningAssignment(this);
		return clone;
	}
	
	// ############################################################################
	// PLANNING VARIABLES
	// ############################################################################	
	
	public void setMachineSetup(MachineSetup machineSetup){
		//System.out.println("Settin machine setup for " + this + " to " + machineSetup);
		this.machineSetup = machineSetup;
	}
	
	@PlanningVariable(valueRangeProviderRefs = {"machineSetupRange"})
	public MachineSetup getMachineSetup(){
		return this.machineSetup;
	}
	
	@ValueRangeProvider(id = "machineSetupRange")
	public List<MachineSetup> getAllowedMachineSetupList() {
		return getInitialMaterial().getAllowedMachineSetups();
	}
	
	public void setTimeStart(int timeStart){
		//System.out.println("Setting time start for " + this + " to " + timeStart);
		this.timeStart = timeStart;
	}
	
	@PlanningVariable(valueRangeProviderRefs = {"timePointRange"})
	public int getTimeStart(){
		return this.timeStart;
	}	

	public void setAdditionalTime(int additionalTime) {
		//System.out.println("Setting additional time for " + this + " to " + additionalTime);
		this.additionalTime = additionalTime;
	}
	
	@PlanningVariable(valueRangeProviderRefs = {"additionalTimeRange"})
	public int getAdditionalTime() {
		return additionalTime;
	}
	
	@ValueRangeProvider(id = "additionalTimeRange")
	public List<Integer> getAdditionalTimeRange() {
		return getInitialMaterial().getAvailableTimeRanges(additionalTimeRangeLowerBound, additionalTimeRangeUpperBound);
	}
	
	// ############################################################################
	// SHADOW VARIABLES
	// ############################################################################	
	
	@CustomShadowVariable(variableListenerClass = AmountUpdatingVariableListener.class,
			sources = {@CustomShadowVariable.Source(variableName = "assignment", entityClass = Task.class )})
	public Map<Material,Integer> getAmountMap() {
		return this.amountMap;
	}
	
	public void setAmountMap(Map<Material,Integer> amountMap) {
		this.amountMap = amountMap;
	}
	
	@CustomShadowVariable(variableListenerClass = AmountSumUpdatingVariableListener.class,
			sources = {@CustomShadowVariable.Source(variableName = "assignment", entityClass = Task.class )})
	public int getAmountSum() {
		return amountSum;
	}

	public void setAmountSum(int amountSum) {
		this.amountSum = amountSum;
	}
	
	public void addAmountSum(int amount) {
		this.amountSum += amount;
	}
	
	@CustomShadowVariable(variableListenerClass = AssignedTasksCountUpdatingVariableListener.class,
			sources = {@CustomShadowVariable.Source(variableName = "assignment", entityClass = Task.class )})
	public int getAssignedTasksCount() {
		return assignedTasksCount;
	}

	public void setAssignedTasksCount(int taskCount) {
		this.assignedTasksCount = taskCount;
	}
	
	public void increaseAssignedTasksCount() {
		this.assignedTasksCount++;
	}
	
	public void decreaseAssignedTasksCount() {
		this.assignedTasksCount--;
	}
	
	@CustomShadowVariable(variableListenerClass = NumberOfTimeStepsUpdatingVariableListener.class,
			sources = {
				@CustomShadowVariable.Source(variableName = "amountMap"), 
				@CustomShadowVariable.Source(variableName = "machineSetup")
				})
	public int getNumberOfTimeSteps() {		
		return numberOfTimeSteps;
	}
	
	public void setNumberOfTimeSteps(int timeSteps) {
		this.numberOfTimeSteps = timeSteps;
	}
	
	@CustomShadowVariable(variableListenerClass = AssignmentStatusUpdatingVariableListener.class,
			sources = {
				@CustomShadowVariable.Source(variableName = "assignedTasksCount"),
				@CustomShadowVariable.Source(variableName = "amountSum"),
				@CustomShadowVariable.Source(variableName = "additionalTime")})
	public boolean isAssigned() {
		return assigned;
	}

	public void setAssigned(boolean assigned) {
		this.assigned = assigned;
	}
	
	@CustomShadowVariable(variableListenerClass = NeighbourhoodUpdatingVariableListener.class,
			sources = {
				@CustomShadowVariable.Source(variableName = "timeStart"),
				@CustomShadowVariable.Source(variableName = "assigned"),
				@CustomShadowVariable.Source(variableName = "machineSetup") })
	public Neighbourhood getNeighbourhood() {
		return neighbourhood;
	}

	public void setNeighbourhood(Neighbourhood neighbourhood) {
		this.neighbourhood = neighbourhood;
	}
	
	@CustomShadowVariable(variableListenerClass = TimeEndUpdatingVariableListener.class,
			sources = {
				@CustomShadowVariable.Source(variableName = "timeStart"),
				@CustomShadowVariable.Source(variableName = "additionalTime"),
				@CustomShadowVariable.Source(variableName = "numberOfTimeSteps")})
	public int getTimeEnd() {
		return timeEnd;
	}
	
	public void setTimeEnd(int timeEnd) {
		this.timeEnd = timeEnd;
	}
	
	/**
	 * Pri zmene numberOfTimeSteps sa meni len hodnota u nasledujucich planning assignments
	 * @return
	 */
	@CustomShadowVariable(variableListenerClass = ComponentProductionTimeUpdatingVariableListener.class,
			sources = {
				@CustomShadowVariable.Source(variableName = "neighbourhood"),
				@CustomShadowVariable.Source(variableName = "timeStart"),
				@CustomShadowVariable.Source(variableName = "timeEnd"),
				@CustomShadowVariable.Source(variableName = "additionalTime"),
				@CustomShadowVariable.Source(variableName = "numberOfTimeSteps")})
	public int getComponentProductionTimeSoFar() {
		return componentProductionTimeSoFar;
	}

	public void setComponentProductionTimeSoFar(int componentProductionTimeSoFar) {
		this.componentProductionTimeSoFar = componentProductionTimeSoFar;
	}
	
	@CustomShadowVariable(variableListenerClass = EndOfComponentProductionUpdatingVariableListener.class,
			sources = {
				@CustomShadowVariable.Source(variableName = "neighbourhood"),
				//@CustomShadowVariable.Source(variableName = "machineSetup"),
				@CustomShadowVariable.Source(variableName = "componentProductionTimeSoFar")})
	public boolean isEndOfComponentProduction() {
		return endOfComponentProduction;
	}
	
	public void setEndOfComponentProduction(boolean endOfComponentProduction) {
		this.endOfComponentProduction = endOfComponentProduction;
	}
	
	
	@CustomShadowVariable(variableListenerClass = PreviousAssignmentTimeEndUpdatingVariableListener.class,
			sources = {
				@CustomShadowVariable.Source(variableName = "timeEnd"),
				@CustomShadowVariable.Source(variableName = "neighbourhood")})
	public Map<Machine,Integer> getPreviousAssignmentTimeEnd() {
		return previousAssignmentTimeEnd;
	}

	public void setPreviousAssignmentTimeEnd(
			Map<Machine,Integer> previousAssignmentTimeEnd) {
		this.previousAssignmentTimeEnd = previousAssignmentTimeEnd;
	}
	
	// ############################################################################
	// OTHER
	// ############################################################################	

	public int getAmount(Material material) {
		Integer amount = this.amountMap.get(material);
		return amount == null ? 0 : amount;
	}
	
	private void setAmount(Material material,int amount) {
		if (amount == 0) {
			this.amountMap.remove(material);
		}
		else {
			this.amountMap.put(material,amount);
		}	
	}
	
	public void addAmount(Material material,int amount) {
		Integer oldAmount = this.amountMap.get(material);
		if (oldAmount == null) {
			oldAmount = 0;
		}	
		setAmount(material,oldAmount + amount);
	}

	public void setInitialMaterial(Material material){
		this.initialMaterial = material;
	}
	
	public Material getInitialMaterial(){
		return this.initialMaterial;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getNumericId() {
		return numericId;
	}

	public void setNumericId(int numericId) {
		this.numericId = numericId;
	}

	public List<Task> getInitialTasks() {
		return initialTasks;
	}

	public void setInitialTasks(List<Task> initialTasks) {
		this.initialTasks = initialTasks;
	}
	
	public void addInitialTask(Task task) {
		getInitialTasks().add(task);
	}

	public String toString() {
		return "Planning Assignment " + numericId; //+ " (" + timeStart + " - " + timeEnd + ")";
	}
	
	public boolean isRightAfter(PlanningAssignment assignment) {
		return (assignment.getTimeEnd() == getTimeStart());
	}
	
	public int compareTo(PlanningAssignment planningAssignment) {
		int comp = Integer.compare(getTimeStart(), planningAssignment.getTimeStart());
		return comp == 0 ? Integer.compare(getNumericId(), planningAssignment.getNumericId()) : comp;
	}
	
	public void printChain(Machine machine) {
		if (!getMachineSetup().getMachineList().contains(machine)) return;
		
		PlanningAssignment first = this;
		while (true) {
			PlanningAssignment previous = first.getNeighbourhood().getPreviousPlanningAssignment(machine);
			if (previous == null) break;
			first = previous;
		}
		
		//print
		PlanningAssignment assignment = first;
		while (assignment != null) {
			System.out.print(assignment);
			assignment = assignment.getNeighbourhood().getNextPlanningAssignment(machine);
			if (assignment != null) {
				System.out.print("->");
			}
		}
		System.out.println();
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public int getAdditionalTimeRangeLowerBound() {
		return additionalTimeRangeLowerBound;
	}

	public void setAdditionalTimeRangeLowerBound(
			int additionalTimeRangeLowerBound) {
		this.additionalTimeRangeLowerBound = additionalTimeRangeLowerBound;
	}

	public int getAdditionalTimeRangeUpperBound() {
		return additionalTimeRangeUpperBound;
	}

	public void setAdditionalTimeRangeUpperBound(
			int additionalTimeRangeUpperBound) {
		this.additionalTimeRangeUpperBound = additionalTimeRangeUpperBound;
	}
}
