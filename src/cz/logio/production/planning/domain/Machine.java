package cz.logio.production.planning.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Machine")
public class Machine {

	private String name;
	
	/**
	 * true -> linka,
	 * false -> balicka
	 */
	private boolean line;
	
	public Machine() {
	}
	
	public Machine(String machineName) {
		setName(machineName);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String machineName) {
		this.name = machineName;
	}

	public boolean isLine() {
		return line;
	}

	public void setLine(boolean line) {
		this.line = line;
	}
	
	public String toString() {
		return getName();
	}
}

