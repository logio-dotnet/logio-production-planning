package cz.logio.production.planning.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Shift")
public class Shift implements Comparable<Shift> {
	private int timeStart;
	
	private int timeEnd;
	
	private int cost;
	
	private List<Machine> forbiddenMachines = new ArrayList<Machine>();
	
	private Date dateStart;
	
	public Shift(){
	}
	
	public Shift(int timeStart, int timeEnd){
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
	}
	
	public void setTimeStart(int timeStart){
		this.timeStart = timeStart;
	}
	
	public int getTimeStart(){
		return this.timeStart;
	}
	
	public void setTimeEnd(int timeEnd){
		this.timeEnd = timeEnd;
	}
	
	public int getTimeEnd(){
		return this.timeEnd;
	}
	
	public void setCost(int cost){
		this.cost = cost;
	}
	
	public int getCost(){
		return this.cost;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public boolean isForbidden(Machine machine) {
		return forbiddenMachines.contains(machine);
	}

	public void setForbidden(boolean forbidden,Machine machine) {
		if (!forbidden && isForbidden(machine)) {
			forbiddenMachines.remove(machine);
		}
		if (forbidden && !isForbidden(machine)) {
			forbiddenMachines.add(machine);
		}
	}
	
	public List<Machine> getForbiddenMachines() {
		return forbiddenMachines;
	}

	public int compareTo(Shift o) {
		return Integer.compare(getTimeStart(),o.getTimeStart());			
	}
	
	public boolean isDayShift() {
		return dateStart.getHours() < 12; 
	}
	
	
}
