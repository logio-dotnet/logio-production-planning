package cz.logio.production.planning.filter;

import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.PlanningAssignment;

public class MovablePlanningAssignmentSelectionFilter implements
		SelectionFilter<PlanningAssignment> {

	public boolean accept(ScoreDirector scoreDirector,
			PlanningAssignment selection) {
		return !selection.isFixed();
	}

}
