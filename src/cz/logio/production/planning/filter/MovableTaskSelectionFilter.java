package cz.logio.production.planning.filter;

import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionFilter;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Task;

public class MovableTaskSelectionFilter implements SelectionFilter<Task> {

	public boolean accept(ScoreDirector scoreDirector, Task selection) {
		return !selection.isFixed();
	}

}
