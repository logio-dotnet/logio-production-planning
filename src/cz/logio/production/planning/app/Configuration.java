package cz.logio.production.planning.app;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import cz.logio.production.planning.utils.DbConnector;

public class Configuration {
	
	public final String[] LINES = {};
	
	public int timeWindow = 1;
	
	public int timeWindowOfc = 11;
	
	public int timeWindowProductionSuggested = 1;
	
	final String POSTFIX = "";
	
	String pwDate = "2016-01-06";
	
	public String pwDateTime = pwDate + " 10:00:00";
	
	long solverTimeInSeconds = 30; 
	
	/**
	 * Verzia prepočtu: 1-klasická, 2-inkrementálna (začína pomocou predchádzajúceho rozvrhu)
	 */
	static int schedulingVersion = 1;
	
	//////////////////////////////////////////////////////////////////////////////
	
	public String solutionFolder = "data/productionPlanning/";
	
	///////////////////////////////////////////////////////////////////////////////
	
	final InputModelType INPUT_TYPE = InputModelType.NEW;
	
	public final int TIME_STEP_LENGTH = 15;
	
	/**
	 * Linky na ktorych mozu balit dve balicky sucasne
	 */
	public static final String[] MULTIPACKING = {"2","7"};
	
	public void load() {
		try {		
			Connection conn = DbConnector.getConnectionToPwDB();
			
			ResultSet res = DbConnector.executeFetchAll("SELECT `value` FROM production_scheduling_configuration"
					+ " WHERE `key` = 'time-window';");
			
			if (res.next()) {
				timeWindow = res.getInt("value");
			}	
			
			res = DbConnector.executeFetchAll("SELECT `value` FROM production_scheduling_configuration"
					+ " WHERE `key` = 'time-window-production-suggested';");
			
			if (res.next()) {
				timeWindowProductionSuggested = res.getInt("value");
			}
			
			res = DbConnector.executeFetchAll("SELECT `value` FROM production_scheduling_configuration"
					+ " WHERE `key` = 'time-window-ofc';");
			
			if (res.next()) {
				timeWindowOfc = res.getInt("value");
			}
			
			
			res = DbConnector.executeFetchAll("SELECT `value` FROM production_scheduling_configuration"
					+ " WHERE `key` = 'schedule-start';");
			
			if (res.next()) {		
				pwDate = res.getString("value").substring(0, 10);
				pwDateTime = res.getString("value");
			}
			
			res = DbConnector.executeFetchAll("SELECT `value` FROM production_scheduling_configuration"
					+ " WHERE `key` = 'version';");
		
			if (res.next()) {		
				schedulingVersion = res.getInt("value");
			}			
			
			res = DbConnector.executeFetchAll("SELECT `value` FROM production_scheduling_configuration"
					+ " WHERE `key` = 'solver-time';");
			
			if (res.next()) {
				int multiplier = 1;
				
				String solverTime = res.getString("value");
				if (solverTime.contains("m")) multiplier = 60;
				if (solverTime.contains("h")) multiplier = 60*60;
				
				solverTime = solverTime.replaceAll("[^\\d.]", "");
				
				solverTimeInSeconds = (long) (Double.parseDouble(solverTime)*multiplier);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		  	}
	}
	
	public static String getSolverConfig() {
		if (schedulingVersion == 1) return "cz/logio/production/planning/solver/productionPlanningSolverConfig.xml";
		else return "cz/logio/production/planning/solver/productionPlanningIncrementalSolverConfig.xml";
	}

}

enum InputModelType {
	/**
	 * Load existing model with new problem (tasks)
	 */
	EXISTING, 
	
	/**
	 * Load new model and new problem from DB
	 */
	NEW,
	
	/**
	 * Load existing model with existing problem (from XML)
	 */
	OLD
}
