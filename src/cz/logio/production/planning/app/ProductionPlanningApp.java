package cz.logio.production.planning.app;

import java.io.File;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.termination.TerminationConfig;

import cz.logio.production.planning.data.io.ConstraintLogger;
import cz.logio.production.planning.data.io.ProductionPlanningConsolePrinter;
import cz.logio.production.planning.data.io.ProductionPlanningDbIO;
import cz.logio.production.planning.data.io.ProductionPlanningDbPrinter;
import cz.logio.production.planning.data.io.ProductionPlanningFileIO;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.utils.DbConnector;


public class ProductionPlanningApp {
	
	public static final boolean DEBUG = false;
	
	private static boolean loadConfigurationFromDb = !DEBUG;
	
	private static boolean storeXmlFiles = false;
	
	private static Configuration configuration;
	
	private static ProductionPlanning previousProductionPlanning = null;
	
	private static ProductionPlanningFileIO productionPlanningFileIO;
	
	private static ProductionPlanningDbIO productionPlanningDbIO;
	
	
	public static void main(String[] args) {
		
		configuration = new Configuration();
		if (args.length > 1) {
			configuration.solutionFolder = args[1] + "/";
		}
		
		productionPlanningFileIO = new ProductionPlanningFileIO();
		productionPlanningDbIO = createProductionPlanningDbIO(args);
		
		if (loadConfigurationFromDb) configuration.load();
		
		
		String fileName = ""; //file to load the model from
		ProductionPlanning unsolvedProductionPlanning = getModel(fileName);
		
		boolean storeUnsolvedProductionPlanning = false;
		beforeSolving(unsolvedProductionPlanning, storeUnsolvedProductionPlanning);
		
		//WITHOUT THREADS
		//ConstraintLogger constraintLogger = new ConstraintLogger();
	    //ProductionPlanning solvedProductionPlanning = solve(unsolvedProductionPlanning,constraintLogger);
		
		//WITH THREADS
		ProductionPlanningThread bestThread = solveUsingThreads(unsolvedProductionPlanning);    
		ProductionPlanning solvedProductionPlanning = bestThread.solvedProductionPlanning;
		ConstraintLogger constraintLogger = bestThread.logger;
		
		boolean consoleOutput = DEBUG;
		boolean xmlOutput = true;
		boolean dbOutput = (configuration.schedulingVersion == 1) ? false : true;
		processResults(solvedProductionPlanning,constraintLogger,consoleOutput,xmlOutput,dbOutput);
		
		DbConnector.closeConnections();
    }
	
	/**
	 * Solves the ProductionPlanning in multiple parallel threads 
	 * and returns a ProductionPlanningThread with the best score.
	 * <br/><b>Currently only one thread is created!</b>
	 * 
	 * @param unsolvedProductionPlanning ProductionPlanning to solve
	 * @return	ProductionPlanningThread with the best score
	 * @see ProductionPlanningThread
	 * @see ProductionPlanning
	 */
	private static ProductionPlanningThread solveUsingThreads(ProductionPlanning unsolvedProductionPlanning) {
		
		int processors = Runtime.getRuntime().availableProcessors();
		processors = 1;
		ProductionPlanningThread[] threads = new ProductionPlanningThread[processors];
		
		for(int i=0; i < processors; i++) {
			threads[i] = new ProductionPlanningThread(unsolvedProductionPlanning.planningClone(),i);
			threads[i].start();
		}
		
		for (Thread thread : threads) {
		    try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ProductionPlanningThread bestThread = threads[0];
		
		for (ProductionPlanningThread thread : threads) {
			System.out.println("Thread " + thread.id + ": score " + thread.solvedProductionPlanning.getScore());
			
		    if (bestThread.solvedProductionPlanning.getScore().compareTo(thread.solvedProductionPlanning.getScore()) < 0) {
		    	bestThread = thread;
		    }
		}
		
		System.out.println("Best thread: " + bestThread.id);
		
		return bestThread;
	}
	
	/**
	 * Provides an output in format determined by other parameters.
	 * 
	 * @param productionPlanning	ProductionPlanning to process
	 * @param constraintLogger	ConstraintLogger with constraints to log
	 * @param consoleOutput	determines whether to log to the console
	 * @param xmlOutput	determines whether to create an xml file with solution (
	 * @param dbOutput	determines whether to write results into database
	 */
	private static void processResults(ProductionPlanning solvedProductionPlanning, ConstraintLogger constraintLogger,
			boolean consoleOutput, boolean xmlOutput, boolean dbOutput) {
	    if (consoleOutput) {
		    ProductionPlanningConsolePrinter printer = new ProductionPlanningConsolePrinter(solvedProductionPlanning);
		    //printer.printUsingTreeStructure();
		    printer.print();
	    }
	    
	    if (xmlOutput) {
	    	if (configuration.schedulingVersion == 1) {
	    		saveProductionPlanningIntoXML("solvedProductionPlanning.xml",solvedProductionPlanning);
	    	}
	    	else {
	    		saveProductionPlanningIntoXML("solvedProductionPlanningIncremental.xml",solvedProductionPlanning);
	    	}	
	    }
	    
	    if (dbOutput) {
		    constraintLogger.log();
		    System.out.println("Broken constraints logged into production_schedule_broken_constraints");
	
		    ProductionPlanningDbPrinter dbPrinter = new ProductionPlanningDbPrinter(solvedProductionPlanning);
			dbPrinter.print();
			System.out.println("Results inserted into production_schedule");
		}
	}
	
	/**
	 * Truncates all the output tables if DEBUG mode is turned on.
	 * If storeProductionPlanning is set to true, stores full production 
	 * planning (including changeover matrix) as an XML file into unsolved directory.
	 * 
	 * @param productionPlanning	unsolved ProductionPlanning
	 * @param storeProductionPlanning	determines whether the given ProductionPlanning should be stored
	 * @see ProductionPlanning
	 */
	private static void beforeSolving(ProductionPlanning productionPlanning, boolean storeProductionPlanning) {
		if (DEBUG) { // otherwise tables are truncated in batch-order
			truncateTables();
		}
		
		if (storeProductionPlanning) {
			saveProductionPlanningIntoXML("unsolved.xml",productionPlanning);
		}
	}
		
	private static ProductionPlanningDbIO createProductionPlanningDbIO(String[] args) {
		if (args.length < 1) {
			System.out.println("No database provided!");
			System.exit(1);
		}	
		return new ProductionPlanningDbIO(args[0], configuration);
	}
	
	private static ProductionPlanning getModel(String fileName){
		System.out.println("Loading model...");
		
		ProductionPlanning unsolvedProductionPlanning;	
		
		String unsolvedXMLPath = configuration.solutionFolder + "/unsolved/" + fileName;
		
		switch (configuration.INPUT_TYPE) {				
			case OLD : 
			    unsolvedProductionPlanning = loadProductionPlanningFromXml(unsolvedXMLPath);
				break;
			case NEW : 
				unsolvedProductionPlanning = productionPlanningDbIO.read();
				if (storeXmlFiles) saveProductionPlanningIntoXML("unsolved/" + fileName,unsolvedProductionPlanning);
				break;
			default : return null;
		}
		
		System.out.println("Model loaded");
			
		return unsolvedProductionPlanning;
	}
	
// TODO Remove unused code 
// 	/**
// 	 * Runs the solver.
// 	 * @param unsolved	unsolved model
// 	 * @return	solution
// 	 */
// 	public static ProductionPlanning solveV2(ProductionPlanning unsolved,ConstraintLogger constraintLogger, int seed) {
// 		System.out.println("SOLVING V2...");
// 		
// 		SolverFactory solverFactory = SolverFactory.createFromXmlResource(Configuration.SOLVER_CONFIG);
// 		TerminationConfig terminationConfig = solverFactory.getSolverConfig().getTerminationConfig();
// 		
// 		if (loadConfigurationFromDb) {	
// 			terminationConfig.setSecondsSpentLimit(configuration.solverTimeInSeconds);
// 			terminationConfig.setMinutesSpentLimit(0L);
// 		}
// 		
// 		solverFactory.getSolverConfig().setRandomSeed((long)seed);
// 		
// 		Solver solver = solverFactory.buildSolver();
// 		
// 		ScoreDirectorFactory scoreDirectorFactory = solver.getScoreDirectorFactory();
// 		ScoreDirector scoreDirector = scoreDirectorFactory.buildScoreDirector();
// 		
// 		int phaseCount = 3;
// 		
// 		Long seconds = terminationConfig.getSecondsSpentLimit()/phaseCount;
// 		terminationConfig.setSecondsSpentLimit(seconds);
// 		
// 		for (int run = 0; run < phaseCount; run++) {
// 			if (run != 0) {
// 				System.out.println("SHIFTING PHASE");
// 				unsolved = (ProductionPlanning) solver.getBestSolution();
// 				scoreDirector.setWorkingSolution(unsolved);
// 				PlanningAssignmentShifter shifter = new PlanningAssignmentShifter(scoreDirector);
// 				shifter.shiftPlanningAssignments();
// 			}
// 			
// 			System.out.println("SOLVING PHASE");
// 			solver.solve(unsolved);
// 		}
// 		
// 		constraintLogger.setSolver(solver);
// 		
// 		System.out.println("SOLVING FINISHED");
// 		
// 	    return (ProductionPlanning) solver.getBestSolution();
// 	}
	
	/**
	 * Runs the solver.
	 * @param unsolved	unsolved model
	 * @return	solution
	 */
	static ProductionPlanning solve(ProductionPlanning unsolved,ConstraintLogger constraintLogger, int seed) {
		System.out.println("SOLVING...");
		
		SolverFactory solverFactory = SolverFactory.createFromXmlResource(Configuration.getSolverConfig());
		
		if (loadConfigurationFromDb) {
			TerminationConfig terminationConfig = solverFactory.getSolverConfig().getTerminationConfig();
			terminationConfig.setSecondsSpentLimit(configuration.solverTimeInSeconds);
			terminationConfig.setMinutesSpentLimit(0L);
		}
		
		solverFactory.getSolverConfig().setRandomSeed((long)seed);
		
		Solver solver = solverFactory.buildSolver();
		
		solver.solve(unsolved);
		
		constraintLogger.setSolver(solver);
		
		System.out.println("SOLVING FINISHED");
		
	    return (ProductionPlanning) solver.getBestSolution();	
	}
	
	public static ProductionPlanning loadProductionPlanningFromXml(String xmlFileName) {
		File file = new File(xmlFileName);		
		return (ProductionPlanning) productionPlanningFileIO.read(file);
	}
	
	private static void saveProductionPlanningIntoXML(String filename, ProductionPlanning productionPlanning) {	
		System.out.println("Saving xml file to " + configuration.solutionFolder + filename);
		
		File file = new File(configuration.solutionFolder + filename);
		productionPlanningFileIO.write(productionPlanning, file);
	    
	    System.out.println("solution saved as " + filename);
	}
	
	/**
	 * 
	 * @return previous solution loaded from xml file
	 */
	public static ProductionPlanning getPreviousProductionPlanning() {
		if (previousProductionPlanning == null && Configuration.schedulingVersion == 2) {
			previousProductionPlanning = loadProductionPlanningFromXml(configuration.solutionFolder + "solvedProductionPlanning.xml");
		}	
		return previousProductionPlanning;
	}
	
	/**
	 * Truncates the following tables:
	 * <ul>
	 * <li> production_schedule
	 * <li> production_description
	 * <li> production_schedule_broken_constraints
	 * <li> production_changeover_matrix
	 * </ul>
	 */
	private static void truncateTables() {		
		DbConnector.exec("TRUNCATE production_schedule");
		DbConnector.exec("TRUNCATE production_description");
		DbConnector.exec("TRUNCATE production_schedule_broken_constraints");
		DbConnector.exec("TRUNCATE production_changeover_matrix");
	}

}


