package cz.logio.production.planning.app;

import org.optaplanner.benchmark.api.PlannerBenchmark;
import org.optaplanner.benchmark.api.PlannerBenchmarkFactory;

public class ProductionPlanningBenchmarkApp {
	
	public static void main(String[] args) {
		PlannerBenchmarkFactory plannerBenchmarkFactory = PlannerBenchmarkFactory.createFromXmlResource(
				 "cz/logio/production/planning/benchmark/productionPlanningBenchmarkConfig.xml");
		PlannerBenchmark plannerBenchmark = plannerBenchmarkFactory.buildPlannerBenchmark();

		plannerBenchmark.benchmark(); 
    }
}
