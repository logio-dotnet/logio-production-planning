package cz.logio.production.planning.app;

import cz.logio.production.planning.data.io.ConstraintLogger;
import cz.logio.production.planning.domain.ProductionPlanning;

public class ProductionPlanningThread extends Thread {
	ProductionPlanning unsolvedProductionPlanning;
	ProductionPlanning solvedProductionPlanning;
	
	ConstraintLogger logger = new ConstraintLogger();
	int id;
	
	public ProductionPlanningThread(ProductionPlanning productionPlanning, int id) {
		unsolvedProductionPlanning = productionPlanning;
		this.id = id;
	}
	
	public void run() {
		System.out.println("Thread " + id + " solving started ");
		solvedProductionPlanning = ProductionPlanningApp.solve(unsolvedProductionPlanning, logger,id);
		System.out.println("Thread " + id + " solving finished ");
	}
}
