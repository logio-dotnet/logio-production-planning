package cz.logio.production.planning.debugger;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.core.impl.score.director.ScoreDirectorFactory;
import org.optaplanner.persistence.xstream.impl.domain.solution.XStreamSolutionFileIO;

import cz.logio.production.planning.app.ProductionPlanningApp;
import cz.logio.production.planning.app.Configuration;
import cz.logio.production.planning.data.io.ConstraintLogger;
import cz.logio.production.planning.data.io.ProductionPlanningConsolePrinter;
import cz.logio.production.planning.data.io.ProductionPlanningDbPrinter;
import cz.logio.production.planning.data.io.ProductionPlanningFileIO;
import cz.logio.production.planning.domain.Group;
import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.MachineSetup;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.Shift;
import cz.logio.production.planning.domain.Task;
import cz.logio.production.planning.moves.PlanningAssignmentChainShiftMove;
import cz.logio.production.planning.utils.DbConnector;
import cz.logio.production.planning.utils.SolutionManager;

public class Debugger {

	public static void main(String[] args) {
		Configuration configuration = new Configuration();
		//ProductionPlanning productionPlanningOriginal = ProductionPlanningApp.loadProductionPlanningFromXml(Configuration.solutionFolder + "/solved/solvedProductionPlanningIncremental.xml");	
		File solutionFile = new File(configuration.solutionFolder + "/solved/solvedProductionPlanningIncremental.xml");
		//File solutionFile = new File("C:/Users/Naty/git/logio-production-planning/logio-production-planning/data/productionPlanning/2015-10-22_134115/unsolved/Original/unsolved.xml");
		
		//unsolvedSolutionFile = new File("C:/Users/Naty/unsolved.xml");
		ProductionPlanning productionPlanningOriginal = (ProductionPlanning) new ProductionPlanningFileIO().read(solutionFile);
		productionPlanningOriginal.getTimeHandler().timeStepLength = 15;
		/*printSolutionToDb("bonavita_live",productionPlanningOriginal);
		if (true) return;*/
		ProductionPlanning productionPlanningModified = productionPlanningOriginal.planningClone();	
		
		SolverFactory solverFactory = SolverFactory.createFromXmlResource(Configuration.getSolverConfig());	
		Solver solver = solverFactory.buildSolver();	
		ScoreDirectorFactory scoreDirectorFactory = solver.getScoreDirectorFactory();
		
		ConstraintLogger constraintLogger = new ConstraintLogger();
		constraintLogger.setSolver(solver);
		
		ScoreDirector scoreDirectorOriginal = scoreDirectorFactory.buildScoreDirector();			
		scoreDirectorOriginal.setWorkingSolution(productionPlanningOriginal);
		
		ScoreDirector scoreDirectorModified = scoreDirectorFactory.buildScoreDirector();
		scoreDirectorModified.setWorkingSolution(productionPlanningModified);
		
		modifySolution(productionPlanningModified,scoreDirectorModified);
		
		compareSolutions(productionPlanningOriginal,scoreDirectorOriginal,productionPlanningModified,scoreDirectorModified);
	}
	
	private static void checkConstraints(ConstraintLogger constraintLogger) {
		constraintLogger.printIncorrectAssignments("incompatibleGroups");
	}
	
	
	
	private static void modifySolution(ProductionPlanning solution, ScoreDirector scoreDirector) {
	
		// MODIFICATIONS HERE
		System.out.println("Modifying solution");
		System.out.println("PwDate: " + solution.getTimeHandler().pwDateTime);
		
		SolutionManager solutionManager = new SolutionManager(solution);
		//printMachineSetups(solution,"146649-b2238-2015-11-09-2015-11-11-44012373-part1PA");
		
		//shiftPlanningAssignmentStartTime(solution,scoreDirector,"144884-b3132-2015-10-07-2015-10-12-00000260-part1PA",-18);
		System.out.println("TEST BEFORE");
		test(solution);
		
		/*Material b4022 = solution.getDictionary().getMaterial("b4022");
		for (Task task : solution.getTaskList()) {
			if (task.getMaterial() == b4022) {
				System.out.println("PLANNING ASSIGNMENT ID : " + task.getAssignment().getId());
			}
		}*/
		
		shiftPlanningAssignmentStartTime(solution,scoreDirector,"b4084minPA",8);
		shiftPlanningAssignmentStartTime(solution,scoreDirector,"b4022minPA",-32);
		shiftPlanningAssignmentStartTime(solution,scoreDirector,"b4034minPA",-436);
		setAdditionalTime(solution,scoreDirector,"b4022minPA",12);
		setAdditionalTime(solution,scoreDirector,"b4034minPA",8);

		//multiPackingTest(solution);	
		//reassignTask(solution,scoreDirector,"144717-b2053-2015-10-05-2015-10-06-64945880-part1","144702-b2038-2015-10-05-2015-10-07-44012373-part1PA");
		//setAdditionalTime(solution,scoreDirector,"144702-b2038-2015-10-05-2015-10-07-44012373-part1PA",4);
		/*shiftPlanningAssignmentStartTime(solution,scoreDirector,"b2669minPA",15);
		shiftPlanningAssignmentStartTime(solution,scoreDirector,"144838-b2685-2015-10-06-2015-10-20-00000210-part3PA",15);
		//switchMachineSetup(solution,scoreDirector,"PROMO-1737-b2444-week2-part1PA", 5);*/
		
		System.out.println("TEST AFTER");
		test(solution);
		
		// END OF MODIFICATIONS
		
	}
	
	
	private static void test(ProductionPlanning solution) {
		
		for (PlanningAssignment pa : solution.getPlanningAssignmentList()) {
			if (pa.getId().equals("b3050minPA")) {				
				PlanningAssignment prev = pa.getNeighbourhood().getPreviousPlanningAssignment(pa.getMachineSetup().getLine());
				
				/*System.out.println("Is assigned: " + pa.isAssigned());				
				System.out.println("Is fixed: " + pa.isFixed());
				
				PlanningAssignment next = pa.getNeighbourhood().getNextPlanningAssignment(pa.getMachineSetup().getLine());
				if (next != null) {
					System.out.println("NEXT: " + next.getId());
					System.out.println("NEXT IS ASSIGNED: " + next.isAssigned());
				}*/
				
				System.out.println("Time start: " + pa.getTimeStart() + " " + solution.getTimeHandler().getDate(pa.getTimeStart()));
				System.out.println("Time end: " + pa.getTimeEnd() + " " +  solution.getTimeHandler().getDate(pa.getTimeEnd()));
				
				System.out.println("number of time steps " + pa.getNumberOfTimeSteps());
				System.out.println("Additional time " + pa.getAdditionalTime());
				
				//System.out.println("MOQ: " + pa.getMachineSetup().getComponentMaterial().getMoq());
				//System.out.println("line capacity " + pa.getMachineSetup().getComponentMaterial().getLineCapacityInKg());	
				System.out.println("Machine setup: " + pa.getMachineSetup());	
				
				
				for (Material material : pa.getMachineSetup().getMaterialList()) {
					if (!material.isFinalMaterial()) continue;			
					
					System.out.println("Capacity: " + pa.getMachineSetup().getCapacity(material));
					//System.out.println("Is make to order: " + material.isMakeToOrder());
					System.out.println("Amount map value: " + pa.getAmount(material));
					boolean maketoorder = pa.getAmount(material) < pa.getMachineSetup().getCapacity(material)*(pa.getTimeEnd() - pa.getTimeStart());
					System.out.println("MAKETOORDER: " + maketoorder);
					
					/*System.out.println("Material " + material);
					System.out.println("Is make to order: " + material.isMakeToOrder());
					System.out.print(" min: " + material.getMakeToMin());
					System.out.print(" max: " + material.getMakeToMax());
					System.out.println(" avg capacity " + material.getAvgCapacity());
					System.out.println(" capacity " + pa.getMachineSetup().getCapacity(material));*/
					
					if (prev == null) continue;
					for (Material prevMaterial : prev.getMachineSetup().getMaterialList())
					{
						int changeover = solution.getChangeoverMatrix().getChangeover(prevMaterial, material, pa.getMachineSetup().getMachine(material));
						System.out.println("CHANGEOVER ON MACHINE " + pa.getMachineSetup().getMachine(material) +  ": " +  changeover);
					}
				}

				if (prev == null) continue;
				System.out.println("Is right after previous " + pa.isRightAfter(pa.getNeighbourhood().getPreviousPlanningAssignment(pa.getMachineSetup().getLine())));
				System.out.println("Previous assignemnt time end: " + pa.getPreviousAssignmentTimeEnd().get(pa.getMachineSetup().getLine()));
				boolean linePrepNecessary = pa.getPreviousAssignmentTimeEnd().get(pa.getMachineSetup().getLine()) < pa.getTimeStart() - 4;
				System.out.println("Line preparation necessary: " + linePrepNecessary + " for " + pa.getMachineSetup().getComponentMaterial().getStartupTime() + " timesteps");
				
				/*
				$previousAssignmentTimeEnd.intValue() > (timeStart - $linePreparation),
				$line : machineSetup.getLine())
			 */
				
				
				
				System.out.println("Previous assignemnt time start: " + prev.getTimeStart());
				System.out.println("Previous assignemnt: " + prev.getId());
				System.out.println("Previous assignemnt number of timesteps: " + prev.getNumberOfTimeSteps());
				System.out.println("Previous assignemnt additional time: " + prev.getAdditionalTime());
				
				/*PlanningAssignment next = pa.getNeighbourhood().getNextPlanningAssignment(pa.getMachineSetup().getLine());
				String nextComponent = next == null ? "null" : next.getMachineSetup().getComponentMaterial().getName();
				System.out.println("Next planning assignment component material " + nextComponent);*/
			}
			
			/*for (Task task : solution.getTaskList()) {
				if (task.getAssignment().getId().equals("current-a127minPA")) {
					System.out.println();
					System.out.println();
					System.out.println("Task: " + task.getId());
					System.out.println("OFC: " + task.getOfcId());
					System.out.println("MAterial: " + task.getMaterial());
					System.out.println("Amount " + task.getAmount());
				}
			}*/
			
			/*
			if (task.getAssignment().getId().equals("142327-b1403-2015-08-20-2015-09-16-418-part6PA")) {
				int time = task.getAssignment().getTimeEnd() - task.getDueTime();
				System.out.println("less critical, price: " + task.getPrice() + " time: " + time);
			}*/
		}

	}
	
	private static PlanningAssignment getPlanningAssignment(ProductionPlanning solution, String id) {
		for (PlanningAssignment pa : solution.getPlanningAssignmentList()) {
			if (pa.getId().equals(id)) return pa;
		}
		return null;
	}
	
	private static void printMachineSetups(ProductionPlanning solution, String planningAssignmentId) {
		for (PlanningAssignment pa : solution.getPlanningAssignmentList()) {
			if (pa.getId().equals(planningAssignmentId)) {
				System.out.println();
				System.out.println("MACHINE SETUPS:");
				int index = 0;
				for (MachineSetup machineSetup : pa.getAllowedMachineSetupList()) {
					System.out.println("Index: " + index);
					System.out.println("Setup: " + machineSetup);
					for (Material m : machineSetup.getMaterialList()) {
						System.out.println("Material: " + m + "(capacity " + machineSetup.getCapacity(m) + ")");
					}
					index++;
				}
				System.out.println();
			}
		}
	}
	
	private static void multiPackingTest(ProductionPlanning solution) {
		
		Material material = solution.getDictionary().getMaterial("b2037");
		System.out.println("material " + material + " is make to order " + material.isMakeToOrder());
		
		for (PlanningAssignment pa : solution.getPlanningAssignmentList()) {
			if (pa.getId().equals("144712-b2060-2015-10-05-2015-10-07-47114789-part1PA")) {
				for (MachineSetup setup : pa.getAllowedMachineSetupList()) {
					if (setup.getMaterialList().contains(material)) {
						System.out.println("Material can be produced! On " + setup);
						for (Material m : setup.getMaterialList()) {
							System.out.println(m);
						}
					}					
				}
			}
		}
		
	}
	
	private static void switchMachineSetup(ProductionPlanning solution,
			ScoreDirector scoreDirector, String planningAssignmentId, int machineSetupIndex) {
		PlanningAssignment assignment = null;
		
		for (PlanningAssignment pa : solution.getPlanningAssignmentList()) {
			if (pa.getId().equals(planningAssignmentId)) {
				assignment = pa;
			}
		}
		
		if (assignment == null) {
			System.out.println("Planning assignment " + planningAssignmentId + " not found");
			return;
		}
		
		scoreDirector.beforeVariableChanged(assignment, "machineSetup");
		assignment.setMachineSetup(assignment.getAllowedMachineSetupList().get(machineSetupIndex));
		scoreDirector.afterVariableChanged(assignment, "machineSetup");
	}
	
	private static void reassignTask(ProductionPlanning solution, 
			ScoreDirector scoreDirector, String taskId, String planningAssignmentId) {
		
		PlanningAssignment assignment = null;
		
		for (PlanningAssignment pa : solution.getPlanningAssignmentList()) {
			if (pa.getId().equals(planningAssignmentId)) {
				assignment = pa;
			}
		}
		
		if (assignment == null) {
			System.out.println("Planning assignment " + planningAssignmentId + " not found");
			return;
		}
		
		for (Task t : solution.getTaskList()) {
			if (t.getId().equals(taskId)) {
				scoreDirector.beforeVariableChanged(t, "assignment");
				t.setAssignment(assignment);
				scoreDirector.afterVariableChanged(t, "assignment");
				return;
			}
		}
		
		System.out.println("Task " + taskId + " not found");
	}
	
	private static void setAdditionalTime(ProductionPlanning solution, 
			ScoreDirector scoreDirector, String planningAssignmentId, int time) {
		
		for (PlanningAssignment pa : solution.getPlanningAssignmentList()) {
			if (pa.getId().equals(planningAssignmentId)) {
				scoreDirector.beforeVariableChanged(pa, "additionalTime");
				pa.setAdditionalTime(time);			
				scoreDirector.afterVariableChanged(pa, "additionalTime");
				return;
			}
		}
		
		System.out.println("Planning assignment " + planningAssignmentId + " not found");
	}
	
	private static void shiftPlanningAssignmentStartTime(ProductionPlanning solution, 
			ScoreDirector scoreDirector, String planningAssignmentId, int shift) {
		
		for (PlanningAssignment pa : solution.getPlanningAssignmentList()) {
			if (pa.getId().equals(planningAssignmentId)) {
				scoreDirector.beforeVariableChanged(pa, "timeStart");
				int newTime = pa.getTimeStart() + shift;
				System.out.println("Original time " + pa.getTimeStart());
				pa.setTimeStart(pa.getTimeStart() + shift);				
				System.out.println("Shifting planning assignment to time " + newTime);
				scoreDirector.afterVariableChanged(pa, "timeStart");
				return;
			}
		}
		
		System.out.println("Planning assignment " + planningAssignmentId + " not found");
	}
	
	private static void compareSolutions(ProductionPlanning original, ScoreDirector scoreDirectorOriginal,
			ProductionPlanning modified, ScoreDirector scoreDirectorModified) {
		
		Score originalScore = scoreDirectorOriginal.calculateScore();
		Score modifiedScore = scoreDirectorModified.calculateScore();
		
		System.out.println("Original score: " + originalScore);
		System.out.println("Modified score: " + modifiedScore);
		
		if (originalScore.compareTo(modifiedScore) == 0) {
			System.out.println("Scores are the same!");
			return;
		}
		
		String betterScore = originalScore.compareTo(modifiedScore) < 0 
				? "modified score" : "original score";
		System.out.println(betterScore + " is better!");
	}
	
	private static void printSolutionToDb(String dbName,ProductionPlanning productionPlanning) {
		DbConnector.PW_DB = dbName;
		DbConnector.exec("TRUNCATE production_schedule");
		DbConnector.exec("TRUNCATE production_description");
		DbConnector.exec("TRUNCATE production_schedule_broken_constraints");
		DbConnector.exec("TRUNCATE production_changeover_matrix");
		ProductionPlanningDbPrinter dbPrinter = new ProductionPlanningDbPrinter(productionPlanning);
		dbPrinter.print();
	}

}
