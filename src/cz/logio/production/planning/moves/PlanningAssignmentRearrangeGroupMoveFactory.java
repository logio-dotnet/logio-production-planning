package cz.logio.production.planning.moves;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;

public class PlanningAssignmentRearrangeGroupMoveFactory implements
		MoveListFactory<ProductionPlanning> {

	public List<? extends Move> createMoveList(ProductionPlanning solution) {
		List<Move> moveList = new ArrayList<Move>();
        
        for (PlanningAssignment planningAssignment : solution.getPlanningAssignmentList()) {
        	
        	if (planningAssignment.getMachineSetup() == null) continue;
        	if (planningAssignment.isFixed()) continue;
        	
        	Machine line = planningAssignment.getMachineSetup().getLine();
        	PlanningAssignment previous = planningAssignment.getNeighbourhood().getPreviousPlanningAssignment(line);
        	if (previous == null || previous.isFixed() ||
        			previous.getMachineSetup().getComponentMaterial() != planningAssignment.getMachineSetup().getComponentMaterial() ||
        			previous.getTimeEnd() > planningAssignment.getTimeStart()) continue;
        	
        	moveList.add(new PlanningAssignmentRearrangeGroupMove(planningAssignment));
        }

        return moveList;
	}

}
