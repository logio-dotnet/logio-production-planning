package cz.logio.production.planning.moves;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;

public class PlanningAssignmentChainShiftMove implements Move {

	private PlanningAssignment planningAssignment;
	private int startTime;
	
	private List<PlanningAssignment> affectedPlanningAssignments = new ArrayList<PlanningAssignment>();
	private List<Integer> newStartTimes = new ArrayList<Integer>();
	
	public PlanningAssignmentChainShiftMove(List<PlanningAssignment> planningAssignments, int startTime) {
		this.planningAssignment = planningAssignments.get(0);
		this.startTime = startTime;
		this.affectedPlanningAssignments = planningAssignments;
	}
	
	public boolean isMoveDoable(ScoreDirector scoreDirector) {
		return planningAssignment.getTimeStart() != startTime; 
	}

	public Move createUndoMove(ScoreDirector scoreDirector) {
		return new PlanningAssignmentChainShiftMove(affectedPlanningAssignments,planningAssignment.getTimeStart());
	}
	
	public static List<PlanningAssignment> findAffectedPlanningAssignments(PlanningAssignment changingAssignment){
		List<PlanningAssignment> affectedPlanningAssignments = new ArrayList<PlanningAssignment>();
		Material componentMaterial = changingAssignment.getMachineSetup().getComponentMaterial();
		
		while (true) {
			affectedPlanningAssignments.add(changingAssignment);
			
			if (changingAssignment.getMachineSetup() == null) break;
			Machine line = changingAssignment.getMachineSetup().getLine();
			
			PlanningAssignment nextAssignment = changingAssignment.getNeighbourhood().getNextPlanningAssignment(line);
			if (nextAssignment == null || !nextAssignment.isRightAfter(changingAssignment) || 
					nextAssignment.getMachineSetup().getComponentMaterial() != componentMaterial) break;
			
			changingAssignment = nextAssignment;			
		}
		
		return affectedPlanningAssignments;
	}

	public void doMove(ScoreDirector scoreDirector) {
		int startTimeShift = startTime - planningAssignment.getTimeStart();
		
		for (PlanningAssignment assignment : affectedPlanningAssignments) {
			int newTimeStart = assignment.getTimeStart() + startTimeShift;
			newStartTimes.add(newTimeStart);
			
			scoreDirector.beforeVariableChanged(assignment, "timeStart");
			assignment.setTimeStart(newTimeStart);
			scoreDirector.afterVariableChanged(assignment, "timeStart");
		}
	}

	public String getSimpleMoveTypeDescription() {
		String desc = "Shifting chain from " + planningAssignment + " to " + startTime;
		return desc;
	}
	
	public String toString() {
		return getSimpleMoveTypeDescription();
	}

	public Collection<? extends Object> getPlanningEntities() {
		return affectedPlanningAssignments;
	}

	public Collection<? extends Object> getPlanningValues() {
		return newStartTimes;
	}
	
	public boolean equals(Object o) {
        if (this == o) return true; 
        
        else if (o instanceof PlanningAssignmentChainShiftMove) {
        	PlanningAssignmentChainShiftMove other = (PlanningAssignmentChainShiftMove) o;
            return new EqualsBuilder()
                    .append(planningAssignment, other.planningAssignment)
                    .append(startTime, other.startTime)
                    .isEquals();
        } 
        
        else return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(planningAssignment)
                .append(startTime)
                .toHashCode();
    }

}
