package cz.logio.production.planning.moves;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;

public class PlanningAssignmentChainShiftMoveFactory implements
		MoveListFactory<ProductionPlanning> {

	public List<? extends Move> createMoveList(ProductionPlanning solution) {
        List<Move> moveList = new ArrayList<Move>();
        
        for (PlanningAssignment planningAssignment : solution.getPlanningAssignmentList()) {
        	
        	if (planningAssignment.getMachineSetup() == null) continue;
        	if (planningAssignment.isFixed()) continue;
        	
        	Machine line = planningAssignment.getMachineSetup().getLine();
        	PlanningAssignment next = planningAssignment.getNeighbourhood().getNextPlanningAssignment(line);
        	if (next == null || !next.isRightAfter(planningAssignment) || 
        			next.getMachineSetup().getComponentMaterial() != planningAssignment.getMachineSetup().getComponentMaterial()) continue;
        	
        	List<PlanningAssignment> affectedAssignments = PlanningAssignmentChainShiftMove.findAffectedPlanningAssignments(planningAssignment);
        	
        	boolean containsFixedAssignment = false;
        	
        	for (PlanningAssignment assignment : affectedAssignments) {
        		if (assignment.isFixed()) {
        			containsFixedAssignment = true;
        			break;
        		}
        	}
        	
        	if (containsFixedAssignment) continue;
        	
        	int from = Math.max(0,planningAssignment.getTimeStart()-100);
        	int to = Math.max(from, Math.min(solution.getTimePointsAvailable(),planningAssignment.getTimeStart()+100));       			
        	
        	Iterator<Integer> it = ValueRangeFactory.createIntValueRange(from,to).createOriginalIterator();
        	
        	while (it.hasNext()) {
        		moveList.add(new PlanningAssignmentChainShiftMove(affectedAssignments, it.next()));
        	}
        }

        return moveList;
	}

}
