package cz.logio.production.planning.moves;

import java.util.ArrayList;
import java.util.Collection;

import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.PlanningAssignment;

/**
 * Vymeni vyrobu PlanningAssignmentu s predchadzajucim na linke ak pouziva rovnaky polotovar
 * @author Naty
 */
public class PlanningAssignmentRearrangeGroupMove implements Move {
	private PlanningAssignment planningAssignment;
	private PlanningAssignment previousPlanningAssignment;
	
	private int originalStartTime;
	private int originalStartTimePrevious;
	
	private int newStartTime;
	private int newStartTimePrevious;
	
	private boolean doable = false;;
	
	public PlanningAssignmentRearrangeGroupMove(PlanningAssignment planningAssignment) {
		this.planningAssignment = planningAssignment;
		originalStartTime = planningAssignment.getTimeStart();
		if (planningAssignment.getMachineSetup() == null) return;
		
		Machine line = planningAssignment.getMachineSetup().getLine();
		previousPlanningAssignment = planningAssignment.getNeighbourhood().getPreviousPlanningAssignment(line);	
		if (previousPlanningAssignment != null) {
			if (previousPlanningAssignment.getMachineSetup().getComponentMaterial() != 
					planningAssignment.getMachineSetup().getComponentMaterial()) return;
			originalStartTimePrevious = previousPlanningAssignment.getTimeStart();
			
			newStartTime = originalStartTimePrevious;
			int previousAssignmentTime = previousPlanningAssignment.getTimeEnd() - previousPlanningAssignment.getTimeStart();
			newStartTimePrevious = planningAssignment.getTimeEnd() - previousAssignmentTime;
			
			doable = true;
		}
	}
	
	public PlanningAssignmentRearrangeGroupMove(PlanningAssignment assignment1, int startTime1, 
			PlanningAssignment assignment2, int startTime2) {
		this.planningAssignment = assignment1;
		this.previousPlanningAssignment = assignment2;
		
		this.newStartTime = startTime1;
		this.newStartTimePrevious = startTime2;
		
		doable = true;
	}
	
	public boolean isMoveDoable(ScoreDirector scoreDirector) {
		return (previousPlanningAssignment != null && 
				!planningAssignment.isFixed() && !previousPlanningAssignment.isFixed());
	}

	public Move createUndoMove(ScoreDirector scoreDirector) {
		return new PlanningAssignmentRearrangeGroupMove(planningAssignment, originalStartTime, 
				previousPlanningAssignment, originalStartTimePrevious);
	}

	public void doMove(ScoreDirector scoreDirector) {
		scoreDirector.beforeVariableChanged(planningAssignment, "timeStart");
		planningAssignment.setTimeStart(newStartTime);
		scoreDirector.afterVariableChanged(planningAssignment, "timeStart");
		
		scoreDirector.beforeVariableChanged(previousPlanningAssignment, "timeStart");
		previousPlanningAssignment.setTimeStart(newStartTimePrevious);
		scoreDirector.afterVariableChanged(previousPlanningAssignment, "timeStart");
	}

	public String getSimpleMoveTypeDescription() {
		String desc = "Switching " + planningAssignment + " (" + originalStartTime + " -> " +  newStartTime + 
				") and (" + originalStartTimePrevious + " -> " +  newStartTimePrevious + ")";
		return desc;
	}

	public Collection<? extends Object> getPlanningEntities() {
		Collection<PlanningAssignment> planningAssignments = new ArrayList<PlanningAssignment>();
		planningAssignments.add(planningAssignment);
		planningAssignments.add(previousPlanningAssignment);
		return planningAssignments;
	}

	public Collection<? extends Object> getPlanningValues() {
		Collection<Integer> values = new ArrayList<Integer>();
		values.add(newStartTime);
		values.add(newStartTimePrevious);
		return values;
	}

}
