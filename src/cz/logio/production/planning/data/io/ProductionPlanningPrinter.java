package cz.logio.production.planning.data.io;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.Task;
import cz.logio.production.planning.domain.Tube;
import cz.logio.production.planning.utils.SolutionManager;
import cz.logio.production.planning.utils.TimeHandler;

abstract class ProductionPlanningPrinter {
	
	protected SolutionManager solutionManager;
	protected ProductionPlanning productionPlanning;
	protected TimeHandler timeHandler;
	
	protected static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	protected static NumberFormat numberFormat = NumberFormat.getInstance(Locale.GERMANY);
	
	protected char delim = ' ';
	
	public ProductionPlanningPrinter(ProductionPlanning productionPlanning) {
		this.timeHandler = productionPlanning.getTimeHandler();		
		this.solutionManager = new SolutionManager(productionPlanning);
		this.productionPlanning = productionPlanning;
	}
	
	public abstract void print();
	
	protected String getNonfinalMaterials(PlanningAssignment assignment) {
		String nonfinalMaterials = "";
    	
		List<Material> materials = assignment.getMachineSetup().getMaterialList();
    	for (Material material : materials) {
    		if (!material.isFinalMaterial()) {
    			if (!nonfinalMaterials.equals("")) {
    				nonfinalMaterials += delim;
    			}
    			nonfinalMaterials += material.getName();
    		}
    	}
    	
    	return nonfinalMaterials;
	}
	
	protected String getMachines(PlanningAssignment assignment, boolean line) {
		String machines = "";
		
		for (Machine machine : assignment.getMachineSetup().getMachineList()) {
			 if (machine.isLine() == line) {
				 if (!machines.equals("")) {
					 machines += delim;
				 }
				 machines += machine.getName();
			 }
		 }
		
		return machines;
	}
	
	protected String getTubes(PlanningAssignment assignment) {
		String tubes = "";
		
		for (Machine machine : assignment.getMachineSetup().getMachineList()) {
			 Tube tube = assignment.getMachineSetup().getTubeForMachine(machine);
			 if (tube != null) {
				 if (!tubes.equals("")) {
					 tubes += delim;
				 }
				 tubes += tube.getName();
			 }
		 }
		
		return tubes;
	}
	
	private boolean isLinePreparationNecessary(PlanningAssignment previous, PlanningAssignment current) {
		if (previous == null) return false;
		
		return ((previous.getTimeEnd() < (current.getTimeStart()-4)) 
				&& getSanitationTime(current, current.getMachineSetup().getLine()) == 0);
		
	}
	
	protected int getSanitationTime(PlanningAssignment assignment, Machine machine) {
		PlanningAssignment previousAssignment = solutionManager.getPreviousPlanningAssignments(machine, assignment);
		
		Material currentMaterial = assignment.getMachineSetup().getMaterialProducedOnMachine(machine);
		
		int sanitationTime = 0;
		
		if (previousAssignment != null) {
			Material previousMaterial = previousAssignment.getMachineSetup().getMaterialProducedOnMachine(machine);
			
			if (previousMaterial.getGroup() != currentMaterial.getGroup() 
					|| currentMaterial.getOrder() < previousMaterial.getOrder()) {
				sanitationTime = previousMaterial.getGroup().getSanitationTime();
				
				if (machine.isLine() && sanitationTime > 0) { //na linke navyse priprava
					sanitationTime += currentMaterial.getStartupTime();
				}
			}
		}
		
		return sanitationTime;
	}
	
	protected int getPreparationTime(PlanningAssignment assignment, Machine machine) {
		PlanningAssignment previousAssignment = solutionManager.getPreviousPlanningAssignments(machine, assignment);
		
		int startupTime = 0;
		
		if (previousAssignment != null) {
			Material previousMaterial = previousAssignment.getMachineSetup().getMaterialProducedOnMachine(machine);		
			Material currentMaterial = assignment.getMachineSetup().getMaterialProducedOnMachine(machine); 
			
			if (machine.isLine()) {
				if (isLinePreparationNecessary(previousAssignment,assignment)) {
					startupTime = currentMaterial.getStartupTime();
				}
			}
			else {					
				if (previousMaterial != currentMaterial) {
					//FICL: pokud se bal� polotovar ze stejn�ho shluku a tubus z�st�v� stejn�, �as na p��pravu balen� je 0
					if (previousMaterial.getComponentMaterial().getGroup() != currentMaterial.getComponentMaterial().getGroup()) {
						startupTime = currentMaterial.getStartupTime();
					}
				}
			}
		}
		
		return startupTime;
	}
	
	protected int getTubeSwitchTime(PlanningAssignment assignment, Machine machine) {
		PlanningAssignment previousAssignment = solutionManager.getPreviousPlanningAssignments(machine, assignment);
		
		if (previousAssignment != null) {
			
			Tube previousTube = previousAssignment.getMachineSetup().getTubeForMachine(machine);
			Tube tube = assignment.getMachineSetup().getTubeForMachine(machine);
			
			if (previousTube != null && tube != null && previousTube != tube) {
				return tube.getSwitchTime();
			}
		}
		
		return 0;
	}
	
	protected int getCriticalAmount(PlanningAssignment planningAssignment, Material material) {
		List<Task> tasks = solutionManager.getTasks(planningAssignment);
		
		int criticalAmount = 0;
		
		for (Task t : tasks) {
			if (t.getMaterial() == material && t.isOfc()){
				criticalAmount += t.getAmount();
			}
		}
		
		return criticalAmount;
	}
}
