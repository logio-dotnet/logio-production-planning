package cz.logio.production.planning.data.io;

import java.io.File;

import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.persistence.common.api.domain.solution.SolutionFileIO;
import org.optaplanner.persistence.xstream.impl.domain.solution.XStreamSolutionFileIO;

import cz.logio.production.planning.app.ProductionPlanningApp;
import cz.logio.production.planning.domain.ProductionPlanning;

public class ProductionPlanningFileIO implements SolutionFileIO {

	public String getInputFileExtension() {
		return "xml";
	}

	public String getOutputFileExtension() {
		return "xml";
	}

	public Solution read(File inputSolutionFile) {
		XStreamSolutionFileIO xStreamSolutionFileIO = new XStreamSolutionFileIO();	
		ProductionPlanning loadedSolution = (ProductionPlanning) xStreamSolutionFileIO.read(inputSolutionFile);
		loadedSolution.computeChangeoverMatrix();
	    return loadedSolution;
	}

	public void write(Solution solution, File outputSolutionFile) {
		XStreamSolutionFileIO xStreamSolutionFileIO = new XStreamSolutionFileIO();    
	    ProductionPlanning copyOfProductionPlanning = ((ProductionPlanning)solution).planningClone();
	    copyOfProductionPlanning.setChangeoverMatrix(null);    
	    xStreamSolutionFileIO.write(copyOfProductionPlanning, outputSolutionFile);
	}

}
