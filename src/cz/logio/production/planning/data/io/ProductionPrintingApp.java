package cz.logio.production.planning.data.io;

import java.io.File;
import org.optaplanner.persistence.xstream.impl.domain.solution.XStreamSolutionFileIO;
import cz.logio.production.planning.domain.ProductionPlanning;

public class ProductionPrintingApp {
	
	public static void main(String[] args) {
		
		//String pathToSolution = "data/productionPlanning/solved/production_'1B'_3days_2015-05-25.xml";
		String pathToSolution = "data/productionPlanning/solved/production_2_5days_2015-06-05.xml";
		
		File solutionFile = new File(pathToSolution);
		XStreamSolutionFileIO xStreamSolutionFileIO = new XStreamSolutionFileIO();
		ProductionPlanning productionPlanning = (ProductionPlanning) xStreamSolutionFileIO.read(solutionFile);
		
		ProductionPlanningDbPrinter printer = new ProductionPlanningDbPrinter(productionPlanning);
		printer.print();
	}

}
