package cz.logio.production.planning.data.io;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.score.constraint.ConstraintMatch;
import org.optaplanner.core.api.score.constraint.ConstraintMatchTotal;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import cz.logio.production.planning.domain.*;
import cz.logio.production.planning.utils.DbConnector;

public class ConstraintLogger {
	private Solution solution;
	private ScoreDirector scoreDirector;

	private Map<String,Integer> brokenConstraintsCount = new HashMap<String,Integer>();
	
	public void setSolver(Solver solver) {
		//this.solver = solver;
		scoreDirector = solver.getScoreDirectorFactory().buildScoreDirector();
		solution = solver.getBestSolution();
	}
	
	public void log() {		
		scoreDirector.setWorkingSolution(solution);

		Collection<ConstraintMatchTotal> constraintMatchTotals = scoreDirector.getConstraintMatchTotals();
		
		try {
			Connection conn = DbConnector.getConnectionToPwDB();
			if (conn == null) return;
			
			Statement st = conn.createStatement();
			
			for (ConstraintMatchTotal cmt: constraintMatchTotals) {
				logConstraintMatchTotal(cmt,st);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	private void logConstraintMatchTotal(ConstraintMatchTotal cmt,Statement st) throws SQLException {
		
		String constraintName = cmt.getConstraintName();
		int countTotal = cmt.getConstraintMatchCount();
		brokenConstraintsCount.put(constraintName, countTotal);
		
		//if (cmt.getScoreLevel() > 0) return; //non-hard constraint
		
		//System.out.println(constraintName + " count: " + countTotal + " level " + cmt.getScoreLevel());
		//Number weightTotal = cmt.getWeightTotalAsNumber();
		
	    for (ConstraintMatch constraintMatch : cmt.getConstraintMatchSet()) {
	    	if (constraintMatch.getConstraintName().equals("machinePreparation")) continue;
	    	logSingleConstraintMatch(constraintMatch,st);
	    }
	}
	
	private void logSingleConstraintMatch(ConstraintMatch constraint,Statement st) throws SQLException {
		List<Object> justificationList = constraint.getJustificationList();
		
		PlanningAssignment planningAssignment1 = null;
		PlanningAssignment planningAssignment2 = null;
		Task task = null;
		Machine machine = null;
		
		//create objects
		for (Object o : justificationList) {
			
			if (o instanceof Task) {
				if (task != null) System.out.println("Multiple tasks in single constraint");			
				task = (Task)o;
				continue;
			}
			
			if (o instanceof Machine) {
				if (machine != null) System.out.println("Multiple machines in single constraint");
				machine = (Machine)o;
				continue;
			}
			
			if (o instanceof PlanningAssignment) {
				if (planningAssignment1 != null && planningAssignment2 != null) {
					System.out.println("More than 2 planning assignments in single constraint");
				}
				
				if (planningAssignment1 == null) {
					planningAssignment1 = (PlanningAssignment)o;
					continue;
				}
				
				PlanningAssignment pa = (PlanningAssignment)o;
				
				if (planningAssignment1.getTimeStart() < pa.getTimeStart()) {
					planningAssignment2 = pa;
				}
				else {
					planningAssignment2 = planningAssignment1;
					planningAssignment1 = pa;
				}
			}	
		}
		
		
		//log them into DB
		String ruleName = "'" + constraint.getConstraintName() + "'";
		String pa1String = planningAssignment1 == null ? "NULL" : "'" + planningAssignment1.getId() + "'";
		String pa2String = planningAssignment2 == null ? "NULL" : "'" + planningAssignment2.getId() + "'";	
		String taskString = task == null ? "NULL" : "'" + task.getId() + "'";
		String machineString = machine == null ? "NULL" : "'" + machine.getName() + "'";
		
		String sql = "INSERT INTO production_schedule_broken_constraints VALUES (" + 
				ruleName + "," + pa1String + "," + pa2String + "," + 
				taskString + "," + machineString + ");";
		
		st.executeUpdate(sql);
		
	}
	
	public void printIncorrectAssignments(String ruleName) {
		System.out.println("Planning assignments that break the rule " + ruleName);
		
		Collection<ConstraintMatchTotal> constraintMatchTotals = scoreDirector.getConstraintMatchTotals();
		
		for (ConstraintMatchTotal cmt: constraintMatchTotals) {
			for (ConstraintMatch constraintMatch : cmt.getConstraintMatchSet()) {
		    	if (constraintMatch.getConstraintName().equals(ruleName)) {
		    		List<Object> justificationList = constraintMatch.getJustificationList();
		    		for (Object o : justificationList) {			
		    			if (o instanceof PlanningAssignment) {
		    				System.out.println(((PlanningAssignment)o).getId());
		    			}
		    		}
		    	}
		    }
		}
		
		System.out.println();
	}

	public Solution getSolution() {
		return solution;
	}

	public void setSolution(Solution solution) {
		this.solution = solution;
	}
	
	
}
