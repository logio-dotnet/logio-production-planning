package cz.logio.production.planning.data.io;

import java.util.Arrays;

import org.optaplanner.core.api.domain.solution.Solution;

import cz.logio.production.planning.app.Configuration;
import cz.logio.production.planning.data.load.ModelLoader;
import cz.logio.production.planning.data.load.ProblemLoader;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.utils.DbConnector;
import cz.logio.production.planning.utils.Dictionary;
import cz.logio.production.planning.utils.TimeHandler;

public class ProductionPlanningDbIO {
	
	private Configuration configuration;
	
	public ProductionPlanningDbIO(String dbName, Configuration configuration) {
		DbConnector.PW_DB = dbName;
		this.configuration = configuration;
	}
	
	public ProductionPlanning read() {
		Dictionary dictionary = new Dictionary();
		
		TimeHandler timeHandler = new TimeHandler(configuration.TIME_STEP_LENGTH, configuration.pwDateTime);
		timeHandler.setDaysAvailable(configuration.timeWindow);
		timeHandler.setDaysOfc(configuration.timeWindowOfc);
		timeHandler.setDaysProductionSuggested(configuration.timeWindowProductionSuggested);
		
		ModelLoader loader = new ModelLoader(dictionary,timeHandler);
		
		loader.load(Arrays.asList(configuration.LINES));
	
	    ProductionPlanning unsolvedProductionPlanning = new ProductionPlanning();
	     
	    unsolvedProductionPlanning.setGroupList(loader.groupList);
	    unsolvedProductionPlanning.setMachineList(loader.machineList);
	    unsolvedProductionPlanning.setMachineSetupList(loader.machineSetupList);
	    unsolvedProductionPlanning.setMaterialList(loader.materialList);
	    unsolvedProductionPlanning.setTubeList(loader.tubeList);
	     
	    unsolvedProductionPlanning.setTimeHandler(timeHandler);
	    unsolvedProductionPlanning.setDictionary(dictionary);
	    
	    unsolvedProductionPlanning.computeChangeoverMatrix();
	    
	    importProblem(unsolvedProductionPlanning);
	        
	    return unsolvedProductionPlanning;
	}
	
	/**
	 * Imports new production suggested loaded from csv into the given ProductionPlanning. 
	 * If materialNameToMaterialMap is null, it is created from the ProductionPlanning.
	 *  
	 * @param productionPlanning
	 * @param materialNameToMaterialMap
	 */
	private static void importProblem(ProductionPlanning productionPlanning) {
		ProblemLoader loader = new ProblemLoader(productionPlanning.getDictionary(),
				productionPlanning.getTimeHandler());
		loader.loadProblem();
		
		productionPlanning.setShiftList(loader.shiftList);
		productionPlanning.setTaskList(loader.taskList);
		productionPlanning.setPlanningAssignmentList(loader.planningAssignmentList);
		productionPlanning.setTrees(loader.trees);
	}
	
	protected void finalize () {
	    DbConnector.closeConnections();
	}	
}
