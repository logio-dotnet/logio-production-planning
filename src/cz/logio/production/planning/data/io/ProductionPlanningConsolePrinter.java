package cz.logio.production.planning.data.io;

import java.util.List;
import java.util.Map.Entry;

import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.Task;

public class ProductionPlanningConsolePrinter extends ProductionPlanningPrinter{
	
	public ProductionPlanningConsolePrinter(ProductionPlanning productionPlanning) {
		super(productionPlanning);
	}
	
	@Override
	public void print() {
		System.out.println("Printing started");
		
		for (PlanningAssignment planningAssignment : solutionManager.getSortedPlanningAssignments()) {
			
			String plannedMachines = "[ " + 
					getMachines(planningAssignment,true) +
					getMachines(planningAssignment, false) + "]";
	    	 
	    	String necessaryMaterials = "[ " + getNonfinalMaterials(planningAssignment) + "]";
	    	
	    	for (Entry<Material,Integer> entry : planningAssignment.getAmountMap().entrySet()) {
	    		Material material = entry.getKey();
	    		int amountRequired = entry.getValue(); 
	    		
	    		//amount validation
		    	List<Task> tasks = solutionManager.getTasks(planningAssignment);
		    	if (tasks == null) continue;
		    	
		    	int amount = 0;
		    	amount = planningAssignment.getMachineSetup().getCapacity(material)*(planningAssignment.getTimeEnd() - planningAssignment.getTimeStart());
		    	 
		    	 System.out.println("Planning Assignment ID: " + planningAssignment.getId() + 
		    			 " Material: " + material.getName() +
		    			 " Start time: " + planningAssignment.getTimeStart() + 
		    			 " End time: " + planningAssignment.getTimeEnd() + 
		    			 " Materials " + necessaryMaterials +
		    			 " Amount: " + amountRequired + "/" + amount + 
		    			 " Machines: " + plannedMachines);
	    	}
		}
		
		System.out.println();
	    System.out.println();
	    /*
	    for (PlanningAssignment planningAssignment : solutionManager.getSortedPlanningAssignments()) {
	    	List<Task> tasks = solutionManager.getTasks(planningAssignment);
	    	if (tasks == null) continue;
	    	
	    	for (Task task : tasks) {
	    		
		    	System.out.println(
		    			"Planning Assignment ID: " + planningAssignment.getId() + 
		    			" Task ID: " + task.getId() +
		    			" Planning Assignment amountSum: " + planningAssignment.getAmountSum() +
		    			" Task material: " + task.getMaterial().getName() +
		    			" Assignment start time: " + planningAssignment.getTimeStart() + 
		    			" Assignment end time: " + planningAssignment.getTimeEnd() + 
		    			" Task due time: " + task.getDueTime() +
		    			" Task amount: " + task.getAmount());		
	    	}    	
	    }*/
	}
	
	public void printUsingTreeStructure() {
		
		for (Machine machine : productionPlanning.getMachineList()) {
			System.out.println("MACHINE " + machine);
			PlanningAssignment current = productionPlanning.getTrees().get(machine).pollFirst();
			
			/*while (current != null) {
				System.out.println(current.getTimeStart() + "-" + current.getTimeEnd() + ": " + current.getNumericId() + 
						" amount: " + current.getAmountSum() + " assigned: " + 
						current.isAssigned() + " additional time " + current.getAdditionalTime());
				current = current.getNeighbourhood().getNextPlanningAssignment(machine);
			}*/
			
		}
	}
}
