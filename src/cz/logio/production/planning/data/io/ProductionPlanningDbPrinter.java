package cz.logio.production.planning.data.io;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import cz.logio.production.planning.app.ProductionPlanningApp;
import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.MachineSetup;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.ProductionPlanning;
import cz.logio.production.planning.domain.Task;
import cz.logio.production.planning.domain.Tube;
import cz.logio.production.planning.utils.ChangeoverMatrix;
import cz.logio.production.planning.utils.ChangeoverMatrix.Changeover;
import cz.logio.production.planning.utils.DbConnector;


public class ProductionPlanningDbPrinter extends ProductionPlanningPrinter {
	
	private ChangeoverMatrix changeoverMatrix;
	
	public ProductionPlanningDbPrinter(ProductionPlanning productionPlanning) {
		super(productionPlanning);
		
		changeoverMatrix = productionPlanning.getChangeoverMatrix();
		
		delim = '|'; 
		numberFormat = NumberFormat.getInstance(Locale.US);
	}
	
	@Override
	public void print() {
		
		try {
			Connection conn = DbConnector.getConnectionToPwDB();
			if (conn == null) return;
			
			Statement st = conn.createStatement();
			
			printPlan(st);
			if (!ProductionPlanningApp.DEBUG) printChangeoverMatrix(st);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	private void printChangeoverMatrix(Statement st) throws SQLException {	
		List<Changeover> changeovers = changeoverMatrix.getChangeovers();
		
		for (Changeover changeover : changeovers) {    	
			printChangeover(st,changeover);
		}	
	}
	
	private void printChangeover(Statement st, Changeover changeover) throws SQLException {
		String sql = "INSERT INTO production_changeover_matrix VALUES (" + 
				quote(changeover.getMaterialFrom().getName()) + "," + 
				quote(changeover.getMaterialTo().getName()) + "," +
				quote(changeover.machine.getName()) + "," +
				changeover.getChangeoverTimeInMinutes() + "," +
				quote(changeover.getChangeoverDescription()) + ");";
		
		st.executeUpdate(sql);
	}
	
	private void printPlan(Statement st) throws SQLException, ParseException {
		
		for (PlanningAssignment assignment : solutionManager.getSortedPlanningAssignments()) {			
			if (!assignment.isAssigned() ||assignment.getTimeEnd() == assignment.getTimeStart()) continue;
			
			//packaging preparation - before production
			printPreparation(st,assignment);
			
			//production
			printProduction(st,assignment);
			
			//sanitation - after production
			printSanitation(st,assignment);
			
			//production_suggested to production_schedule mapping
			printProductionDescription(st,assignment);		
		}
						
	}
	
	private void printProductionDescription(Statement st, PlanningAssignment assignment) throws ParseException, SQLException {
		for (Task task : solutionManager.getTasks(assignment)) {
			String sql = "INSERT INTO production_description VALUES (" + 
					quote(task.getId()) + "," + 
					quote(assignment.getId()) + "," +
					quote(dateFormat.format(timeHandler.getDate(task.getDueTime()))) + "," +
					quote(dateFormat.format(timeHandler.getDate(assignment.getTimeEnd()))) + ");";
			
			st.executeUpdate(sql);
		}
	}
	
	private void printProduction(Statement st, PlanningAssignment assignment) throws SQLException, ParseException {
		MachineSetup machineSetup = assignment.getMachineSetup();
		String line = machineSetup.getLine().getName();
		String nonfinalMaterials = getNonfinalMaterials(assignment);
		
		int earliestDueTime = getEarliestDueTime(assignment);
		int latestDueTime = getLatestDueTime(assignment);
		
		int timeSteps = assignment.getTimeEnd()-assignment.getTimeStart();
			
		for (Material material : assignment.getMachineSetup().getMaterialList()) {
			if (!material.isFinalMaterial()) continue;
			int amountPlanned = assignment.getAmount(material);
						
			String packer =  machineSetup.getMachine(material) == null ? "---" : machineSetup.getMachine(material).getName();
			String tube = material.getTube().getName();
			
			int capacity = assignment.getMachineSetup().getCapacity(material);
			int amountProduced = (int)(capacity*timeSteps);
			amountProduced = (int)(material.getPaletteAmount()*(Math.floor(amountProduced/material.getPaletteAmount())));
					
			int criticalAmount = getCriticalAmount(assignment,material);
			
			String sql = "INSERT INTO production_schedule VALUES (" +
					quote(assignment.getId()) + "," +
					quote("výroba") + "," +
					quote(dateFormat.format(timeHandler.getDate(assignment.getTimeStart()))) + "," +
					quote(dateFormat.format(timeHandler.getDate(assignment.getTimeEnd()))) + "," +
					numberFormat.format(timeHandler.timeStepsToHours(timeSteps)) + "," +
					quote(dateFormat.format(timeHandler.getDate(earliestDueTime))) + "," +
					quote(dateFormat.format(timeHandler.getDate(latestDueTime))) + "," +
					amountProduced + "," +
					amountPlanned + "," +
					quote(material.getName()) + "," +
					quote(nonfinalMaterials) + "," +
					quote(line) + "," +
					quote(packer) + "," +
					quote(tube) + "," +
					assignment.getMachineSetup().getWorkers() +	"," +
					criticalAmount + ");";	
			
			st.executeUpdate(sql);
		}	
	}
	
	private void printSanitation(Statement st, PlanningAssignment assignment) throws ParseException, SQLException {
		
		for (Machine machine : assignment.getMachineSetup().getMachineList()) {
			int sanitationTime = getSanitationTime(assignment,machine);
			
			if (machine.isLine()) printSideTask(st,assignment,machine,"sanitácia a príprava - linka",sanitationTime);
			if (!machine.isLine()) printSideTask(st,assignment,machine,"sanitácia - balicka",sanitationTime);
		}
		
	}
	
	private void printPreparation(Statement st, PlanningAssignment assignment) throws ParseException, SQLException {
		
		for (Machine machine : assignment.getMachineSetup().getMachineList()) {
			
			int tubeSwitchTime = getTubeSwitchTime(assignment,machine);
			
			printSideTask(st,assignment,machine,"výmena tubusu (vrátane prípravy)",tubeSwitchTime);
			
			if (tubeSwitchTime == 0 || machine.isLine()) {
			
				int preparationTime = getPreparationTime(assignment,machine);
				
				String description = "príprava balenia";
				
				if (machine.isLine()) {
					description = "príprava linky";
				}
				
				printSideTask(st,assignment,machine,description,preparationTime);
			}
		}
	}
	
	private void printSideTask(Statement st, PlanningAssignment assignment, Machine machine, 
			String definition, int timeSteps) throws ParseException, SQLException {
		
		if (timeSteps != 0) {
			
			String line = (!machine.isLine()) ? "NULL" : quote(machine.getName());
			String packer = (machine.isLine()) ? "NULL" : quote(machine.getName());
			
			Tube t = assignment.getMachineSetup().getTubeForMachine(machine);
			String tube = t == null ? "NULL" : quote(t.getName());
			
			String sql = "INSERT INTO production_schedule VALUES (" +
					quote(assignment.getId()) + "," +
					quote(definition) + "," +
					quote(dateFormat.format(timeHandler.getDate(assignment.getTimeStart() - timeSteps))) + "," +
					quote(dateFormat.format(timeHandler.getDate(assignment.getTimeStart()))) + "," +
					numberFormat.format(timeHandler.timeStepsToHours(timeSteps)) + "," +
					"NULL," + //earliest due time
					"NULL," + //latest due time
					"NULL," + //amount produced
					"NULL," + //amount 
					"NULL," + //final material
					"NULL," + //nonfinal materials
					line + "," +
					packer + "," +
					tube + "," +
					assignment.getMachineSetup().getWorkers() + "," +
					0 + ");";
			
			st.executeUpdate(sql);	
		}
	}
	
	private String quote(String string) {
		return "'" + string + "'";
	}
	
	private int getEarliestDueTime(PlanningAssignment assignment) {
		List<Task> tasks = solutionManager.getTasks(assignment);
		
		int minTime = timeHandler.getTimePointsAvailable();
		
		if (tasks != null) {
			for (Task task : tasks) {
				if (task.getDueTime() < minTime) {
					minTime = task.getDueTime();
				}
			}
		}
		
		return minTime;
	}
	
	private int getLatestDueTime(PlanningAssignment assignment) {
		List<Task> tasks = solutionManager.getTasks(assignment);
		
		int maxTime = 0;
		
		if (tasks != null) {
			for (Task task : tasks) {
				if (task.getDueTime() > maxTime) {
					maxTime = task.getDueTime();
				}
			}
		}
		
		return maxTime;
	}
}
