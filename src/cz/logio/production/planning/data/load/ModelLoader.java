package cz.logio.production.planning.data.load;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import cz.logio.production.planning.app.Configuration;
import cz.logio.production.planning.domain.Group;
import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.MachineSetup;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.Tube;
import cz.logio.production.planning.utils.DbConnector;
import cz.logio.production.planning.utils.Dictionary;
import cz.logio.production.planning.utils.TimeHandler;

public class ModelLoader {
	
	private Dictionary dictionary; 
	
	private TimeHandler timeHandler;
	
	public List<Group> groupList = new ArrayList<Group>();	
	
	public List<Material> materialList = new ArrayList<Material>();
	
	public List<MachineSetup> machineSetupList = new ArrayList<MachineSetup>();
	
	public List<Machine> machineList = new ArrayList<Machine>();
	
	public List<Tube> tubeList = new ArrayList<Tube>();
	
	public ModelLoader(Dictionary dictionary, TimeHandler timeHandler) {
		this.dictionary = dictionary;
		this.timeHandler = timeHandler;
	}
	
	public void load(List<String> lines) {
		
		String linesQueryPart = "";
		if (lines != null && !lines.isEmpty()) {
			linesQueryPart = " AND line IN (" + StringUtils.join(lines,",") + ")";
		}
		
		System.out.println(linesQueryPart);

		try {		
			Connection conn = DbConnector.getConnectionToPwDB();
			if (conn == null) return;
		  
			Statement st = conn.createStatement();
			ResultSet res = st.executeQuery("SELECT *, make_to_order != 0 AS is_make_to_order FROM " + DbConnector.PRODUCTION_INFO_TABLE + 
					  " LEFT JOIN import_product ip ON product_id = ip.id_in_pw "
					+ " LEFT JOIN product p on product_id = p.id "
					+ " LEFT JOIN import_product_locator ipl ON ipl.product_id_in_pw = p.id"
					+ " LEFT JOIN product_locator pl ON ipl.id_in_pw = pl.id "
					+ " LEFT JOIN product_locator_attributes pla ON ipl.id_in_pw = pla.product_locator_id "
					+ " WHERE ipl.c1_id_from_customer = 'BON' "
					+ "" + linesQueryPart + ";");
			
			while (res.next()) {
				processResultSet(res);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		  	}	
		
		loadIncompatibleGroups();		
		loadMultiPacking();
		
		System.out.println(groupList.size() + " product groups loaded");
		System.out.println(materialList.size() + " materials loaded");
		System.out.println(machineSetupList.size() + " machine setups loaded");
		System.out.println(machineList.size() + " machines loaded");
	}
	
	private void loadIncompatibleGroups() {
		try {		
			Connection conn = DbConnector.getConnectionToPwDB();
			if (conn == null) return;
		  
			Statement st = conn.createStatement();
			ResultSet res = st.executeQuery("SELECT * FROM bonavita_production_incompatible_groups;");
			
			while (res.next()) {
				String groupName1 = res.getString("group_1_id");
				String groupName2 = res.getString("group_2_id");
				
				Group group1 = dictionary.getGroup(groupName1);
				Group group2 = dictionary.getGroup(groupName2);
				
				if (group1 != null && group2 != null) {
					group1.addIncompatibleGroup(group2);
					group2.addIncompatibleGroup(group1);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		  	}
	}
	
	private void loadMultiPacking() {
		List<String> multiPackingLines = Arrays.asList(Configuration.MULTIPACKING);	
		Map<String,List<Material>> lineNamesToMaterials = getFinalMaterialsForLines(multiPackingLines);	
		
		for (Material material : materialList) {
			if (!material.isFinalMaterial()) continue;
			
			List<Material> sameLineMaterials = lineNamesToMaterials.get(material.getLine().getName());
			if (sameLineMaterials == null) continue;
			
			for (MachineSetup simpleMachineSetup : material.getSimpleMachineSetups()) {	
				int lineCapacity = simpleMachineSetup.getCapacity(material.getComponentMaterial());
				int packerCapacity = simpleMachineSetup.getCapacity(material);
				
				if (lineCapacity > packerCapacity) { //create multi-packer machine setups				
					for (Material sameLineMaterial : sameLineMaterials) {
						if (sameLineMaterial.isMakeToOrder()) continue;
						if (sameLineMaterial == material) continue;
						if (material.getComponentMaterial() == sameLineMaterial.getComponentMaterial()) {
							mergeSimpleMachineSetups(material,simpleMachineSetup,sameLineMaterial);
						}
					}
				}		
			}		
		}
	}
	
	private void mergeSimpleMachineSetups(Material material, MachineSetup machineSetup, Material otherMaterial) {
		int lineCapacity = machineSetup.getCapacity(material.getComponentMaterial());
		int packerCapacity = machineSetup.getPackerCapacity(material);
		
		for (MachineSetup otherMachineSetup : otherMaterial.getSimpleMachineSetups()) {
			Machine otherPacker = otherMachineSetup.getMachine(otherMaterial);
			if (otherPacker == machineSetup.getMachine(material)) continue;
			
			int otherPackerCapacity = otherMachineSetup.getPackerCapacity(otherMaterial);				
			int capacity = Math.min(otherPackerCapacity,lineCapacity-packerCapacity);
			
			MachineSetup mergedMachineSetup = machineSetup.clone();
			mergedMachineSetup.addProduction(otherMaterial,capacity,otherPacker);
			
			material.addAllowedMachineSetup(mergedMachineSetup);
			machineSetupList.add(mergedMachineSetup);
		}
	}
	
	private Map<String,List<Material>> getFinalMaterialsForLines(List<String> lineNames) {
		Map<String,List<Material>> lineNameToMaterials = new HashMap<String,List<Material>>();
		
		for (String lineName : lineNames) {
			lineNameToMaterials.put(lineName, new ArrayList<Material>());
		}
		
		for (Material material : materialList) {
			if (!material.isFinalMaterial()) continue;
			
			List<Material> materials = lineNameToMaterials.get(material.getLine().getName());
			
			if (materials != null) {
				materials.add(material);
			}
		}
		
		return lineNameToMaterials;
			}

	
	/**
	 * Processes one mxcen table row
	 * 
	 * @param result table row to process
	 * @throws SQLException 
	 */
	private void processResultSet(ResultSet result) throws SQLException {
		
		String materialName = result.getString("id_from_customer");
		double materialWeight = result.getDouble("weight");
		double paletteAmount = result.getDouble("palette_amount");
		
		//LINKA
		String lineName = result.getString("line").toUpperCase(); 
		String secondaryLineName = result.getString("secondary_line").toUpperCase();
		String componentMaterialName = result.getString("production_material");
		int componentMaterialMoq = result.getInt("production_material_moq");
		double lineStartupTime = result.getDouble("line_preparation_time");
		int order = result.getInt("order");
		String lineGroupName = result.getString("line_group");
		int lineTactInKg = result.getInt("line_tact");
		if (lineTactInKg == 0) lineTactInKg = 60; //minimalny nenulovy takt z dat
		int lineTact = (int)(lineTactInKg/materialWeight);
		
		boolean lineSanitationBeforeSwitchingGroup = result.getBoolean("line_change_over_sanitation");
		int lineSanitationPeriod = result.getInt("line_sanitation_period");
		double lineSanitationTime = result.getDouble("line_sanitation_time");
		int lineSanitationWorkers = result.getInt("line_sanitation_workers");
		
		//BALICKY
		String pckgGroupName = result.getString("packer_group");
			
		boolean pckgSanitationBeforeSwitchingGroup = result.getBoolean("packer_change_over_sanitation");
		int pckgSanitationPeriod = result.getInt("packer_sanitation_period");
		double pckgSanitationTime = result.getDouble("packer_sanitation_time");
		String tubeName = result.getString("tube");
		
		//HLAVNA BALICKA
		String pckg1Name = result.getString("primary_packer");
		int pckg1Tact = result.getInt("primary_packer_tact");
		int pckg1Workers = result.getInt("primary_packer_workers");
		double pckg1StartupTime = result.getDouble("primary_packer_preparation_time");
		
		//NAHRADNA BALICKA
		String pckg2Name = result.getString("secondary_packer");
		int pckg2Tact = result.getInt("secondary_packer_tact");
		int pckg2Workers = result.getInt("secondary_packer_workers");
		double pckg2StartupTime = result.getDouble("secondary_packer_preparation_time");
		
		boolean makeToOrder = result.getBoolean("is_make_to_order");
		
		//postupne sa kontroluje, ci vsetko existuje, je spravne pospajane
		//v pripade potreby sa vytvoria nove objekty, modifikuju sa vazby
		
		Group lineGroup = getGroup(lineGroupName,lineSanitationBeforeSwitchingGroup,
				lineSanitationPeriod,lineSanitationTime,lineSanitationWorkers);
		
		Group pckgGroup = getGroup(pckgGroupName,pckgSanitationBeforeSwitchingGroup,
				pckgSanitationPeriod,pckgSanitationTime,0);
		
		Tube tube = getTube(tubeName);
		
		Machine line = getMachine(lineName,true);
		
		Machine secondaryLine = getMachine(secondaryLineName, true);
		
		Machine pckg1 = getMachine(pckg1Name,false);
		
		Machine pckg2 = getMachine(pckg2Name,false);
		
		Material component = getMaterial(componentMaterialName, lineGroup, order, lineStartupTime, null);
		component.setFinalMaterial(false);
		component.setMoq(componentMaterialMoq);
		component.setLineCapacityInKg((int)(lineTactInKg/60.0*TimeHandler.timeStepLength));
		
		double pckgStartupTime = Math.max(pckg1StartupTime,pckg2StartupTime);
		
		Material finalMaterial = getMaterial(materialName,pckgGroup,1,pckgStartupTime,tube);
		finalMaterial.setFinalMaterial(true);
		finalMaterial.setComponentMaterial(component);
		finalMaterial.setWeight(materialWeight);
		finalMaterial.setMakeToOrder(makeToOrder);
		finalMaterial.setMoq((int)paletteAmount);
		finalMaterial.setPaletteAmount((int)paletteAmount);
		
		setMachineSetupComponent(component, line, secondaryLine, lineTact);
		
		setMachineSetupFinal(finalMaterial,component,line, secondaryLine, lineTact,
				pckg1,pckg1Tact,pckg1Workers,pckg2,pckg2Tact,pckg2Workers);
		
		//spocita priemerny takt este predtym nez sa pridaju dvojite balenia...
		finalMaterial.getAvgCapacity(); 
	}
	
	private void setMachineSetupComponent(Material material, Machine line,
			Machine secondaryLine, int tact) {
		
		material.setLine(line);
		
		if (secondaryLine != null){
			material.setSecondaryLine(secondaryLine);
		}
		
		if (material.getSecondaryPacker() != null) {
			material.setSecondaryPacker(null);
		}
		
		if (material.getPrimaryPacker() == null) {
			MachineSetup machineSetup = new MachineSetup();
			material.addAllowedMachineSetup(machineSetup);
			
			int capacity = (int)(tact/60.0*TimeHandler.timeStepLength);
			machineSetup.addProduction(material, capacity, line);
			
			machineSetupList.add(machineSetup);
			
			if (secondaryLine != null) {
				MachineSetup machineSetup2 = new MachineSetup();
				material.addAllowedMachineSetup(machineSetup2);
				machineSetup2.addProduction(material, capacity, secondaryLine);	
				machineSetupList.add(machineSetup2);
			}
		}		
	}
	
	/**
	 * Pri zmene mozu ostat stare povolene machine setupy pre dany material, TODO ak bude potrebne
	 */
	private void setMachineSetupFinal(Material material, Material component, 
			Machine line, Machine secondaryLine, int tactLine, 
			Machine pckgPrimary, int tactPrimary, int workersPrimary, 
			Machine pckgSecondary, int tactSecondary, int workersSecondary) {
		
		int capacityLine = (int)(tactLine/60.0*TimeHandler.timeStepLength);
		
		material.setLine(line);
		material.setSecondaryLine(secondaryLine);
		
		//PRIMARY MACHINE SETUP
		if (material.getPrimaryPacker() == null) {
			material.setPrimaryPacker(pckgPrimary);
			
			MachineSetup primaryMachineSetup = new MachineSetup();
			material.addAllowedMachineSetup(primaryMachineSetup);
			machineSetupList.add(primaryMachineSetup);
			
			int capacityPrimary = (int)(tactPrimary/60.0*TimeHandler.timeStepLength);		
			
			primaryMachineSetup.addProduction(component, capacityLine, line);
			primaryMachineSetup.addProduction(material, capacityPrimary, pckgPrimary);		
			primaryMachineSetup.setWorkers(workersPrimary);
			
			//WITH SECONDARY LINE - dolepene, TODO: urobit krajsie
			if (secondaryLine != null) {
				MachineSetup primaryMSwithSecondaryLine = new MachineSetup();
				material.addAllowedMachineSetup(primaryMSwithSecondaryLine);
				machineSetupList.add(primaryMSwithSecondaryLine);
				
				primaryMSwithSecondaryLine.addProduction(component, capacityLine, secondaryLine);
				primaryMSwithSecondaryLine.addProduction(material, capacityPrimary, pckgPrimary);
				primaryMSwithSecondaryLine.setWorkers(workersPrimary);
			}
		}
		
		//SECONDARY MACHINE SETUP	
		if (pckgSecondary == null) {
			material.setSecondaryPacker(null);
			return;
		}
		
		if (material.getSecondaryPacker() == null) {
			material.setSecondaryPacker(pckgSecondary);
			
			MachineSetup secondaryMachineSetup = new MachineSetup();
			material.addAllowedMachineSetup(secondaryMachineSetup);
			machineSetupList.add(secondaryMachineSetup);
			
			int capacitySecondary = (int)(tactSecondary/60.0*TimeHandler.timeStepLength);
			
			secondaryMachineSetup.addProduction(component, capacityLine, line);
			secondaryMachineSetup.addProduction(material, capacitySecondary, pckgSecondary);		
			secondaryMachineSetup.setWorkers(workersSecondary);
			
			//WITH SECONDARY LINE - dolepene, TODO: urobit krajsie
			if (secondaryLine != null) {
				MachineSetup secondaryMSwithSecondaryLine = new MachineSetup();
				material.addAllowedMachineSetup(secondaryMSwithSecondaryLine);
				machineSetupList.add(secondaryMSwithSecondaryLine);
				
				secondaryMSwithSecondaryLine.addProduction(component, capacityLine, secondaryLine);
				secondaryMSwithSecondaryLine.addProduction(material, capacitySecondary, pckgSecondary);
				secondaryMSwithSecondaryLine.setWorkers(workersSecondary);
			}
		}
	}
	
	private Material getMaterial(String name, Group group, int order, double startupTime, Tube tube) {
		
		Material material = dictionary.getMaterial(name);
		
		if (material == null) {
			material = new Material(name);
			materialList.add(material);
			dictionary.addMaterial(material);
		}
		
		material.setGroup(group);
		material.setOrder(order);
		material.setStartupTime(timeHandler.hoursToTimeSteps(startupTime));
		material.setTube(tube);
		
		return material;	
	}
	
	
	private Machine getMachine(String name, boolean line) {
		if (name.equals("0") || name == null || name.equals("")) return null;
		
		Machine machine = dictionary.getMachine(name);
		
		if (machine == null) {
			machine = new Machine(name);
			machineList.add(machine);
			dictionary.addMachine(machine);
		}
		
		machine.setLine(line);
		
		return machine;
	}
		
	private Group getGroup(String name, boolean sanitationBeforeSwitch, int sanitationPeriod,
			double sanitationTime, int sanitationWorkers) {
		
		Group group = dictionary.getGroup(name);
	
		if (group == null) {
			group = new Group(name);
			groupList.add(group);
			dictionary.addGroup(group);
		}
		
		int sanitationTimeInTimeSteps = timeHandler.hoursToTimeSteps(sanitationTime);
		
		group.setSanitationWorkers(sanitationWorkers);
		group.setSanitationPeriod(sanitationPeriod);
		group.setPeriodicalSanitationTime(sanitationPeriod == 0 ? 0 : sanitationTimeInTimeSteps);
		group.setSanitationTime(sanitationBeforeSwitch ? sanitationTimeInTimeSteps : 0);
		
		return group;
	}
	
	private Tube getTube(String name) {
		Tube tube = dictionary.getTube(name);
		
		if (tube == null) {
			tube = new Tube(name);
			tubeList.add(tube);
			dictionary.addTube(tube);
		}
		
		tube.setSwitchTime(timeHandler.hoursToTimeSteps(1));
		
		return tube;
	}
}
