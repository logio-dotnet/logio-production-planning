package cz.logio.production.planning.data.load;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import cz.logio.production.planning.app.Configuration;
import cz.logio.production.planning.domain.Machine;
import cz.logio.production.planning.domain.MachineSetup;
import cz.logio.production.planning.domain.Material;
import cz.logio.production.planning.domain.PlanningAssignment;
import cz.logio.production.planning.domain.Shift;
import cz.logio.production.planning.domain.Task;
import cz.logio.production.planning.utils.DbConnector;
import cz.logio.production.planning.utils.Dictionary;
import cz.logio.production.planning.utils.Neighbourhood;
import cz.logio.production.planning.utils.TimeHandler;

public class ProblemLoader {
	
	private Dictionary dictionary;
	
	private TimeHandler timeHandler;
	
	private Map<Material,List<PlanningAssignment>> materialToPlanningAssignmentsMap = 
			new HashMap<Material,List<PlanningAssignment>>();
	
	public Map<Machine,TreeSet<PlanningAssignment>> trees;
	
	public List<Task> taskList;
	
	public List<PlanningAssignment> planningAssignmentList;
	
	public List<Shift> shiftList;
	
	private Double maxStockProductionPriority = null;
	
	public ProblemLoader(Dictionary dictionary, TimeHandler timeHandler) {
		this.dictionary = dictionary;
		this.timeHandler = timeHandler;
		this.trees = Neighbourhood.createTreeStructure(dictionary.getMachines());
	}
	
	private int lastAssignmentId = -1;
	
	/**
	 * Loads production_suggested and corresponding shifts. 
	 * Information is stored in taskList, planningAssignmentList and shiftList.
	 */
	public void loadProblem() {
		taskList = new ArrayList<Task>();
		planningAssignmentList = new ArrayList<PlanningAssignment>();
		
		loadProductionSuggested();
		loadCurrentProduction();
		
		setAllowedPlanningAssignments();
		
		loadShifts();
		
		System.out.println(taskList.size() + " tasks loaded");
		System.out.println(shiftList.size() + " shifts loaded");
	}
	
	private void loadProductionSuggested() {
		try {	
			//OFC
			String query = "SELECT bps.*,bofc.estimated_date_of_delivery "
					+ "FROM bonavita_production_suggested bps " +
					"JOIN bonavita_order_from_client bofc USING (ofc_id_from_customer)"
					+ " WHERE bofc.part = 1;";		
			
			ResultSet res = DbConnector.executeFetchAll(query);
			while (res.next()) {
				processProductionSuggested(res);
			}
			
			//NON-OFC
			query = "SELECT * FROM bonavita_production_suggested bps "
					+ " WHERE ofc_id_from_customer IS NULL;";		
			
			res = DbConnector.executeFetchAll(query);
			while (res.next()) {
				processNonOfc(res);
			}
		} catch (Exception e) {
			e.printStackTrace();
		  	}
	}
	
	private void processProductionSuggested(ResultSet result) throws SQLException {
		String materialName = result.getString("product_id_from_customer");
		
		Material material = dictionary.getMaterial(materialName);
		
		if (material != null) {//loaded material		
			String id = result.getString("id");
			int amount = result.getInt("amount");
			Date date = TimeHandler.getDateTime(result.getString("date"));
			String ofcId =  result.getString("ofc_id_from_customer");
			boolean ofc = ofcId != null;
			double price = result.getDouble("priority");
					
			int dueTime = timeHandler.getTimeStepsFromStart(date);				
			
			Date estimatedDateOfDelivery = TimeHandler.getDateTime(result.getString("estimated_date_of_delivery"));
					
			Task task = createTask(id,0,dueTime,material,amount,true);
			task.setOfc(ofc);
			if (ofc) task.setOfcId(ofcId);
			task.setPrice(price);
			task.setOfcDueTime(timeHandler.getTimeStepsFromStart(estimatedDateOfDelivery));
		}
	}
	
	private void processNonOfc(ResultSet result) throws SQLException {
		String materialName = result.getString("product_id_from_customer");	
		Material material = dictionary.getMaterial(materialName);
		
		if (material != null) {//loaded material		
			String id = result.getString("id");
			int amount = result.getInt("amount");
			Date date = TimeHandler.getDateTime(result.getString("date"));
			double priority = result.getDouble("priority");
					
			double earliestPickupTime = 23.75;
			int dueTime = timeHandler.getTimeStepsFromStart(date) + timeHandler.hoursToTimeSteps(earliestPickupTime);	
			
			if (id.endsWith("min")) {
				material.setMakeToMin(amount);
				material.setPriority(priority);
				material.setReversedPriority(getMaxStockProductionPriority()-priority);
				
				List<PlanningAssignment> existingAssignments = materialToPlanningAssignmentsMap.get(material);
				if (existingAssignments == null || existingAssignments.isEmpty()) {
					Task task = createTask(id,0,dueTime,material,0,true);
					task.setFixed(true);
					task.setPrice(priority);
				}		
			}
			if (id.endsWith("max")) {
				material.setMakeToMax(amount);
				material.setPriority(priority);
			}		
		}
	}
	
	private void loadCurrentProduction() {
		try {	
			Connection conn = DbConnector.getConnectionToPwDB();
			if (conn == null) return;			
			
			String query = "SELECT * from bonavita_current_production";
			
			ResultSet res = DbConnector.executeFetchAll(query);
			
			Map<String,List<List<String>>> currentAssignments = new HashMap<String,List<List<String>>>();
			
			while (res.next()) {
				String originalPlanningAssignment = res.getString("planning_assignment");
				List<List<String>> results = currentAssignments.get(originalPlanningAssignment);				
				if (results == null) {
					results = new ArrayList<List<String>>();
					currentAssignments.put(originalPlanningAssignment, results);
				}	
				
				List<String> resultList = new ArrayList<String>();
				resultList.add(res.getString("line"));
				resultList.add(res.getString("product_id_from_customer"));
				resultList.add(res.getString("packer"));
				resultList.add(res.getString("amount_to_produce"));
				resultList.add(res.getString("start_time"));	
				results.add(resultList);
			}	
			
			for (Entry<String, List<List<String>>> results : currentAssignments.entrySet()) {
				processCurrentProduction(results.getValue(),results.getKey());
			}	
			
		} catch (Exception e) {
			e.printStackTrace();
		  	}
	}
	
	private void processCurrentProduction(List<List<String>> results, String planningAssignmentId) throws SQLException {
		Map<Material,Integer> materials = new HashMap<Material,Integer>();
		List<MachineSetup> availableMachineSetups = new ArrayList<MachineSetup>();
		List<Machine> machines = new ArrayList<Machine>();
		List<Task> tasks = new ArrayList<Task>();
		
		int startTime = 0;
		Material material = null;
		
		int i = 0;
		
		for (List<String> result : results) {
			i++;
			String lineName = result.get(0);
			String materialName = result.get(1);
			String packerName = result.get(2);	
			int amount = Integer.parseInt(result.get(3));
			Date startDate = TimeHandler.getDateTime(result.get(4));	
			
			startTime = timeHandler.getTimeStepsFromStart(startDate);
			
			material = dictionary.getMaterial(materialName);		
			int loadedAmount = amount;
			amount = (int)(material.getPaletteAmount()*(Math.floor(amount/material.getPaletteAmount()))); //round to palettes
			materials.put(material,amount);
			//System.out.println("Current production material " + material + " amount " + amount + " loaded amount " + loadedAmount + " palette amount " + material.getPaletteAmount());
			availableMachineSetups.addAll(material.getAllowedMachineSetups());
			
			Machine line = dictionary.getMachine(lineName);
			machines.add(line);
			Machine packer = dictionary.getMachine(packerName);
			machines.add(packer);
			
			Task task = createTask("current-" + planningAssignmentId + "-" + i,startTime,timeHandler.getTimePointsAvailable(),material,amount,false);
			task.setFixed(true);
			tasks.add(task);
		}
		
		MachineSetup possibleMachineSetup = null;
		
		for (MachineSetup allowedMachineSetup : availableMachineSetups) {	
			//check all machines and materials
			if (allowedMachineSetup.getMaterialList().containsAll(materials.keySet()) &&
					allowedMachineSetup.getMachineList().containsAll(machines)) {
				possibleMachineSetup = allowedMachineSetup;
				break;
			}
		}
		
		if (possibleMachineSetup == null) {
			return;
		}
		
		PlanningAssignment assignment = new PlanningAssignment("current-" + planningAssignmentId,++lastAssignmentId,material);
		planningAssignmentList.add(assignment);
		assignment.getNeighbourhood().setTrees(trees);
		assignment.setInitialTasks(tasks);
		assignment.setFixed(true);
		assignment.setTimeStart(startTime);
		assignment.setMachineSetup(possibleMachineSetup);
		
		for (Task task : tasks) {
			task.setInitialAssignment(assignment);
		}
	}
	
	private Task createTask(String id, int readyTime, int dueTime,Material material,int amount, boolean createAssignment) {
		Task task = new Task(id,readyTime,dueTime,material,amount);
		taskList.add(task);
		
		if (createAssignment) {
			//each task has its own planning assignment that might be used to fulfill the task
			PlanningAssignment assignment = new PlanningAssignment(id+"PA",++lastAssignmentId,material);
			planningAssignmentList.add(assignment);
			assignment.getNeighbourhood().setTrees(trees);
			
			task.setInitialAssignment(assignment);
			
			Set<Material> availableMaterials = new HashSet<Material>();
			for (MachineSetup machineSetup : assignment.getAllowedMachineSetupList()) {
				availableMaterials.addAll(machineSetup.getMaterialList());
			}
			
			for (Material m : availableMaterials) {
				if (!materialToPlanningAssignmentsMap.containsKey(m)) {
					materialToPlanningAssignmentsMap.put(m, new ArrayList<PlanningAssignment>());
				}
				materialToPlanningAssignmentsMap.get(m).add(assignment);
			}	
		}
		
		return task;
	}
	
	private void setAllowedPlanningAssignments(){
		for (Task task : taskList) {
			if (!task.isFixed())
				task.setAllowedPlanningAssignments(materialToPlanningAssignmentsMap.get(task.getMaterial()));
		}
	}
	
	private void loadShifts() {
		shiftList = new ArrayList<Shift>();
		
		int timeStart = 0;
		int cost = 100;
		
		List<CapacityException> exceptions = loadCapacityExceptions();
		
		//first shift - partial
		{
			int timeEnd = timeStart+timeHandler.getTimeStepsToNextShift();
			if (timeEnd != timeStart) {
				createShift(timeStart,timeEnd,cost, exceptions);						
				timeStart = timeEnd;
			}
		}
				
		while (timeStart < timeHandler.getTimePointsAvailable()) {			
			int timeEnd = timeStart+timeHandler.getShiftLengthInTimeSteps();				
			createShift(timeStart,timeEnd,cost, exceptions);						
			timeStart = timeEnd;
		}
	}
	
	
	private List<CapacityException> loadCapacityExceptions() {
		List<CapacityException> exceptions = new ArrayList<CapacityException>();
		
		try {	
			Connection conn = DbConnector.getConnectionToPwDB();
			if (conn == null) return exceptions;
			
			String query = "SELECT * FROM bonavita_production_capacity_exception WHERE date >= '" + 
					timeHandler.dateToString(timeHandler.getStartDate()) + "';";
			
			System.out.println(query);
			
			Statement st = conn.createStatement();
			ResultSet res = st.executeQuery(query);
			
			while (res.next()) {
				exceptions.add(new CapacityException(res.getString("machine"),res.getString("date"),res.getInt("shift"), res.getBoolean("allowed")));
			}
		} catch (Exception e) {
			e.printStackTrace();
	  	}
		
		return exceptions;
	}
	
	private void createShift(int start, int end, int cost, List<CapacityException> exceptions) {
		Shift shift = new Shift(start,end);		
		shiftList.add(shift);		
		shift.setCost(cost);
		shift.setDateStart(timeHandler.getDate(start));
			
		for (Machine machine : dictionary.getMachines()) {
			shift.setForbidden(isShiftForbidden(exceptions,shift,machine), machine);
		}
	}
	
	private boolean isShiftForbidden(List<CapacityException> exceptions,Shift shift, Machine machine) {
		for (CapacityException exception: exceptions) {
			if (exception.machine == machine && exception.shiftStart == shift.getTimeStart()) {
				return !exception.allowed;
			}
		}
		
		//nedelna odstavka - v nedelu v noci a v pondelok cez den
		int dayOfWeek = timeHandler.getDayOfWeek(shift.getTimeStart());
		if (dayOfWeek == 7 && !shift.isDayShift()) {			
			return true;
		}
		if (dayOfWeek == 1 && shift.isDayShift()) {
			return true;
		}
		
		return false;
	}
	
	private class CapacityException {
		public Machine machine;
		public int shiftStart;
		public boolean allowed;
		
		public CapacityException(String machine, String date, int shift, boolean allowed) {
			this.machine = dictionary.getMachine(machine);
			this.shiftStart = timeHandler.getTimeStepsFromStart(timeHandler.getShiftStartingDate(TimeHandler.getDate(date), shift));
			this.allowed = allowed;	
		};
	}
	
	private double getMaxStockProductionPriority() {
		if (maxStockProductionPriority == null) {
			ResultSet res = DbConnector.executeFetchAll("SELECT MAX(priority) FROM bonavita_production_suggested WHERE ofc_id_from_customer IS NULL");
			try {
				res.next();
				maxStockProductionPriority = res.getDouble(1);
			} catch (SQLException e) {
				e.printStackTrace();
			}	
		}
		
		return maxStockProductionPriority;
	}
	
	
}
